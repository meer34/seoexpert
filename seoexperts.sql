-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jun 09, 2018 at 08:19 AM
-- Server version: 10.1.29-MariaDB
-- PHP Version: 7.2.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `seoexperts`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin_login`
--

CREATE TABLE `admin_login` (
  `admin_id` int(222) NOT NULL,
  `name` varchar(200) NOT NULL,
  `username` varchar(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `pic` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin_login`
--

INSERT INTO `admin_login` (`admin_id`, `name`, `username`, `email`, `password`, `pic`) VALUES
(1, 'admin', 'admin', 'admin@gmail.com', 'admin', 'uploads/my.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `chat`
--

CREATE TABLE `chat` (
  `id` bigint(20) NOT NULL,
  `roomId` int(11) NOT NULL,
  `senderId` int(11) NOT NULL,
  `message` text NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `chat`
--

INSERT INTO `chat` (`id`, `roomId`, `senderId`, `message`, `time`, `status`) VALUES
(1, 138, 154, '0', '2018-06-04 20:49:42', 0),
(2, 138, 154, 'hello', '2018-06-04 20:50:25', 0),
(3, 138, 154, 'how r u', '2018-06-04 20:50:37', 0),
(4, 166, 154, 'hello guys how r u', '2018-06-04 20:51:17', 0),
(5, 139, 154, 'hi', '2018-06-05 06:13:52', 0),
(6, 139, 154, 'tanveer', '2018-06-05 06:42:08', 0),
(7, 139, 154, 'abrar', '2018-06-05 06:42:24', 0),
(8, 166, 154, 'tanveer', '2018-06-05 06:43:08', 0),
(9, 157, 154, 'hello', '2018-06-05 06:44:10', 0),
(10, 157, 157, 'expert here', '2018-06-05 06:49:57', 0),
(11, 157, 154, 'hello expert', '2018-06-05 06:50:35', 0),
(12, 157, 157, 'how r u', '2018-06-05 06:50:51', 0),
(13, 157, 154, 'hello', '2018-06-05 07:24:19', 0),
(14, 166, 154, 'how r u', '2018-06-05 07:25:32', 0),
(15, 157, 154, 'my name is danish', '2018-06-05 08:56:12', 0),
(16, 157, 157, 'hello danish how r u', '2018-06-05 08:56:37', 0),
(17, 139, 167, 'hello mr. a', '2018-06-05 09:03:50', 0),
(18, 139, 139, 'hello everybody', '2018-06-05 09:08:35', 0),
(19, 139, 154, 'expert is here', '2018-06-05 09:09:02', 0),
(20, 139, 154, 'heelo expert', '2018-06-05 09:10:11', 0),
(21, 138, 0, 'hello', '2018-06-06 04:33:39', 0),
(22, 158, 167, 'hello', '2018-06-06 05:52:09', 0),
(23, 158, 167, 'how r u', '2018-06-06 05:52:17', 0),
(24, 158, 158, 'hello', '2018-06-06 05:53:24', 0),
(25, 158, 158, 'how r u danial', '2018-06-06 05:54:14', 0),
(26, 158, 167, 'im fine', '2018-06-06 05:56:54', 0),
(27, 158, 167, 'what about you', '2018-06-06 05:57:17', 0),
(28, 158, 158, ':)', '2018-06-06 05:58:50', 0),
(29, 166, 139, 'hello tanveer', '2018-06-06 07:36:26', 0),
(30, 166, 166, 'hello', '2018-06-06 07:42:09', 0),
(31, 166, 167, 'hello tanveer', '2018-06-06 08:13:40', 0),
(32, 166, 166, 'how r u', '2018-06-06 08:14:35', 0),
(33, 166, 167, 'whats up', '2018-06-06 08:14:40', 0),
(34, 166, 167, 'hi', '2018-06-06 09:40:01', 0),
(35, 166, 154, 'hello', '2018-06-06 09:57:43', 0),
(36, 166, 154, 'hello mister', '2018-06-06 09:58:59', 0),
(37, 166, 167, 'how r u', '2018-06-06 09:59:29', 0),
(38, 166, 167, 'hi', '2018-06-06 10:08:26', 0),
(39, 166, 166, 'hi', '2018-06-06 10:20:18', 0),
(40, 166, 167, 'hello tanveer', '2018-06-06 10:21:50', 0),
(41, 166, 166, 'HI DANIAL', '2018-06-06 10:27:17', 0),
(42, 166, 167, 'how r u', '2018-06-06 19:24:59', 0),
(43, 166, 166, 'shukkr Allah ka', '2018-06-06 19:25:10', 0);

-- --------------------------------------------------------

--
-- Table structure for table `experts`
--

CREATE TABLE `experts` (
  `expert_id` int(6) NOT NULL,
  `expert_name` varchar(200) DEFAULT NULL,
  `expert_nick` varchar(50) DEFAULT NULL,
  `expert_email` varchar(100) DEFAULT NULL,
  `expert_phone` varchar(20) DEFAULT NULL,
  `expert_bd` varchar(10) DEFAULT NULL,
  `expert_pass` varchar(100) DEFAULT NULL,
  `expert_gender` char(1) DEFAULT NULL,
  `isExpert` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `experts`
--

INSERT INTO `experts` (`expert_id`, `expert_name`, `expert_nick`, `expert_email`, `expert_phone`, `expert_bd`, `expert_pass`, `expert_gender`, `isExpert`) VALUES
(0, 'Tanveer Arshad', 'meer34', 'tanveerarshad39@gmail.com', '3064798395', '29/03/1996', '4b27741a5301900b23c1ad91eafee3a8', 'M', 0),
(0, 'Tanveer Arshad', 'meer345', 'tanveerarshad93@gmail.com', '3064798395', '29/04/1996', 'ba7d9405fbf0ac77aefc8d657629e34f', 'M', 0),
(0, 'Anus Shahbaz', 'anus34', 'anus34@gmial.com', '03456789123', '28/02/2018', '6531401f9a6807306651b87e44c05751', 'M', 0),
(0, 'Tanveer Arshad', 'Meer', 'tanveerarshad@gmail.com', '3064798395', '29/03/1996', '81dc9bdb52d04dc20036dbd8313ed055', 'M', 0),
(0, 'Danish Raza', 'danish', 'danish@gmail.com', '3064798395', '05/03/2018', '827ccb0eea8a706c4c34a16891f84e7b', 'M', 0),
(0, 'Danial Raza', 'danial', 'danial@gmail.com', '03451234567', '29/03/1996', '81dc9bdb52d04dc20036dbd8313ed055', 'M', 0),
(0, 'Tanveer Arshad', 'Meer12', 'tanveerarshad97@gmail.com', '3064798395', '29/03/1996', '56be0c23094c10eebf592c9bf3e5319a', 'M', 0),
(0, 'Tanveer Arshad', 'meer34567', 'meer@gmail.com', '3064798395', '29/03/1996', '25d55ad283aa400af464c76d713c07ad', 'M', 0);

-- --------------------------------------------------------

--
-- Table structure for table `footer`
--

CREATE TABLE `footer` (
  `id` bigint(20) NOT NULL,
  `footer_conditions` varchar(200) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `footer`
--

INSERT INTO `footer` (`id`, `footer_conditions`) VALUES
(213, 'footer terms and conditions'),
(214, 'Contact us'),
(215, 'Events &amp; Media '),
(216, 'Help '),
(217, 'Ownership Statement');

-- --------------------------------------------------------

--
-- Table structure for table `header`
--

CREATE TABLE `header` (
  `id` int(222) NOT NULL,
  `heading` varchar(200) NOT NULL,
  `header_img` varchar(200) NOT NULL,
  `paragraph` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `header`
--

INSERT INTO `header` (`id`, `heading`, `header_img`, `paragraph`) VALUES
(1, 'welcome', '3.jpg', 'dsdsdsdsds'),
(2, 'welocme', '', 'dsdsddsds'),
(3, 'welocme', '', 'dsdsddsds'),
(4, 'welocme', '', 'dsdsddsds'),
(5, 'welocme', '', 'dsdsddsds'),
(6, 'sdsd', '', 'sdsdsds'),
(7, 'sdsd', '', 'sdsdsds'),
(8, 'fdf', '', 'dfdfdf'),
(9, 'fdfgsf', '', 'ffddsfdsfdssf'),
(10, 'fdfgsf', '', 'ffddsfdsfdssf'),
(11, 'helo', '2.jpg', 'sasqsasa'),
(12, 'expert', 'pso.png', 'helo this is expert page');

-- --------------------------------------------------------

--
-- Table structure for table `rating`
--

CREATE TABLE `rating` (
  `id` int(10) UNSIGNED NOT NULL,
  `clientId` int(10) UNSIGNED NOT NULL,
  `expertId` int(10) UNSIGNED NOT NULL,
  `rating` int(10) UNSIGNED NOT NULL,
  `review` varchar(200) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `rating`
--

INSERT INTO `rating` (`id`, `clientId`, `expertId`, `rating`, `review`, `time`) VALUES
(1, 167, 157, 1, 'very nice work', '2018-06-06 19:23:00'),
(2, 167, 157, 2, 'another rating set', '2018-06-06 19:28:00'),
(3, 167, 157, 3, 'i want to rate agian', '2018-06-06 19:29:32'),
(4, 167, 166, 4, 'here\'s another rating', '2018-06-06 19:31:23'),
(5, 167, 166, 5, 'test rating', '2018-06-06 19:33:37'),
(6, 167, 138, 5, 'test rating', '2018-06-06 19:33:37'),
(7, 167, 139, 4, 'very nice work', '2018-06-07 07:14:12');

-- --------------------------------------------------------

--
-- Table structure for table `side_bar`
--

CREATE TABLE `side_bar` (
  `id` int(222) NOT NULL,
  `category` varchar(200) NOT NULL,
  `icon` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `side_bar`
--

INSERT INTO `side_bar` (`id`, `category`, `icon`) VALUES
(19, 'Love and Relationship', 'menus/1.jpg'),
(20, 'Tarot and Cards', 'menus/2.jpg'),
(21, 'Dream Interpretation', 'menus/31.jpg'),
(22, 'Astorolgy', 'menus/bazmeraza1.jpg'),
(23, 'Palm Reading', 'menus/pso4.png'),
(24, 'Spiritual Guides dfsff', 'menus/bazmeraza3.jpg'),
(27, 'Spiritual Guides ', 'menus/23.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `user_id` int(6) NOT NULL,
  `fname` varchar(200) DEFAULT NULL,
  `lname` varchar(200) NOT NULL,
  `user_nick` varchar(50) DEFAULT NULL,
  `user_email` varchar(100) DEFAULT NULL,
  `user_alt_email` varchar(200) NOT NULL,
  `rpm` int(3) NOT NULL,
  `user_phone` varchar(20) DEFAULT NULL,
  `user_bd` varchar(10) DEFAULT NULL,
  `user_pass` varchar(100) DEFAULT NULL,
  `user_gender` char(1) DEFAULT NULL,
  `registration_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `country` varchar(100) NOT NULL,
  `city` varchar(100) NOT NULL,
  `state_province` varchar(200) NOT NULL,
  `street` varchar(100) NOT NULL,
  `zipcode` int(222) NOT NULL,
  `profile_pic` varchar(200) NOT NULL DEFAULT 'uploads/profile.png',
  `cover_pic` varchar(200) NOT NULL DEFAULT 'uploads/cover.png',
  `id_front_pic` varchar(200) NOT NULL,
  `id_back_pic` varchar(200) NOT NULL,
  `expert_info` varchar(200) NOT NULL,
  `isExpert` int(100) NOT NULL DEFAULT '0',
  `isVerified` int(222) NOT NULL,
  `verificationCode` varchar(6) NOT NULL,
  `isTerminated` int(1) NOT NULL DEFAULT '0',
  `short_desc` text NOT NULL,
  `bio` text NOT NULL,
  `status` varchar(50) NOT NULL DEFAULT 'pending',
  `availability` int(3) NOT NULL DEFAULT '3'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`user_id`, `fname`, `lname`, `user_nick`, `user_email`, `user_alt_email`, `rpm`, `user_phone`, `user_bd`, `user_pass`, `user_gender`, `registration_date`, `country`, `city`, `state_province`, `street`, `zipcode`, `profile_pic`, `cover_pic`, `id_front_pic`, `id_back_pic`, `expert_info`, `isExpert`, `isVerified`, `verificationCode`, `isTerminated`, `short_desc`, `bio`, `status`, `availability`) VALUES
(138, 'a', 'abrar', 'w', 'abrar_bashir11@yahoo.com', '', 0, '333434', '05/04/2018', '81dc9bdb52d04dc20036dbd8313ed055', 'M', '2018-06-06 19:24:12', 'PK', 'SF', 'CA', 'dssdd', 0, 'uploads/214.jpg', 'uploads/116.jpg', '', '', 'Love and Relationship,Tarot and Cards,Dream Interpretation', 1, 1, '', 0, 'fdfddf', 'fddf', 'Approved', 3),
(139, 'a', 'a', 'd', 'a@yahoo.com', '', 0, '3232', '31/03/2018', '81dc9bdb52d04dc20036dbd8313ed055', 'M', '2018-06-06 07:36:33', 'PK', 'SF', 'CA', 'ds', 0, 'uploads/2.jpg', 'uploads/21.jpg', '', '', 'Dream Interpretation', 1, 1, '', 0, 'dss', 'ds', 'pending', 3),
(154, 'Danish', 'Raza', 'danish', 'danish@gmail.com', '', 0, '3064798395', '05/04/2018', '5b119a961fcb523c81c25e8f79de2380', 'M', '2018-06-04 06:26:23', 'PK', 'Gujranwala', 'Punjab', 'Colg Rd', 0, 'uploads/danish.jpg', 'uploads/cover.png', '', '', '', 0, 1, '518623', 0, '', '', 'pending', 3),
(157, 'Tanveer', 'Arshad', 'meer', 'tanveerarshad@gmail.com', 't@gmal.com', 1, '3064798395', '29/03/1996', '81dc9bdb52d04dc20036dbd8313ed055', 'M', '2018-06-02 07:04:56', 'Pakistan', 'Daska', 'Punjab', 'College road, Gulistan Colony, Barkat Town, daska', 51010, 'uploads/profile.png', 'uploads/cover.png', '', '', 'Love and Relationship,Tarot and Cards,Dream Interpretation', 1, 1, '506284', 0, 'this is tanveer', 'this is tanveer', 'pending', 3),
(158, 'Abc', 'Def', 'abc', 'abc@gmail.com', 'def@gmail.com', 0, NULL, NULL, '81dc9bdb52d04dc20036dbd8313ed055', NULL, '2018-06-06 07:22:13', '', '', '', '', 0, 'uploads/3.png', 'uploads/cover.png', '', '', '', 1, 1, '987654', 0, '', '', 'pending', 3),
(160, 'ghi', 'jkm', 'ghi', 'ghi@gmail.com', 'def@gmail.com', 0, NULL, NULL, '81dc9bdb52d04dc20036dbd8313ed055', NULL, '2018-06-06 07:23:26', '', '', '', '', 0, 'uploads/4.png', 'uploads/cover.png', '', '', '', 1, 1, '987653', 0, '', '', 'pending', 3),
(161, 'mno', 'pqr', 'pqr', 'pqr@gmail.com', '', 0, NULL, NULL, '81dc9bdb52d04dc20036dbd8313ed055', NULL, '2018-06-02 05:23:18', '', '', '', '', 0, 'uploads/5.png', 'uploads/cover.png', '', '', '', 1, 1, '123456', 0, '', '', 'pending', 3),
(162, 'mno', 'pqr', 'pqr', 'pqr@gmail.com', '', 0, NULL, NULL, '81dc9bdb52d04dc20036dbd8313ed055', NULL, '2018-06-02 05:23:22', '', '', '', '', 0, 'uploads/6.png', 'uploads/cover.png', '', '', '', 1, 1, '123456', 0, '', '', 'pending', 3),
(166, 'Tanveer', 'Arshad', 'meer34', 'tanveerarshad39@gmail.com', 'tanveerarshad39@gmail.com', 1, '3064798395', '29/03/1996', '81dc9bdb52d04dc20036dbd8313ed055', 'M', '2018-06-06 20:16:10', 'Pakistan', 'Daska', 'Punjab', 'College road, Gulistan Colony, Barkat Town, daska', 51010, 'uploads/dear1.png', 'uploads/dear1.png', '', '', '', 1, 1, '583487', 0, 'this is tanveer', 'this is tanveer', 'pending', 3),
(167, 'Danial', 'Raza', 'danial', 'danial@gmail.com', '', 0, '03064798395', '29/03/1996', 'ab5e831d8600f15167307bd73177b525', 'M', '2018-06-05 09:01:44', 'PK', 'Daska', 'Punjab', 'College road, Gulistan Colony, Barkat Town, daska', 0, 'uploads/profile.png', 'uploads/cover.png', '', '', '', 0, 0, '230158', 0, '', '', 'pending', 3);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin_login`
--
ALTER TABLE `admin_login`
  ADD PRIMARY KEY (`admin_id`);

--
-- Indexes for table `chat`
--
ALTER TABLE `chat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `footer`
--
ALTER TABLE `footer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `header`
--
ALTER TABLE `header`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rating`
--
ALTER TABLE `rating`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `side_bar`
--
ALTER TABLE `side_bar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`user_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admin_login`
--
ALTER TABLE `admin_login`
  MODIFY `admin_id` int(222) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `chat`
--
ALTER TABLE `chat`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=44;

--
-- AUTO_INCREMENT for table `rating`
--
ALTER TABLE `rating`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `user_id` int(6) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=168;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

var chat = {}

chat.expert = $('.expert').val();
chat.sender = $('.sender').val();

chat.throwMessages = function (message)
{
    if($.trim(message).length != 0)
    {
        $.ajax({
            url: '<?= base_url("chat/throwMessages"); ?>',
            method:'POST',
            data:{receiver:chat.expert, sender:chat.sender ,message: message, },
            success:function(data){
                console.log(chat.expert);
                console.log(chat.sender);
                console.log(message);
                chat.enter.val('');
            }
        });        
    }
}

chat.enter = $('.enter');
chat.enter.bind('keydown', function(e){
    console.log(e.keyCode);
    if(e.keyCode === 13 && e.shiftKey === false){
        chat.throwMessages($(this).val());
        e.preventDefault();
    }
});
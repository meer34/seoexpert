<?php

class Live extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('menus');
        $this->load->helper('form');
        $this->load->model('MainPageProfiles');
        $this->load->model('expertmodel');
        $data = [];
    }

    public function index()
    {   
        if( $this->session->userdata['isExpert'] == 1 && $this->session->userdata['login'] == true && $this->session->userdata['id'] == $this->session->userdata['id'] )
        {
            $this->expertmodel->availability();
            $this->data['expert'] = $this->MainPageProfiles->randomExperts(); 
            $this->data['sidebar']=$this->menus->getallcategories();
            $this->data['footer']=$this->menus->getallfooter();		
            $this->data['title'] = $this->session->set_flashdata('title', $this->session->userdata['name'].'\'s Cam');
            $this->load->view('live', $this->data);
            $this->load->view('static/fixedSidebarLeft',$this->data);
            $this->load->view('static/footer',$this->data);
        }
        else
        {
            redirect('welcome');
        }
    }

    public function expert($userId)
    {
        // if( $this->session->userdata['isExpert'] == 0 )
        // {
            $this->data['user'] =  $this->MainPageProfiles->getExpert($userId);
            $this->data['expert'] = $this->MainPageProfiles->randomExperts();
            $this->data['sidebar'] = $this->menus->getallcategories();
            $this->data['footer'] = $this->menus->getallfooter();
            $this->data['title'] = $this->session->set_flashdata('title', $this->data['user']->fname.'\'s Cam');
            $this->load->view('live', $this->data);
            $this->load->view('static/fixedSidebarLeft',$this->data);
            $this->load->view('static/footer',$this->data);
        // }
        // else
        // {
        //     redirect('live');
        // }
    }

}
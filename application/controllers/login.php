<?PHP

class Login extends CI_Controller 
{
    public function __construct()
    {
        parent::__construct();
        $data = [];
    }
    public function index()
    {   
        $this->load->helper('form');
        $this->load->view('signup');
    }

    public function user_login()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<small class="form-text" style="color: #ff5e3a;">','</small>');

        $email = $this->input->post('email');
        $password = $this->input->post('password');

        if ($email == "" || $password == "")
        {
            $this->data['messages'] = $this->session->set_flashdata("messages","Password or Email Field is empty");
            $this->data['title'] = $this->session->set_flashdata("title", "Login Faild");
            redirect('messages', $this->data);
        }
        else{
            if ( $this->form_validation->run('login') )
            {                
                $this->load->model('Loginmodel');
                $login_id = $this->Loginmodel->login_valid($email, $password);
                if ( $login_id )
                {
                    if($login_id['isExpert']== 1){
                    if ( $login_id['isTerminated'] == 0 )//and $login_id['status'] == "Approved")
                    {
                        $loginSessions = [
                            'id' => $login_id['user_id'],
                            'name' => $login_id['fname'].' '.$login_id['lname'],
                            'email' => $login_id['user_email'],
                            'profile' => $login_id['profile_pic'],
                            'isExpert' => $login_id['isExpert'],
                            'isVerified' => $login_id['isVerified'],                      
                            'login' => true
                        ];
                        $this->session->set_userdata($loginSessions);
                        redirect('welcome');
                    }
                    else 
                    {
                        $loginSessions = [
                            'id' => $login_id['user_id'],
                            'name' => $login_id['fname'].' '.$login_id['lname'],
                            'email' => $login_id['email'],
                            'profile' => $login_id['profile_pic'],
                            'isExpert' => $login_id['isExpert'],
                            'isVerified' => $login_id['isVerified'],
                            'login' => true
                        ];
                        $this->session->unset_userdata($loginSessions);
                        $this->data['messages'] = $this->session->set_flashdata("messages","Your Account is terminated, Contact us to know what's happening");
                        $this->data['title'] = $this->session->set_flashdata("title", "Account Terminated");
                        redirect('messages', $this->data);
                    }
                }
                else
                {
                    if ( $login_id['isTerminated'] == 0 )
                    {
                        $loginSessions = [
                            'id' => $login_id['user_id'],
                            'name' => $login_id['fname'].' '.$login_id['lname'],
                            'email' => $login_id['user_email'],
                            'profile' => $login_id['profile_pic'],
                            'isExpert' => $login_id['isExpert'],
                            'isVerified' => $login_id['isVerified'],                            
                            'login' => true
                        ];
                        $this->session->set_userdata($loginSessions);
                        redirect('welcome');
                    }
                    else 
                    {
                        $loginSessions = [
                            'id' => $login_id['user_id'],
                            'name' => $login_id['fname'].' '.$login_id['lname'],
                            'email' => $login_id['email'],
                            'profile' => $login_id['profile_pic'],
                            'isExpert' => $login_id['isExpert'],
                            'isVerified' => $login_id['isVerified'],
                            'login' => true
                        ];
                        $this->session->unset_userdata($loginSessions);
                        $this->data['messages'] = $this->session->set_flashdata("messages","Your Account is terminated, Contact us to know what's happening");
                        $this->data['title'] = $this->session->set_flashdata("title", "Account Terminated");
                        redirect('messages', $this->data);
                    }

                }
                    
                }
                else
                {
                    $this->data['messages'] = $this->session->set_flashdata("messages","Invalid Email or Password");
                    $this->data['title'] = $this->session->set_flashdata("title", "Login Faild");
                    redirect('messages', $this->data);
                }                
            }
            else
            {
                redirect('welcome', validation_errors());
            }
        }
    }
}

?>
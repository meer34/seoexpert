<?php


class logout extends CI_Controller
{
    public function index()
    {
        $this->load->model('Expertmodel');
        $offline = $this->Expertmodel->offline();        
        $this->session->unset_userdata('id');
        $this->session->unset_userdata('name');
        $this->session->unset_userdata('email');
        $this->session->unset_userdata('profile');
        $this->session->unset_userdata('isExpert');
        $this->session->unset_userdata('isTerminated');
        $this->session->unset_userdata('login');             
        return redirect('welcome');
    }
}


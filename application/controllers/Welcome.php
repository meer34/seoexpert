<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller 
{
	var $data;
	public function __construct()
	{
		parent::__construct();
		$this->load->model('menus');
		$this->load->model('mainpageprofiles');
		$this->data= array();
		$this->load->helper('form');	
		
	}
	public function index()
	{
		// $this->session->set_userdata(['id' => rand(1,1000)]);
		$this->data['title'] = $this->session->set_flashdata('title','Welcome - Hire Experts');
		$this->load->helper('form');	
		//$this->data['experts'] = $this->mainpageprofiles->getProfiles();	
		$this->data['sidebar']=$this->menus->getallcategories();
		$this->data['footer']=$this->menus->getallfooter();	
		$this->load->view('main', $this->data);
		$this->load->view('static/fixedSidebarLeft',$this->data);
		$this->load->view('static/footer',$this->data);
	}

	public function loadMore()
	{
		$this->load->model('mainpageprofiles');
		$this->data['moreProfiles']=$this->mainpageprofiles->getProfiles();
	}

	public function about()
	{
		$this->load->view('about');
	}

}

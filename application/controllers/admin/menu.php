<?php 

class Menu extends CI_Controller{
   var $data;
   public function __construct(){
    parent::__construct();
    $this->load->model('Menus');
        $this->data = array();
   }
	public function index(){
		$this->load->view('admin/header');
	}
	public function header_data(){
		  // $id=$this->input->post('id');
            $heading = $this->input->post('heading');
             $paragraph = $this->input->post('paragraph');
             $header_img = $this->input->post('header_img');
                  
            
           $headerdata=array(
                
                'heading'=>$heading,
                'paragraph'=>$paragraph,
                 'header_img'=>$header_img

                
                          
            );
          
            $result= $this->Menus->insert_header($headerdata);
            if($result == TRUE){
                $this->session->set_flashdata("add", " header successfully added");
                redirect('admin/menu/');    
            } 
            else 
            {
                 $this->session->set_flashdata("exist", "header already exist");
                 redirect('admin/menu/'); 
            }
        
       
}
public function manage_header_view(){
 $this->data['header']=$this->Menus->getallheader();
  $this->load->view('admin/manageheader',$this->data);

}
 public function deleteheader($id) {

        if ($id == null) {
            redirect('admin/menu/manage_header_view');
        } else {
            $r = $this->Menus->removeheader($id);
            if ($r) {
                $this->session->set_flashdata('msg', 'Header Data Removed');
               redirect('admin/menu/manage_header_view');
            }
            redirect('admin/menu/manage_header_view');
    }

}
public function updateheader($id)
    {
        if($id != null)
        {
            $ans['header'] = $this->Menus->getheaderinfo($id);
            $this->load->view('admin/edit_header',$ans);
           
        }
        // $this->load->view('admin/edit_side_bar',$ans);
    }
    public function header_edit()
    {

         //echo "hello";
      

            $id=$this->input->post('id');
            $heading = $this->input->post('heading');
             $paragraph = $this->input->post('paragraph');
             $header_img = $this->input->post('header_img');
                  
            
           $headerdata=array(
                
                'heading'=>$heading,
                'paragraph'=>$paragraph,
                 'header_img'=>$header_img

                
                          
            );
          
            $result= $this->Menus->headerupdate($id,$headerdata);
            if($result == TRUE){
                      $this->session->set_flashdata("add", " edit successfully added");
                redirect('admin/menu/manage_header_view');    
            } 
            else 
            {
               $this->session->set_flashdata("exist", "header already exist");
               redirect('admin/menu/manage_header_view');
            }
        
    
}
 public function side_bar(){
    $this->load->view('admin/sidebar');
 }

    public function side_menu(){
        $config = ['upload_path'=>'./menus','allowed_types'=>'jpg|png|gif|jpeg'];
        $this->load->library('upload',$config);
         if($this->upload->do_upload('picture')){
             $data = $this->upload->data();
           // $image=$data['file_name'];
           // $img_path = base_url('uploads/'.$data["raw_name"].$data["file_ext"]);
              $img_path = 'menus/'.$data['file_name'];
          }
          $category=$this->input->post('Categories');
           // $userData = array(
           //     // 'name' => $this->input->post('name'),
           //      'category' => $this->input->post('Categories'),
           //      'icon' => $img_path
           //  );
           $insertUserData = $this->Menus->sidemenu($category,$img_path);
           if($insertUserData){
            $this->session->set_flashdata("add", "This Category successfully added");
            redirect('admin/menu/side_bar');
           }else{
            // echo "already h";
            $this->session->set_flashdata("exist", "This Category already exist");
          redirect('admin/menu/side_bar');
           }
    } 
    public function footer_view(){
$result=$this->Menus->getallfooter();
        $this->load->view('admin/footer');
    }
    public function footer(){
$footer=$this->input->post('footer_conditions');
//$data=array('footer_conditions' => $footer);
$result=$this->Menus->footer($footer);
 if($result){
            $this->session->set_flashdata("add", "This Footer successfully added");
            redirect('admin/menu/footer_view');
           }else{
            // echo "already h";
            $this->session->set_flashdata("exist", "This footer Category already exist");
          redirect('admin/menu/footer_view');
           }
}
public function manage_side_bar_view(){
 $this->data['sidebar']=$this->Menus->getallcategories();
  $this->load->view('admin/managesidebar',$this->data);

}
 public function deletecategory($id) {

        if ($id == null) {
            redirect('admin/menu/manage_side_bar_view');
        } else {
            $r = $this->Menus->removecategory($id);
            if ($r) {
                $this->session->set_flashdata('msg', 'Category Data Removed');
               redirect('admin/menu/manage_side_bar_view');
            }
            redirect('admin/menu/manage_side_bar_view');
    }

}
public function updatecategory($id)
    {
        if($id != null)
        {
            $ans['category'] = $this->Menus->getcategoryinfo($id);
            $this->load->view('admin/edit_side_bar',$ans);
           
        }
        // $this->load->view('admin/edit_side_bar',$ans);
    }
//////////////////
    public function category_edit()
    {

         //echo "hello";
      

            $id=$this->input->post('id');
            $Categories = $this->input->post('Categories');
            $pic_url_3 = $this->input->post('pic_url_3');           
            $name = $_FILES["icon"]["name"];
            $config['upload_path'] = './menus/';
            $config['allowed_types'] =     'gif|jpg|png|jpeg';
            $this->load->library('upload', $config);
            $is_new_image = false;
            $imgurl = "";
            if ( ! $this->upload->do_upload('icon') )
            {
                $error = array('error' => $this->upload->display_errors());
                $is_new_image = false;
            }
            else
            { 
                $upload_data=$this->upload->data();
                $is_new_image = true;
                $imgurl='menus/'.$upload_data['file_name'];
               // print_r($imgurl);
            }
           
           $categorydata=array(
                
                'category'=>$Categories,
                'icon'=>$imgurl
                          
            );
           //print_r($categorydata);
            if ($is_new_image == true) 
            {
                $categorydata['picture'] = $imgurl;
            } else {
                $categorydata['picture'] = $pic_url_3;
            }
           
           // $user_id=$this->session->userdata('id');
            $result= $this->Menus->categoryupdate($id,$categorydata);
            if($result == TRUE){
                redirect('admin/menu/manage_side_bar_view');    
            } 
            else 
            {
                $data['valiadtion'] = validation_errors();
                $this->load->view('admin/dashboard', $data);
            }
        
    
}

///////////////////
public function manage_footer_view(){
 $this->data['footer']=$this->Menus->getallfooter();
  $this->load->view('admin/managefooter',$this->data);

}
public function deletefooter($id) {

        if ($id == null) {
            redirect('admin/menu/manage_footer_view');
        } else {
            $r = $this->Menus->removefooter($id);
            if ($r) {
                $this->session->set_flashdata('msg', 'Footer Data Removed');
               redirect('admin/menu/manage_footer_view');
            }
            redirect('admin/menu/manage_footer_view');
    }

}
public function updatefooter($id)
    {
        if($id != null)
        {
            $ans['footer'] = $this->Menus->getfooterinfo($id);
            $this->load->view('admin/edit_footer',$ans);
           
        }
       // $this->load->view('admin/edit_footer',$ans);
    }
    ////
    public function footer_edit()
    {

         //echo "hello";
      

            $id=$this->input->post('id');
            $footer = $this->input->post('footer');
                  
            
           $footerdata=array(
                
                'footer_conditions'=>$footer,
                
                          
            );
          
            $result= $this->Menus->footerupdate($id,$footerdata);
            if($result == TRUE){
                redirect('admin/menu/manage_footer_view');    
            } 
            else 
            {
                $data['valiadtion'] = validation_errors();
                $this->load->view('admin/dashboard', $data);
            }
        
    
}

 
}

?>
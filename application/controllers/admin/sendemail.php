<?php 

class Sendemail extends CI_Controller{
	var $data;
	public function __construct(){
		parent::__construct();
		$this->load->model('Emailmodel');
		$this->data = array();
		 $this->load->helper('ckeditor_helper');
 
 
    //Ckeditor's configuration
    $this->data['ckeditor'] = array(
 
      //ID of the textarea that will be replaced
      'id'  =>  'content',
      'path'  =>  'js/ckeditor',
 
      //Optionnal values
      'config' => array(
        'toolbar'   =>  "Full",   //Using the Full toolbar
        'width'   =>  "550px",  //Setting a custom width
        'height'  =>  '100px',  //Setting a custom height
 
      ),
 
      //Replacing styles from the "Styles tool"
      'styles' => array(
 
        //Creating a new style named "style 1"
        'style 1' => array (
          'name'    =>  'Blue Title',
          'element'   =>  'h2',
          'styles' => array(
            'color'   =>  'Blue',
            'font-weight'   =>  'bold'
          )
        ),
 
        //Creating a new style named "style 2"
        'style 2' => array (
          'name'  =>  'Red Title',
          'element'   =>  'h2',
          'styles' => array(
            'color'     =>  'Red',
            'font-weight'     =>  'bold',
            'text-decoration' =>  'underline'
          )
        )       
      )
    );
 
    $this->data['ckeditor_2'] = array(
 
      //ID of the textarea that will be replaced
      'id'  =>  'content_2',
      'path'  =>  'js/ckeditor',
 
      //Optionnal values
      'config' => array(
        'width'   =>  "100%", //Setting a custom width
        'height'  =>  '100px',  //Setting a custom height
        'toolbar'   =>  array(  //Setting a custom toolbar
          array('Bold', 'Italic'),
          array('Underline', 'Strike', 'FontSize'),
          array('Smiley'),
          '/'
        )
      ),
 
      //Replacing styles from the "Styles tool"
      'styles' => array(
 
        //Creating a new style named "style 1"
        'style 3' => array (
          'name'    =>  'Green Title',
          'element'   =>  'h3',
          'styles' => array(
            'color'   =>  'Green',
            'font-weight'   =>  'bold'
          )
        )
 
      )
    );    
 
	}

	public function index(){
		$this->load->library('CKEditor');
    $this->load->library('CKFinder');
   
    //Add Ckfinder to Ckeditor
    $this->ckfinder->SetupCKEditor($this->ckeditor,'../../assets/ckfinder/');  
		$this->data['msg'] = $this->session->flashdata('msg');
       
        $this->data['emails'] = $this->Emailmodel->getallemails();
         //$this->data['useremails'] = $this->Emailmodel->getuseremails();
        
		$this->load->view('admin/email',$this->data);
	}
	public function expertEmail(){
		 $config['smtp_timeout'] = 5;
		 $config['mailtype'] = 'html';
  $config['charset'] = 'utf-8';
  $config['wordwrap'] = TRUE;
  $config['bcc_batch_mode'] = TRUE;  
  $config['bcc_batch_size'] = 1;
   $subject=$this->input->post('subject');
		$message=$this->input->post('textarea');
		$from_email = "abrar.bashir161mail.com"; 
     $email_to['email']=implode(',', $this->input->post('expert'));
      $this->load->library('email',$config); 
   if ($email_to <= 1){
         $this->email->from($from_email, 'seo expert'); 
         $this->email->to($email_to);
         //$this->email->bcc($email_to);
         $this->email->subject($subject); 
         $this->email->message($message); 
    if($this->email->send()) {
    
        redirect('admin/sendemail');
    }
         
         else {
         $this->session->set_flashdata("email_sent","Error in sending Email."); 
         //$this->load->view('email_form'); 
     }}
     else if ($email_to > 1){
         $this->email->from($from_email, 'seo expert'); 
         //this->email->to($email_to);
         $this->email->bcc($email_to);
         $this->email->subject('Email Test'); 
         $this->email->message($message); 
    if($this->email->send()) {
    
        redirect('admin/sendemail');
    }
         
         else {
         $this->session->set_flashdata("email_sent","Error in sending Email."); 
         //$this->load->view('email_form'); 
     }}
	}
	
}
?>
<?php 
class Adminlogin extends CI_Controller{

	public function index(){
 $this->load->helper('form');
       // $this->load->view('signup');
		$this->load->view('admin/adminlogin');
	}


    public function admin_log()
    {
        $this->load->library('form_validation');
        $this->form_validation->set_error_delimiters('<small class="form-text" style="color: #ff5e3a;">','</small>');

        $email = $this->input->post('email');
        $password = $this->input->post('password');

        if ($email == "" || $password == "")
        {
            $this->load->view('admin/adminlogin', validation_errors());
        }
        else{
            if ( $this->form_validation->run('login'))
            {                
                $this->load->model('Adminmodel');
                $login_id = $this->Adminmodel->admin_loginn($email, $password);
                if ( $login_id )
                {   
                    // echo 'login success';
                    //$this->session->set_userdata('admindata', $login_id);
                    //$data=$this->session->userdata('admindata');
                    //print_r($data->pic);

                    if($login_id['type']=='admin'){
                        $this->session->set_userdata('admin_id', $login_id['admin_id']);
                    $this->session->set_userdata('admin_username', $login_id['username']);

                   $this->session->set_userdata('email', $login_id['email']);
                    
                    $this->session->set_userdata('admin_profile', $login_id['pic']);
                   
                     $this->session->set_userdata('type', $login_id['type']);
                      $this->session->set_userdata('admin_login', true);
              
                   //$this->session->set_userdata('login', true);
                redirect('admin/dashboard');}
                else{

                     if($login_id['type']=='subadmin'){
                        $this->session->set_userdata('admin_id', $login_id['admin_id']);
                    $this->session->set_userdata('admin_username', $login_id['username']);

                   $this->session->set_userdata('email', $login_id['email']);
                    
                    $this->session->set_userdata('admin_profile', $login_id['pic']);
                     $this->session->set_userdata('type', $login_id['type']);
                     $this->session->set_userdata('admin_login', true);
              
                   //$this->session->set_userdata('login', true);
                redirect('./subadmin/subadminDashboard');}
                }
                }
                else 
                {
                    redirect('admin/adminlogin');
                   // redirect('messages', validation_errors());
                }
            }
        }
    }
    public function AdminLogout() {
        $this->session->set_userdata('admindata', null);
        redirect('admin/adminlogin/admin_log');
    }
} 
?>
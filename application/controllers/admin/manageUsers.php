<?php 

class ManageUsers extends CI_Controller
{
	
	var $data;
    
    public function __construct() 
    {
        parent::__construct();
      //  $this->load->model('adminusermodel');
        $this->load->model('Adminusermodel');
        //$this->load->model('DashboardModel');        
        $this->data = array();
    }
	
    public function index() 
    {
        $this->data['msg'] = $this->session->flashdata('msg');
       
        //$this->data['allusers'] = $this->adminusermodel->getallusers();
        if($query= $this->Adminusermodel->getallusers()){
             $this->data['allusers'] = $query ;
        }
        else{
            // $this->data['allusers'] = 'No data found';
        }

        // echo '<pre>';
        // print_r($this->data['allusers']);
       
        $this->load->view('admin/userdata', $this->data);
        
    }
    public function deleteUser($usertid) {

        if ($usertid == null) {
            redirect('admin/ManageUsers');
        } else {
            $r = $this->Adminusermodel->removeUser($usertid);
            if ($r) {
                $this->session->set_flashdata('msg', 'User Data Removed');
               redirect('admin/ManageUsers');
            }
           redirect('admin/ManageUsers');
    }

}
}

?>
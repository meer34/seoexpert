<?php
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * @package Upload :  CodeIgniter Upload Image with MySQL
 *
 * @author TechArise Team
 *
 * @email info@techarise.com
 *   
 * Description of Upload Controller
 */
if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Upload extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('Upload_model', 'upl');
    }
    // upload image
    public function index() {
        $data['page'] = 'upload-img';
        $data['title'] = 'Upoad Image | TechArise';         
        $this->load->view('admin/upload', $data);
    }    
    // action save method
    public function save() {       
        if ($this->input->post('upload_img')) {
            $path = 'assets/uploads';
            // Define file rules
            $config = array(
                "upload_path" => $path,
                "allowed_types" => "gif|jpg|jpeg|png|bmp",
                "remove_spaces" => TRUE
            );
             $this->load->library('upload', $config);
            $imagename = 'no-img.jpg';
            if (!$this->upload->do_upload('imageURL')) {
                $error = array('error' => $this->upload->display_errors());
                echo $this->upload->display_errors();
            } else {
                $data = $this->upload->data();
                $imagename = $data['file_name'];
                        }
            // create Thumbnail -- IMAGE_SIZES;
            $image_sizes = array('_mobile'=>array(75,75), '_thumb' => array(300, 200),'_medium' => array(500, 270), '_large' => array(750, 406));
            // load library
             $data = $this->upload->data();
            $this->load->library('image_lib');
            foreach ($image_sizes as $key=>$resize) {
                $config = array(
                    'source_image' => $data['full_path'],
                    'new_image' => ROOT_UPLOAD_PATH .'/'.$key,
                    'maintain_ratio' => FALSE,
                    'width' => $resize[0],
                    'height' => $resize[1],
                    'quality' =>70,
                );
                $this->image_lib->initialize($config);
                $this->image_lib->resize();
                $this->image_lib->clear();
            }            
            $this->load->library('image_lib', $config);
            $this->image_lib->resize();
            $this->upl->setURL($imagename);            
            $this->upl->create();                           
            $this->session->set_flashdata('img_uploaded_msg', '<div class="alert alert-success">Image uploaded successfully!</div>');
            $this->session->set_flashdata('img_uploaded', $imagename);
            // redirect('/');
        
    }
    }
}
?>
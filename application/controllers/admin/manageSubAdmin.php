<?php 
class ManageSubAdmin extends CI_Controller{
	var $data;
	public function __construct(){
		      parent::__construct();
		$this->load->model('SubAdminmodel');
              $this->load->model('TerminateModel');
		 $this->data = array();
		 $this->load->helper('url');
	}

	public function index(){
		   $this->data['msg'] = $this->session->flashdata('msg');

        $this->data['admindata'] = $this->SubAdminmodel->subadmin();
       
        $this->load->view('admin/include/header');
       

		$this->load->view('admin/managesubAdmin',$this->data);
	}
	 public function terminateSub_Admin($id)
    {
        // echo $id;
  
        $terminated = $this->TerminateModel->terminateSubAdmin($id);
        if ( $terminated )
            echo "SubAdmin terminated";
        else
            echo "SubAdmin not terminated";        
    }
    public function enableSub_Admin($id)
    {
       
        $enabled = $this->TerminateModel->enableSubAdmin($id);
        if( $enabled )
            echo "SubAdmin Enabled";
        else
            echo "SubAdmin not enabled";
    }
    public function deletedubadmin($admin_id){
        if ($admin_id == null) {
            redirect('admin/managesubAdmin');
        } else {
            $r = $this->SubAdminmodel->removesubadmin($admin_id);
            if ($r) {
                $this->session->set_flashdata('msg', 'SubAdmin Data Removed');
                redirect('admin/managesubAdmin');
                            }
           redirect('admin/managesubAdmin');
    }

    }

}

?>
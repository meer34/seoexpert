<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

class UserProfile extends CI_Controller
{
      var $data;
    public function __construct()
    {
        parent::__construct();
        $this->load->model('Usermodel');
          // $this->load->model('menus');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $config = [
            'upload_path' => './uploads',
            'allowed_types' => 'jpg|gif|jpeg|png'
        ];
        $this->load->library('upload', $config);
        $this->data = array();
        
        
    }

    public function index() 
    {
        $this->data['title'] = 'User Profile';
        $this->load->view('userprofile', $this->data);
    }

    // get and show user info in profile fields
    public function update($userId)
    {
        if ( $userId != null )
        {          
        //       $this->data['sidebar']=$this->menus->getallcategories();
        // $this->data['footer']=$this->menus->getallfooter(); 
            $ans = $this->Usermodel->getUserInfo($userId);

            if ( $ans )
            {              
                $this->data['id'] = $ans->user_id;
                $this->data['fname'] = $ans->fname;
                $this->data['lname'] = $ans->lname;
                $this->data['user_nick'] = $ans->user_nick;
                $this->data['user_email'] = $ans->user_email;
                $this->data['user_bd'] = $ans->user_bd;
                $this->data['user_phone'] = $ans->user_phone;
                $this->data['user_gender'] = $ans->user_gender;
                $this->data['country'] = $ans->country;
                $this->data['city'] = $ans->city;
                $this->data['state_province'] = $ans->state_province;
                $this->data['street'] = $ans->street;
                $this->data['profile_pic'] = $ans->profile_pic;
                $this->data['cover_pic'] = $ans->cover_pic;
                $this->data['title'] = $this->session->set_flashdata("title", $ans->fname.' '.$ans->lname.'\'s - Profile');
                $this->session->unset_userdata('profile');
                $this->session->set_userdata('profile', $this->data['profile_pic']);
                 
                $profile = $this->load->view('userprofile', $this->data);

            // $this->load->view('static/fixedSidebarLeft',$this->data);
            // $this->load->view('static/footer',$this->data);

            }
        }
    }

    // update user profile info
    public function user_edit()
    {
        $fname = trim($this->input->post('fname'));
        $lname = trim($this->input->post('lname'));
        $dob = $this->input->post('datetimepicker');
        $gender = $this->input->post('gender');
        $phone = trim($this->input->post('phone'));
        $country = trim($this->input->post('country'));
        $state = trim($this->input->post('state'));
        $city = trim($this->input->post('city'));
        $street = trim($this->input->post('street'));

        $userdata = [
            'fname' => $fname,
            'lname' => $lname,
            'user_phone' => $phone,
            'user_bd' => $dob,
            'user_gender' => $gender,
            'country' => $country,
            'state_province' => $state,
            'city' => $city,
            'street' => $street
        ];
        $user_id = $this->session->userdata('id');
        $result = $this->Usermodel->userupdate($user_id, $userdata);
        if( $result == true)
        {
            $this->session->unset_userdata('name');
            $this->session->set_userdata('name', $fname.' '.$lname);
            redirect('userprofile/update/'.$this->session->userdata('id'));
        }
    }

    // update user profile pic
    public function updateProfile()
    {
        if ( $this->upload->do_upload('profile_pic') )
        {           
            $data = $this->upload->data();
            $profile = 'uploads/'.$data['file_name'];
            $user_id = $this->session->userdata('id');
            $result = $this->Usermodel->userProfileUpdate($user_id, $profile);
            if ( $result == true )
            {
                $this->session->unset_userdata('profile');
                $this->session->set_userdata('profile', $this->data['profile_pic']);
                redirect('userprofile/update/'.$this->session->userdata('id'));
            }
                      
        } 
        else
        {
            $upload_error = $this->upload->display_errors();
            redirect('userprofile/update/'.$this->session->userdata('id'), compact('upload_error'));
        }
    }

    // update user cover pic
    public function updateCover()
    {
        if ( $this->upload->do_upload('cover_pic') )
        {
            $data = $this->upload->data();            
            $cover = 'uploads/'.$data['file_name'];
            $user_id = $this->session->userdata('id');
            $result = $this->Usermodel->userCoverUpdate($user_id, $cover);
            if ( $result == true)
            {
                redirect('userprofile/update/'.$this->session->userdata('id'));
            }
            else
            {
                $upload_error = $this->upload->display_errors();
                redirect('userprofile/update/'.$this->session->userdata('id'), compact('upload_error'));
            }
        }
    }

    // email verification
    public function emailVerification()
    {
        $verificationCode = $this->input->post('verificationCode');
        $email = $this->input->post('userEmail');
        $result = $this->Usermodel->Verification($verificationCode, $email);

        if( $result )
        {
            $this->session->unset_userdata('isVerified');
            $this->session->set_userdata('isVerified', 1);
            $this->data['title'] = $this->session->set_flashdata('title', 'Email Verification Success');
            $this->data['messages'] = $this->session->set_flashdata('messages', 'Your email is successfully verified, Enjoy full controll on your Account');
            $this->load->view('messages', $this->data);
        }
        else
        {
            $this->data['title'] = $this->session->set_flashdata('title', 'Email Verification Failed');
            $this->data['messages'] = $this->session->set_flashdata('messages', 'Incorrect Verification Code');
            $this->load->view('messages', $this->data);
        }
    }

}
<?php

class Rating extends CI_Controller
{

    public function __CONSTRUCT()
    {
        parent::__CONSTRUCT();
    }

    public function index()
    {
        $expert = $this->input->post('expert');
        $user = $this->input->post('user'); 
        $rating = $this->input->post('rating');
        $comment = $this->input->post('comment');

        $this->load->model('RatingModel');
       $rating= $this->RatingModel->rate($expert, $user, $rating, $comment);

    }

}

?>
<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Signup extends CI_Controller
{
    function __construct() 
    { 
        parent::__construct();
        $this->load->helper('form');        
        $this->load->library('form_validation'); 
        $data = [];
    } 
    public function index()
    {
       $this->data['title'] = $this->session->set_flashdata('title', 'User Signup');
       $this->load->view('signup', $this->data);
    }

    // user registration
    public function user_signup()
    {
        $this->form_validation->set_error_delimiters('<small class="form-text" style="color: #ff5e3a;">','</small>');

        if ( $this->form_validation->run('signup') ) 
        {
            $fname = trim($this->input->post('uFname'));
            $lname = trim($this->input->post('uLname'));
            $username = trim($this->input->post('uNname'));
            $email = trim($this->input->post('uEmail'));
            $phone = trim($this->input->post('uPhone'));
            $password = md5($this->input->post('uPassword'));
            $country = $this->input->post('country');
            $state = $this->input->post('state');
            $city = $this->input->post('city');
            $street = $this->input->post('street');
            $date = $this->input->post('datetimepicker');
            $gender = $this->input->post('uGender'); 
            $isExpert = $this->input->post('isExpet');
            $verificationCode = rand(100000,900000);

            $loadModel = $this->load->model('Signupmodel');

            if( $loadModel )
            {
                $data['mesg']=$this->Signupmodel->insert_data($fname, $lname, $username, $email, $phone, $password, $country, $state, $city, $street, $date, $gender,$verificationCode);
                 $config['smtp_timeout'] = 60;
                  $config['mailtype'] = 'html';
                  $config['charset'] = 'utf-8';
                  $config['wordwrap'] = TRUE;
                   $config['bcc_batch_mode'] = TRUE;  
                  $config['bcc_batch_size'] = 1;
                $from_email = "abrar.bashir161@gmail.com"; 
                $data['heading']="welcome to our site";
                $data['usermessage']='Hello '.$fname.' '.$lname.'. 
                
Thanks For Coming onboard with us. 

 Here\'s your verification code: '.$verificationCode;
                $email_to['email']=trim($this->input->post('email'));
                 $message=$this->load->view('usermessage',$data,TRUE);
                  $this->load->library('email',$config);             
                $this->email->from($from_email, 'seo expert'); 
                $this->email->to($email);
                $this->email->subject('Verification Email'); 
//                 $this->email->message('Hello '.$fname.' '.$lname.'. 
                
// Thanks For Coming onboard with us. 

// Here\'s your verification code: '.$verificationCode);
                $this->email->message($message);
                if( $this->email->send() ) 
                {
                   
                    $data['messages']=$this->session->set_flashdata("messages","Succesfully Registered <br /> A verification mail has been sent to ....".$email);                     
                    $this->load->view('messages',$data['messages']);
                }
                else 
                {
                    $data['messages']=$this->session->set_flashdata("messages","".$email." is not valid"); 
                    $this->load->view('messages',$data['messages']); 
                }
            }
            else
            {
                $$this->data['title'] = 'Failed';
                $this->data['msg'] = "Something went wrong, Please try again later";
                $this->load->view('messages', $this->data);
            }
        }
        else
        {
            $this->data['title'] = $this->session->set_flashdata('title','User Signup');
            $this->load->view('signup', $this->data, validation_errors());
        }
    }

    public function chk_form_data()
    {        
        $name = $this->input->post('username');
        $email = $this->input->post('email');
        
        $this->load->model('Signupmodel');

        $result = $this->Signupmodel->chk_form_data($name,$email);

        if( $result )
        {
            echo '<div style="color:red">Already Exists</div>';
        }
        else
        {
            echo '<div style="color:green">Avaliable</div>';
        }
    }    
}
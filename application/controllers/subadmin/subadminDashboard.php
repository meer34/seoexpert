<?php 
class subadminDashboard extends CI_Controller{

var $data;
    
    public function __construct() {
        parent::__construct();
        $this->load->model('Expertmodel');
        $this->load->model('DashboardModel');
        
        $this->data = array();
    }

	
	  public function index() {
        $this->data['msg'] = $this->session->flashdata('msg');
       
        $this->data['totalexpert'] = $this->DashboardModel->getexpertCount()->cc;
       $this->data['totalclient'] = $this->DashboardModel->getusertCount()->cc;
        $this->load->view('subadmin/include/header');
        $this->load->view('subadmin/subadminDashboard',$this->data);
        
    }
} 
?>
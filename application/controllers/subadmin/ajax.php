<?php

class Ajax extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
          $this->load->model('TerminateUserModel');
    }
    public function terminateUser($id)
    {
        // echo $id;
      
        $terminated = $this->TerminateUserModel->terminateUser($id);
        if ( $terminated )
            echo "User terminated";
        else
            echo "User not terminated";        
    }
    public function enableUser($id)
    {
       
        $enabled = $this->TerminateUserModel->enableUser($id);
        if( $enabled )
            echo "User Enabled";
        else
            echo "User not enabled";
    }

}

?>
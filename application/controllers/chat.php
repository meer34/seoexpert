<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Chat extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
        $data=[];
    }

    public function fetchMessages()
    {
        if (!$this->input->is_ajax_request()) {
           header('Location: '.base_url());
        }
        $room = $this->input->post('room');
        // echo $room;
        $this->load->model('MessageChat');
        $result = $this->MessageChat->fetchMessages($room);
        // echo $this->db->last_query();
        if( $result == false)
        {
            return false;
        }
        else
        {
            foreach($result as $row){
                    echo '<li>
                        <div class="author-thumb">
                            <!--<img src="" class="avatar" alt="author">-->
                            <div style="font-weight: bold">'.$row['fname'].': </div>
                        </div>
                        <div class="notification-event">
                            <span class="chat-message-item" title="'.$row['time'].'">'.$row['message'].'</span>
                        </div>
                    </li>';
            }
        }
    }

    public function throwMessages()
    {
        $room = $this->input->post('room');
        $sender = $this->input->post('sender');
        $message = $this->input->post('message');
        $this->load->model('MessageChat');
        $this->MessageChat->throwMessages($room, $sender, $message);
    }

}

?>
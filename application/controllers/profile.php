<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Profile extends CI_Controller
{
    var $data;

    public function __construct() 
    {
        parent::__construct();
        $this->load->model('Expertmodel');
        $this->load->model('Menus');
        $this->load->helper('form');
        $this->load->library('form_validation');
        $this->data = array();

        //  $this->data['sidebar']=$this->menus->getallcategories();
        // $this->data['footer']=$this->menus->getallfooter(); 
        //     $this->load->view('static/fixedSidebarLeft',$this->data);
        //     $this->load->view('static/footer',$this->data);
        // $this->data['sidebar'] = $this->menus->getallcategories();
        // $this->data['footer'] = $this->menus->getallfooter(); 
        // $this->load->view('static/fixedSidebarLeft',$this->data);
        // $this->load->view('static/footer',$this->data);

    }
    public function index()
    {
          
       $this->data['title'] = $this->session->set_flashdata('title','Edit Profile');
        $this->load->view('profile', $this->data);
      
        
    }

    // to show data in profile fields
    public function update($expertid)
    {
        if($expertid != null)
        {
            $ans['ans'] = $this->Expertmodel->getexpertinfo($expertid);

            $this->data['sidebar']=$this->Menus->getallcategories();
        $this->data['footer']=$this->Menus->getallfooter(); 
 $this->load->view('static/fixedSidebarLeft',$this->data);
            $this->load->view('static/footer',$this->data);
            $this->load->view('profile',$ans);
               


            $this->data['title'] = $this->session->set_flashdata('title','Edit Profile');
            $this->load->view('profile',$ans, $this->data);

            // if($ans)
            // {
            //     $data['msg'] = '';
            //     $this->data['id']=$ans->user_id;
            //     $this->data['fname'] = $ans->fname;
            //     $this->data['lname'] = $ans->lname;
            //     $this->data['user_nick'] = $ans->user_nick;
            //     $this->data['user_email'] = $ans->user_email;
            //     $this->data['user_phone'] = $ans->user_phone;
            //     $this->data['user_bd'] = $ans->user_bd;
            //     $this->data['user_gender'] = $ans->user_gender;
            //     $this->data['country'] = $ans->country;
            //     $this->data['city'] = $ans->city;
            //     $this->data['state_province'] = $ans->state_province;
            //     $this->data['street'] = $ans->street;               
            //     $this->data['profile_pic'] = $ans->profile_pic;
            //     $this->data['cover_pic'] = $ans->cover_pic;
                
                
            //       // $this->data['expert_info'] =explode(",", $ans->expert_info);
            //           $this->data['expert_info'] = $ans->expert_info;
            //         $this->data['short_desc'] = $ans->short_desc;
            //         $this->data['bio'] = $ans->bio;
                
            //     $this->session->unset_userdata('profile');
            //     $this->session->set_userdata('profile',$ans->profile_pic);
            //     $this->load->view('profile',$this->data);
            // }
            // else
            // {
            //     $this->session->set_flashdata('msg', 'Retry There is Some Problem');
            //     redirect('welcome');
            // }
        }
    }

    // update data in database
    public function expert_edit()
    {
      

            $fname = trim($this->input->post('fname'));
            $lname = trim($this->input->post('lname'));
            $username = trim($this->input->post('username'));
            $email = trim($this->input->post('email'));
            $uCpassword = md5(trim($this->input->post('uCpassword')));
            $dob = $this->input->post('datetimepicker');
            $city = $this->input->post('city');
            $country = $this->input->post('country');
            $street = $this->input->post('street');
            $phone = $this->input->post('phone');            
            $smallDisc = trim($this->input->post('smallDisc'));
            $longDisc = trim($this->input->post('longDisc'));                
            $gender = $this->input->post('gender');
            $state = $this->input->post('state');
            $pic_url_2 = $this->input->post('pic_url_2');
            $pic_url_3 = $this->input->post('pic_url_3');           
            $name = $_FILES["profile_pic"]["name"];
            $config['upload_path'] = './uploads/';
            $config['allowed_types'] =     'gif|jpg|png|jpeg';
            $this->load->library('upload', $config);
            $is_new_image = false;
            $imgurl = "";
            if ( ! $this->upload->do_upload('profile_pic') )
            {
                $error = array('error' => $this->upload->display_errors());
                $is_new_image = false;
            }
            else
            { 
                $upload_data=$this->upload->data();
                $is_new_image = true;
                $imgurl='uploads/'.$upload_data['file_name'];
            }
            $name1 = $_FILES["cover_pic"]["name"];
            // $config['upload_path'] = './uploads/';
            // $config['allowed_types'] =     'gif|jpg|png|jpeg';
            // $this->load->library('upload', $config);
            $is_new_image1 = false;
            $imgurl1 = "";
            if ( ! $this->upload->do_upload('cover_pic'))
            {
                $error = array('error' => $this->upload->display_errors());
                $is_new_image1 = false;
            }
            else
            {
                $upload_data=$this->upload->data();
                $is_new_image1 = true;
                $imgurl1='uploads/'.$upload_data['file_name'];
                // $img_path1 = 'uploads/'.$data['file_name'];
            }
            
           $expertdata=array(
                'fname'=>$fname,
                'lname'=>$lname,
                'user_nick'=>$username,
                'user_email'=>$email,
                'user_pass'=>$uCpassword,
                'user_bd'=>$dob,
                'city'=>$city,
                'country'=>$country,
                'street'=>$street,
                'user_phone'=>$phone,
               
               'short_desc'=>$smallDisc,
                'bio'=>$longDisc,
                
                'user_gender'=>$gender,
                'state_province'=>$state,
                'profile_pic'=>$imgurl,
                'cover_pic'=>$imgurl1            
            );
            if ($is_new_image == true) 
            {
                $expertdata['profile_pic'] = $imgurl;
            } else {
                $expertdata['profile_pic'] = $pic_url_2;
            }
            if ($is_new_image1 == true) {
                $expertdata['cover_pic'] = $imgurl1;
            } else {
                $expertdata['cover_pic'] = $pic_url_3;
            }
            $user_id=$this->session->userdata('id');
            $result= $this->Expertmodel->expertupdate($user_id,$expertdata);
            if($result == TRUE){
                redirect('Welcome');    
            } 
            else 
            {
                $data['valiadtion'] = validation_errors();
                $this->load->view('expersignup', $data);
            }
           

        
    
}
}
 



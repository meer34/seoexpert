<?php 

class UserModel extends CI_Model
{
    public function getUserInfo($userId)
    {
        $query = $this->db->select('*')->where('user_id', $userId)->get('users');
        if($query -> num_rows() == 1)
        {
           $result = $query->row();
           return $result;
        }
       else
            return null;
    }

    public function userupdate($user_id,$data)
    {
        $user_id=$this->session->userdata('id');
        $this->db->where('user_id', $user_id);
        $this->db->set('fname', $data['fname']);
        $this->db->set('lname', $data['lname']);
        $this->db->set('user_bd', $data['user_bd']);
        $this->db->set('city', $data['city']);
        $this->db->set('country', $data['country']);
        $this->db->set('street', $data['street']);
        $this->db->set('user_phone', $data['user_phone']);
        $this->db->set('user_gender', $data['user_gender']);
        $this->db->set('state_province', $data['state_province']);
        $r = $this->db->update('users');
        return $r;
    }

    public function userCoverUpdate($user_id, $cover)
    {
        $result = $this->db->where('user_id', $user_id)->set('cover_pic', $cover)->update('users');
        if ( $result )
            return true;
        else
            return false;
    }

    public function userProfileUpdate($user_id, $profile)
    {
        $result = $this->db->where('user_id', $user_id)->set('profile_pic', $profile)->update('users');
        if ( $result )
            return true;
        else
            return false;
    }

    public function Verification($verificationCode, $email)
    {
        $select = $this->db->select('*')->where(['user_email' => $email, 'verificationCode' => $verificationCode])->get('users');
        if( $select->num_rows() > 0 )
        {
            $update = $this->db->where(['user_email' => $email, 'verificationCode' => $verificationCode])->set('isVerified', 1)->update('users');
            return true;
        }
        else
        {
            return false;
        }
    }
}
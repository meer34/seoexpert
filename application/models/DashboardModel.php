<?php 
class DashboardModel extends CI_Model {
	public function getExpertCount(){
		//$query=$this->db->query('SELECT COUNT(*) AS cc FROM `users`');
		$query=$this->db->select('COUNT(*) as cc')
		                 ->where('isExpert',1)
		                 ->from('users')
						 ->get();
		if($query->num_rows()>0){
			$result=$query->row();
			return $result;
			}
			return null;
		}
		public function getusertCount(){
		//$query=$this->db->query('SELECT COUNT(*) AS cc FROM `users`');
		$query=$this->db->select('COUNT(*) as cc')
		                 ->where('isExpert',0)
		                 ->from('users')
						 ->get();
		if($query->num_rows()>0){
			$result=$query->row();
			return $result;
			}
			return null;
		}
		 public function getallexpert()
        {
                $this->db->from('users');
                $this->db->order_by("registration_date", "DESC");
                $this->db->limit(10);
                $query=$this->db->get();
                if ($query->num_rows() > 0) 
                {
                        return $query->result();
                }
                return null;
        }
        public function getallCount(){
		//$query=$this->db->query('SELECT COUNT(*) AS cc FROM `users`');
		$query=$this->db->select('COUNT(*) as cc')
		                 // ->where('isExpert',0)
		                 ->from('users')
						 ->get();
		if($query->num_rows()>0){
			$result=$query->row();
			return $result;
			}
			return null;
		}
	}
	 
?>
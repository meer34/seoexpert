<?php

class TerminateModel extends CI_Model
{
    public function terminateUser($id)
    {
        $query = $this->db->where('user_id', $id)->set('isTerminated', 1)->update('users');
        if( $query = true)
            return true;
        else
            return false; 
    }
    public function enableUser($id)
    {
        $query = $this->db->where('user_id', $id)->set('isTerminated', 0)->update('users');
        if( $query = true)
            return true;
        else
            return false;
    }
    ////// FOR TERMIATE SUB ADMIN//////////
     public function terminateSubAdmin($id)
    {
        $query = $this->db->where('admin_id', $id)->set('status', 1)->update('admin_login');
        if( $query = true)
            return true;
        else
            return false; 
    }
    public function enableSubAdmin($id)
    {
        $query = $this->db->where('admin_id', $id)->set('status', 0)->update('admin_login');
        if( $query = true)
            return true;
        else
            return false;
    }

}
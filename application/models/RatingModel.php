<?php

class RatingModel extends CI_Model
{
    public function rate($expert, $user, $rate, $comment)
    {
        $data = array(
            'expertId' => $expert,
            'clientId' => $user,
            'rating' => $rate,
            'review' => $comment
        );
        $query = $this->db->insert('rating', $data);
    }
}
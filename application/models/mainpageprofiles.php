<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MainPageProfiles extends CI_Model
{
    function __construct()
    {
        parent::__construct();
    }
    
    public function getProfiles()
    {
        $query = $this->db->select('user_id, fname, lname, profile_pic, short_desc, availability')
                            ->where_not_in('user_id', $this->session->userdata('id'))
                            ->where('isExpert', 1)
                            ->order_by('availability ASC')
                            ->limit(8)
                            ->get('users');
        if($query->num_rows() > 0)
            return $query->result_array();
        else
            return false;
    }

    public function getExpert($userId)
    {
        $query = $this->db->select('user_id, fname, lname, profile_pic, isExpert, availability')
                            ->where('user_id', $userId)
                            ->get('users');
        if( $query->num_rows() > 0 )
            return $query->row();
        else
            return false;
    }

    public function randomExperts()
    {
        $query = $this->db->select('user_id, profile_pic, fname, lname')
                            ->where_not_in('user_id', $this->session->userdata('id'))
                            ->where('isExpert', 1)
                            ->order_by('fname', 'RANDOM')
                            ->limit(2)
                            ->get('users');
        if( $query->num_rows() > 0 )
            return $query->result_array();
        else
            return false;
    }

}
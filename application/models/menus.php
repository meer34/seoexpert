<?php

class Menus extends CI_Model{

public function insert_header($headerdata){
   
     $this->db->where('heading', $headerdata['heading']);
      $this->db->where('header_img', $headerdata['header_img']);
       $this->db->where('paragraph', $headerdata['paragraph']);
    $query = $this->db->get('header');
    if($query->num_rows() >= 1)
    {
        // echo "already exist";
    }else{
         $insert=$this->db->insert('header',$headerdata);
    return $insert;
    }
	
}
public function getallheader(){
    $query=$this->db->select()->get('header');
      if ($query->num_rows() > 0) {
            return $query->result();
        }
        return null;
}
public function removeheader($id) {
        $this->db->where('id', $id);
        $this->db->delete('header');
        return true;
    }
    public function getheaderinfo($id){
        //$expertid=$this->session->userdata('id');
        $this->db->select('*');
        $this->db->where('id',$id);
        $query=$this->db->get('header');
        if($query->num_rows() > 0){
            $result=$query->row();
            return $result;
        }
        return null;

    }
    public function headerupdate($id,$data)
        {
               // $user_id=$this->session->userdata('id');
                $this->db->where('id', $id);
                 $this->db->set('heading', $data['heading']);
               $this->db->set('paragraph', $data['paragraph']);
                 $this->db->set('header_img', $data['header_img']);
               $r = $this->db->update('header');
                //sprint_r($r);
                return $r;
        }
 function sidemenu($category,$img_path) {
    $this->db->where('category', $category);
    $query = $this->db->get('side_bar');
    if($query->num_rows() >= 1)
    {
        // echo "already exist";
    }else{
        $userData = array(
               // 'name' => $this->input->post('name'),
                'category' => $category,
                'icon' => $img_path
            );
        $this->db->insert('side_bar', $userData);
        return true;
    }
    }

public function getallcategories(){
	$query=$this->db->select()->get('side_bar');
	  if ($query->num_rows() > 0) {
            return $query->result();
        }
        return null;
}
public function getallfooter(){
	$query=$this->db->select()->get('footer');
	  if ($query->num_rows() > 0) {
            return $query->result();
        }
        return null;
}
public function footer($footer){
     $this->db->where('footer_conditions', $footer);
    $query = $this->db->get('footer');
    if($query->num_rows() >= 1)
    {
        // echo "already exist";
    }else{
        $userData = array(
               // 'name' => $this->input->post('name'),
                'footer_conditions' => $footer,
                
            );
        $this->db->insert('footer', $userData);
        return true;
    }
	 // $this->db->insert('footer', $footer);
  //       return true;
}
public function removefooter($id) {
        $this->db->where('id', $id);
        $this->db->delete('footer');
        return true;
    }
    public function getfooterinfo($id){
        //$expertid=$this->session->userdata('id');
        $this->db->select('*');
        $this->db->where('id',$id);
        $query=$this->db->get('footer');
        if($query->num_rows() > 0){
            $result=$query->row();
            return $result;
        }
        return null;

    }
     public function footerupdate($id,$data)
        {
               // $user_id=$this->session->userdata('id');
                $this->db->where('id', $id);
                 $this->db->set('footer_conditions', $data['footer_conditions']);
               
               $r = $this->db->update('footer');
                //sprint_r($r);
                return $r;
        }
 public function removecategory($id) {
        $this->db->where('id', $id);
        $this->db->delete('side_bar');
        return true;
    }
    public function getcategoryinfo($id){
        //$expertid=$this->session->userdata('id');
        $this->db->select('*');
        $this->db->where('id',$id);
        $query=$this->db->get('side_bar');
        if($query->num_rows() > 0){
        	$result=$query->row();
        	return $result;
        }
        return null;

	}
	 public function categoryupdate($id,$data)
        {
               // $user_id=$this->session->userdata('id');
                $this->db->where('id', $id);
                 $this->db->set('category', $data['category']);
                $this->db->set('icon', $data['icon']);
               $r = $this->db->update('side_bar');
                //sprint_r($r);
                return $r;
        }

        /////////////
        
}
 ?>
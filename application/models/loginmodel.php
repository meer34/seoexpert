<?php

class LoginModel extends CI_Model
{
    public function login_valid($email, $password)
    {
        $password = md5($password);        

        $q = $this->db->where(['user_email'=>$email, 'user_pass'=> $password])->get('users');
        
        if( $q->num_rows() )
            return $q->row_array();
        else
            return FALSE;
    }
}
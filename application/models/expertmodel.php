<?php 
class Expertmodel extends CI_Model

{
    
        public function getallexpert()
        {
                $query=$this->db->where('isExpert',1)->get('users');
                if ($query->num_rows() > 0) 
                {
                        return $query->result();
                }
                return null;
        }

        public function getexpertinfo($expertid)
        {
        $expertid=$this->session->userdata('id');
        $this->db->select('*');
        $this->db->where('user_id',$expertid);
        $query=$this->db->get('users');
        if($query->num_rows() > 0)
        {
        	$result=$query->row();
        	return $result;
        }
                return null;
        }
        
        public function expertupdate($user_id,$data)
        {
                $user_id=$this->session->userdata('id');
                $this->db->where('user_id', $user_id);
                $this->db->set('fname', $data['fname']);
                $this->db->set('lname', $data['lname']);
                $this->db->set('user_nick', $data['user_nick']);
                $this->db->set('user_email', $data['user_email']);
                $this->db->set('user_pass', $data['user_pass']);
                $this->db->set('user_bd', $data['user_bd']);
                $this->db->set('city', $data['city']);
                $this->db->set('country', $data['country']);
                $this->db->set('street', $data['street']);
                $this->db->set('user_phone', $data['user_phone']);
                $this->db->set('short_desc', $data['short_desc']);
                $this->db->set('bio', $data['bio']);
                $this->db->set('user_gender', $data['user_gender']);
                $this->db->set('state_province', $data['state_province']);
                $this->db->set('profile_pic', $data['profile_pic']);
                $this->db->set('cover_pic', $data['cover_pic']);
                 $this->db->set('expert_info',implode(',', $this->input->post('optionsCheckboxes')));
                $r = $this->db->update('users');
                //sprint_r($r);
                return $r;
        }
        //  public function update_status($user_id)
        // {
        //         // if($this->session->userdata('isTerminate')==0){
        //         //         //$user_id=$this->session->userdata('id');
        //         // $this->db->where('user_id',138);
        //         // $this->db->set('isTerminate', 1);
                
        //         // $r = $this->db->update('users');
        //         // //sprint_r($r);
        //         // return $r;
        //         // }
        //         //  else if($this->session->userdata('isTerminate')==1){
        //                //$user_id=$this->session->userdata('id');
        //         $this->db->where('user_id', 138);
        //         $this->db->set('isTerminate', 0);
                
        //         $r = $this->db->update('users');
        //         //sprint_r($r);
        //         return $r;
        //         // }
               
        // }
        function approveExpert($expertid,$status) 
        {
                //$expertid=$this->session->userdata('id');
                $this->db->where('user_id', $expertid);
                $this->db->set('status', $status);
                $this->db->update('users');
                return true;
        }

        public function removeExpert($expertid) 
        {
                $this->db->where('user_id', $expertid);
                $this->db->delete('users');
                return true;
        }
     

        public function availability()
        {
                $user_id = $this->session->userdata('id');
                $this->db->where('user_id', $user_id);
                $this->db->set('availability', 1);
                $query = $this->db->update('users');
                if( $query )
                        return true;
                else
                        return false;
        }

        public function offline()
        {
                $user_id = $this->session->userdata('id');
                $this->db->where('user_id', $user_id);
                $this->db->set('availability', 3);
                $query = $this->db->update('users');
                if( $query )
                        return true;
                else
                        return false;
        }
}

?>
<?php 

class MessageChat extends CI_Model
{
    public function throwMessages($room, $sender, $message)
    {
        $data = array(
            'roomId' => $room,
            'senderId' => $sender,
            'message' => $message,
        );

        $this->db->insert('chat', $data);
    }

    public function fetchMessages($room)
    {
        $query = $this->db->select('*')
                    ->where('roomId', $room)
                    ->join('users','users.user_id = chat.senderId')
                    ->get('chat');

        if( $query->num_rows() > 0 )
            return $query->result_array();
        else
            return false;
    }

}

?>
<?php

class SignupModel extends CI_Model
{
    public function insert_data($fname, $lname, $username, $email, $phone, $password, $country, $state, $city, $street, $date, $gender,$verificationCode)
    {
        $data = array(
            'fname' => $fname,
            'lname' => $lname,
            'user_nick' => $username,
            'user_email' => $email,
            'user_phone' => $phone,
            'user_pass' => $password,
            'country' => $country,
            'state_province' => $state,
            'city' => $city,
            'street' => $street,
            'user_bd' => $date,
            'user_gender' => $gender,
            'verificationCode' => $verificationCode
        );

        $q = $this->db->insert('users', $data);

        if ($q)
            return TRUE;
        else
            return FALSE;
    }
    
    public function expert_register($isExpert,$fname,$lname,$email,$alt_email,$rpm,$phone,$uCpassword,$gender,$username,$dob,$smallDisc,$longDisc,$profile,$cover,$front,$back,$country,$state,$city,$street,$zip,$verificationCode)
    {
        $expertdata=array(
            'isExpert' => $isExpert,
            'fname'=>$fname,
            'lname'=>$lname,
            'user_email'=>$email,
            'user_alt_email'=>$alt_email,
            'rpm' => $rpm,
            'user_phone' => $phone,
            'user_pass'=>$uCpassword,
            'user_gender'=>$gender,
            'user_nick'=>$username,
            'user_bd'=>$dob,
            'short_desc'=>$smallDisc,
            'bio'=>$longDisc,
            'profile_pic'=>$profile,
            'cover_pic'=>$cover,
            'id_front_pic'=>$front,
            'id_back_pic'=>$back,
            'country'=>$country,
            'state_province'=>$state,
            'city'=>$city,
            'street'=>$street,
            'zipcode'=>$zip,
            'verificationCode'=>$verificationCode
            // 'expert_info'=>implode(',', $this->input->post('optionsCheckboxes'))
        );
        $query=$this->db->insert('users',$expertdata);
        if($query)
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
    
    public function chk_form_data($name,$email)
    {

        $query = $this->db->where('user_nick', $name)->or_where('user_email', $email)->get('users');

        if ( $query->num_rows() > 0)
            return TRUE;
        else
            return FALSE;

    }    
  
}
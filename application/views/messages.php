<?php include 'static/head.php'; ?>

<body class="landing-page">

<div class="content-bg-wrap">
	<div class="content-bg"></div>
</div>

<!-- Header Standard Landing  -->

<div class="header--standard header--standard-landing" id="header--standard">
	<div class="container">
		<div class="header--standard-wrap">
			<a href="#" class="logo">
				<div class="img-wrap">
					<img src="<?php echo base_url(); ?>assets/img/logo.png" alt="Olympus">
				</div>
				<div class="title-block">
					<h6 class="logo-title">olympus</h6>
					<div class="sub-title">SOCIAL NETWORK</div>
				</div>
			</a>
		</div>
	</div>
</div>

<!-- ... end Header Standard Landing  -->
<div class="header-spacer--standard"></div>

<div class="container">
	<div class="row display-flex">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="landing-content">	
				<?php if($msg = $this->session->flashdata('messages')): ?>		
					<h2 style="color: white"><?= $msg; ?></h2>
				<?php endif ?>
				<p style="color: white">You will be redirected to main page shortly or <a style="color:blue" href="<?= base_url(); ?>">click here</a> to jump now</p>
				<?php header('Refresh: 10;'.base_url()); ?>
			</div>
		</div>
	</div>
</div>

<?php include 'static/js.php' ?>
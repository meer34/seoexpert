<?php include_once('include/header.php');?>

<?php include_once('include/side_bar.php');?>

<?php
if(!isset($_SESSION['admin_login']) && !isset($_SESSION['admin_id']))
	{
		redirect('admin/adminlogin');
	}
 ?>



<!-- 11. $JQUERY_DATA_TABLES ===========================================================================

				jQuery Data Tables
-->
				<!-- Javascript -->
				<script>
					init.push(function () {
						$('#jq-datatables-example').dataTable();
						$('#jq-datatables-example_wrapper .table-caption').text('ALL EXPERTS');
						$('#jq-datatables-example_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
					});
				</script>
				<!-- / Javascript -->

				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title"></span>
					</div>
					<div class="panel-body">
						<div class="table-primary">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="jq-datatables-example">
								<thead>
									<tr>
										<th>Sr#</th>
										<th>Picture</th>
										<th>Name</th>
										<th>Username</th>
										<th>Email</th>
										<th>Contact</th>
										<th>Regisration time</th>
										<th>status</th>
										<th>Action</th>
										
									</tr>
								</thead>
								<tbody>
									<?php
									//$arr = (array)@$allexpert;
										if(!empty($allexpert)){ $i = 1; 

									foreach($allexpert as $row){ ?>
									<tr class="odd gradeX" id="<?php echo $row->user_id; ?>">
										<td><?php echo $i++; ?></td>
										<td><img src="<?php echo base_url() . $row->profile_pic; ?>" width="80" height="80" class="img-circle" /></td>
										<td><?php echo $row->fname; ?></td>
										<td><?php echo $row->user_nick; ?> </td>
										<td><?php echo $row->user_email; ?></td>
										<td><?php echo $row->user_phone; ?></td>
										<td><?php echo $row->registration_date; ?></td>
										<td>
											<form  action="<?php echo site_url(); ?>admin/manageExpert/approve" method="post" id="my_form">
												<input type="hidden" name="expert_id" value="<?php echo $row->user_id;?>">
												<input type="hidden" name="email" value="<?php echo $row->user_email;?>">
												<select  name="post_status">
													<option value="pending" <?php if ($row->status == 'pending') echo 'selected="selected"';?>>pending</option>
													<option value="Approved" <?php if ($row->status == 'Approved') echo 'selected="selected"';?>>Approved</option>
													<option value="Disapproved" <?php if ($row->status == 'Disapproved') echo 'selected="selected"';?>>Disapproved</option>
												</select>                                            
                                        </td>
                                        <td><input type="submit" name="submit" value="Approve" class="btn btn-success btn-xs btn-block">
                                            </form>
										<a href="<?php echo site_url('admin/manageExpert/deleteExpert/'.$row->user_id); ?>" class="btn btn-danger btn-xs btn-block" style="background-color: green;">
                                        <i class="fa fa-edit"></i> Delete</a> 
										<?php
											if( $row->isTerminated == 0 )
												echo '<button class="btn btn-danger btn-xs btn-block terminate" id="terminate"><i class="fa fa-times"></i>Terminate</button>';
											else
												echo '<button class="btn btn-success btn-xs btn-block enable" id="enable"><i class="fa fa-check"></i>Enable</button>';?>
										</td>		
									</tr>
									<?php
									}
								} else{?>
									
								<?php }
								?>
								</tbody>
							</table>
						</div>
					</div>
				</div>
<!-- /11. $JQUERY_DATA_TABLES -->

			</div>
		</div>

	</div> <!-- / #content-wrapper -->
	<div id="main-menu-bg"></div>
</div> <!-- / #main-wrapper -->

<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
	<script type="text/javascript"> window.jQuery || document.write('<script src="<?php echo base_url();?>assets/javascripts/jquery.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
	<script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">'+"<"+"/script>"); </script>
<![endif]-->


<!-- LanderApp's javascripts -->
<script src="<?php echo base_url();?>assets/javascripts/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/javascripts/landerapp.min.js"></script>

<script type="text/javascript">
	init.push(function () {
		// Javascript code here
	});
	window.LanderApp.start(init);
</script>
<script>
// 	$(document).ready(function(){
// // function approve_form_send(e)
// // {	j=1;
// // // 	var serializedData = $form.serialize();
// // // $("input[type=select]").change(function() {
// // //     $.ajax({//Submit the value of the drop_down
// // //         url: "admin/manageExpert/UpdateExpert/",
// // //         type: "post",
// // //         data: serializedData
// // //         }).done(function() {
// // //         alert('Updated!');
// // //     });
// // // });
// // if(j==1){
// // $.ajax({
// //                 type:'post',
// //                 url:'<?php //echo site_url(); ?>manageExpert/UpdateExpert',
// //                 data:$('form#'+e).serialize(),
// //                 success:function (data) {
// //                     alert(data);
// //                     //window.location.reload(true);
// //                 }
// //             });
// // }
// // 	});

// $("#my_form").submit(function(event){
//     event.preventDefault(); //prevent default action 
//     var post_url = $(this).attr("action"); //get form action url
//     var request_method = $(this).attr("method"); //get form GET/POST method
//     var form_data = $(this).serialize(); //Encode form elements for submission
    
//     $.ajax({
//         url : post_url,
//         type: request_method,
//         data : form_data
//     }).done(function(response){ //
//         $("#server-results").html(response);
//     });
// });
// });
</script>
<script>
$( document ).ready(function() 
{	
	function autoRefresh() {
        window.location.reload();
    }
	$(".terminate").on('click', function(e){
		var id = $(this).closest('tr').attr('id');
		if (confirm('Are you sure you want to Terminate this user?')) 
		{
			$.ajax({
				url: "ajax/terminateUser/"+id,			
				method: "POST",
				data: {id: id},
				success: function(data){
					autoRefresh();
				}
			})
		}
	});
	$(".enable").on('click', function(e){
		var id = $(this).closest('tr').attr('id');
		if (confirm('Are you sure you want to Enable this user?')) 
		{
			$.ajax({
				url: "ajax/enableUser/"+id,
				method: "POST",
				data: {id: id},
				success: function(data){
					autoRefresh();
				}
			});
		}
	});
});
</script>
</body>
</html>
<?php include_once('include/header.php');?>

<?php include_once('include/side_bar.php');?>


	<div id="content-wrapper">

		<div class="page-header">
			<h1><span class="text-light-gray">HEADER / </span>Settings</h1>
		</div> <!-- / .page-header -->

		<div class="row">
			<div class="col-sm-12">

<!-- 5. $JQUERY_VALIDATION =========================================================================

				jQuery Validation
-->
				<!-- Javascript -->
				<script>
					init.push(function () {
						$("#jq-validation-phone").mask("(999) 999-9999");
						$('#jq-validation-select2').select2({ allowClear: true, placeholder: 'Select a country...' }).change(function(){
							$(this).valid();
						});
						$('#jq-validation-select2-multi').select2({ placeholder: 'Select gear...' }).change(function(){
							$(this).valid();
						});

						// Add phone validator
						$.validator.addMethod(
							"phone_format",
							function(value, element) {
								var check = false;
								return this.optional(element) || /^\(\d{3}\)[ ]\d{3}\-\d{4}$/.test(value);
							},
							"Invalid phone number."
						);

						// Setup validation
						$("#jq-validation-form").validate({
							ignore: '.ignore, .select2-input',
							focusInvalid: false,
							rules: {
								'header_img': {
								  required: true,
								  header_img: true
								},
								'heading': {
								  required: true,
								  heading: true
								},
								'jq-validation-password': {
									required: true,
									minlength: 6,
									maxlength: 20
								},
								'jq-validation-password-confirmation': {
									required: true,
									minlength: 6,
									equalTo: "#jq-validation-password"
								},
								'jq-validation-required': {
									required: true
								},
								'jq-validation-url': {
									required: true,
									url: true
								},
								'jq-validation-phone': {
									required: true,
									phone_format: true
								},
								'jq-validation-select': {
									required: true
								},
								'jq-validation-multiselect': {
									required: true,
									minlength: 2
								},
								'jq-validation-select2': {
									required: true
								},
								'jq-validation-select2-multi': {
									required: true,
									minlength: 2
								},
								'paragraph': {
									required: true
								},
								'jq-validation-simple-error': {
									required: true
								},
								'jq-validation-dark-error': {
									required: true
								},
								'jq-validation-radios': {
									required: true
								},
								'jq-validation-checkbox1': {
									require_from_group: [1, 'input[name="jq-validation-checkbox1"], input[name="jq-validation-checkbox2"]']
								},
								'jq-validation-checkbox2': {
									require_from_group: [1, 'input[name="jq-validation-checkbox1"], input[name="jq-validation-checkbox2"]']
								},
								'jq-validation-policy': {
									required: true
								}
							},
							messages: {
								'jq-validation-policy': 'You must check it!'
							}
						});
					});
				</script>
				<!-- / Javascript -->

				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title">MAIN HEADER</span>
					</div>
					<div class="panel-body">
						
						
						<form class="form-horizontal" method="post" action="<?php echo base_url('admin/menu/header_data'); ?>" id="jq-validation-form">
							 <?php if($this->session->flashdata('add')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('add'); ?>
        </div>

    <?php } else if($this->session->flashdata('exist')){  ?>

        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error!</strong> <?php echo $this->session->flashdata('exist'); ?>
        </div>

    <?php }   ?>
							<div class="form-group">
								<label for="heading" class="col-sm-3 control-label">Heading</label>
								<div class="col-sm-9">
									<input type="hidden" class="form-control" id="id" name="id" placeholder="id">
									<input type="text" class="form-control" id="heading" name="heading" placeholder="heading">
								</div>
							</div>
							
							<div class="form-group">
							 <label for="email" class="col-sm-3 control-label">:</label>
                             <div class="col-sm-9">
                           
                           <!--  <table class="table table-striped" id="employee_table"> -->
                                
                            
                                <tr>
                                	<td>
                                		<input type="text" class="form-control" id="header_img" name="header_img" placeholder="image">
                                	</td>
                                   
                                   <!--  <th><label for="email">Slider:</label></th>
                                    <td><input type="text" placeholder="Enter Detail" id="slider" class="form-control"/></td>
                                     <td> <button type="button" class="btn btn-success btn-sm" onclick="javascript:add_employee_detail()"><span class="fa fa-plus"></span>
	                                    add</button></td> -->

	                                </tr>								
					</div>
				</div>
							

						

							

							<div class="form-group">
								<label for="paragraph" class="col-sm-3 control-label">Text</label>
								<div class="col-sm-9">
									<textarea class="form-control" name="paragraph" id="paraparagraphgraph"></textarea>
									
								</div>
							</div>

							<!-- To use simple error template, just add '.simple' class to the .form-group -->
							

							<!-- To use dark error template, just add '.dark' class to the .form-group -->

					</div>
					
					<!-- </table> -->
							
							

							<hr class="panel-wide">

							

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" name="submit" class="btn btn-success">add Header</button>
								</div>
							</div>
						</form>
					</div>
				</div>
<!-- /5. $JQUERY_VALIDATION -->


<!-- /6. $WIZARD_FORMS_VALIDATION -->

			</div>
		</div>
	</div> <!-- / #content-wrapper -->
	<div id="main-menu-bg"></div>
</div> <!-- / #main-wrapper -->

<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
	<script type="text/javascript"> window.jQuery || document.write('<script src="<?php echo base_url();?>assets/javascripts/jquery.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
	<script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">'+"<"+"/script>"); </script>
<![endif]-->


<!-- LanderApp's javascripts -->
<script src="<?php echo base_url();?>assets/javascripts/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/javascripts/landerapp.min.js"></script>

<script type="text/javascript">
	init.push(function () {
		// Javascript code here
	})
	window.LanderApp.start(init);
</script>
<script type="text/javascript">
// 	function add_employee_detail()
// {
// 	// var type = document.getElementById("type1").value;
// 	var slider = document.getElementById("slider").value;
// 	if( slider.length > 0)
// 	{
// 		var table=document.getElementById("employee_table");
// 		var table_len =  table.rows.length;
// 		var row = table.insertRow(table_len).outerHTML='<tr id="row_id'+table_len+'">\n' +
// 			// '<th><label for="email">Type:</label></th>\n' +
// 			// '<td><input type="text" readonly name="type[]" class="form-control" value="'+type+'"/></td>\n' +
// 			'<th><label for="email">Detail:</label></th>\n' +
// 			'<td><input type="text" name="slider[]" class="form-control" value="'+slider+'"/></td>\n' +
// 			'<td><button type="button" class="btn btn-danger" onclick="javascript:delete_company_detail('+table_len+')"><span class="fa fa-trash"></span>Remove</button></td>\n' +
// 			'</tr>';
// 		// document.getElementById("type1").value = '';
// 		document.getElementById("slider").value = '';
// 		document.getElementById("slider").focus();
// 	}
// 	else
// 	{
// 		// if(type == '')
// 		// {
// 		// 	$('#type1').css("border","2px solid red");
// 		// }
// 		if(slider == '')
// 		{
// 			$('#slider').css("border","2px solid red");
// 		}
		
// 	}
	
// } 

// function delete_company_detail(e)
// {
// document.getElementById("row_id"+e+"").outerHTML="";    
// }

</script>
</body>

</html>
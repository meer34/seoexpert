<!-- 4. $MAIN_MENU =================================================================================

		Main menu
		
		Notes:
		* to make the menu item active, add a class 'active' to the <li>
		  example: <li class="active">...</li>
		* multilevel submenu example:
			<li class="mm-dropdown">
			  <a href="#"><span class="mm-text">Submenu item text 1</span></a>
			  <ul>
				<li>...</li>
				<li class="mm-dropdown">
				  <a href="#"><span class="mm-text">Submenu item text 2</span></a>
				  <ul>
					<li>...</li>
					...
				  </ul>
				</li>
				...
			  </ul>
			</li>
-->
	<div id="main-menu" role="navigation">
		<div id="main-menu-inner">
			<div class="menu-content top" id="menu-content-demo">
				<!-- Menu custom content demo
					 Javascript: html/assets/demo/demo.js
				 -->
				<div>
					<div class="text-bg">

						<span class="text-slim">Welcome,</span> <span class="text-semibold"><?php echo $this->session->userdata('admin_username'); ?></span></div>
                              <?php ?>
					<img src="<?php echo base_url().$this->session->userdata('admin_profile');?>" alt="picture" class="">
					<div class="btn-group">
						<a href="#" class="btn btn-xs btn-primary btn-outline dark"><i class="fa fa-envelope"></i></a>
						<a href="#" class="btn btn-xs btn-primary btn-outline dark"><i class="fa fa-user"></i></a>
						<a href="#" class="btn btn-xs btn-primary btn-outline dark"><i class="fa fa-cog"></i></a>
						<a href="#" class="btn btn-xs btn-danger btn-outline dark"><i class="fa fa-power-off"></i></a>
					</div>
					<a href="#" class="close">&times;</a>
				</div>
			</div>
			<ul class="navigation">
				<li class="active">
					<a href="<?php echo site_url('admin/Dashboard'); ?>"><i class="menu-icon fa fa-dashboard"></i><span class="mm-text">Dashboard</span></a>
				</li>
				<!-- <li class="mm-dropdown">
					<a href="#"><i class="menu-icon fa fa-th"></i><span class="mm-text">Layouts</span></a>
					<ul>
						<li>
							<a tabindex="-1" href="layouts-grid.html"><span class="mm-text">Grid</span></a>
						</li>
						<li>
							<a tabindex="-1" href="layouts-main-menu.html"><i class="menu-icon fa fa-th-list"></i><span class="mm-text">Main menu</span></a>
						</li>
					</ul>
				</li> -->
				<li>
					<a href="<?php echo site_url('admin/manageExpert'); ?>"><i class="menu-icon fa fa-tasks"></i><span class="mm-text">Expert User</span></a>
				</li>
				<li>
					<a href="<?php echo site_url('admin/manageUsers');?>"><i class="menu-icon fa fa-flask"></i><span class="mm-text">Client User </span></a>
				</li>
				<li class="mm-dropdown">
					<a href="#"><i class="menu-icon fa fa-desktop"></i><span class="mm-text">Sub Admin</span></a>
					<ul>
						<li>
							<a tabindex="-1" href="<?php echo site_url('admin/ManageSubAdmin');?>"><span class="mm-text">Manage Sub Admin</span></a>
						</li>
						
						
						
						
					</ul>
				</li>
				<li class="mm-dropdown">
					<a href="#"><i class="menu-icon fa fa-check-square"></i><span class="mm-text">Email</span></a>
					<ul>
						<li>
							<a tabindex="-1" href="<?php echo site_url('admin/sendemail');?>"><span class="mm-text">Send Email To Experts</span></a>
						</li>
						<li>
							<a tabindex="-1" href="<?php echo site_url('admin/useremail');?>"><span class="mm-text">Send Email To Users</span></a>
						</li>
						<li>
							<a tabindex="-1" href="<?php echo site_url('admin/useremail');?>"><span class="mm-text">Send Email To Users</span></a>
						</li>
						
						
						
					</ul>
				</li>
				
				
				<li class="mm-dropdown">
					<a href="#"><i class="menu-icon fa fa-check-square"></i><span class="mm-text">Menu Level</span></a>
					<ul>
						
						 <li class="mm-dropdown">
							<a tabindex="-1" href=""><span class="mm-text">Header</span></a>
							<ul>
								<li>
							<a tabindex="-1" href="<?php echo site_url('admin/menu');?>"><span class="mm-text">Add Header</span></a>
                     
                       

						</li>
								<li>
							<a tabindex="-1" href="<?php echo site_url('admin/menu/manage_header_view');?>"><span class="mm-text">Manage Header</span></a>
                     
                       

						</li>
								
							</ul>
						</li>
							

                            <li class="mm-dropdown">
							<a tabindex="-1" href=""><span class="mm-text">Side Bar</span></a>
							<ul>
								<li>
									<a tabindex="-1" href="<?php echo site_url('admin/menu/side_bar');?>"><span class="mm-text">Add Side Bar</span></a>
								</li>
								<li>
									<a tabindex="-1" href="<?php echo site_url('admin/menu/manage_side_bar_view');?>"><span class="mm-text">Manage Side Bar</span></a>
								</li>
								
							</ul>
						</li>


						
						 <li class="mm-dropdown">
							<a tabindex="-1" href=""><span class="mm-text">Footer</span></a>
							<ul>
								<li>
									<a tabindex="-1" href="<?php echo site_url('admin/menu/footer_view');?>"><span class="mm-text"> Add Footer</span></a>
								</li>
								<li>
									<a tabindex="-1" href="<?php echo site_url('admin/menu/manage_footer_view');?>"><span class="mm-text">Manage Footer</span></a>
								</li>
								
							</ul>
						</li>
						
						
					</ul>
				</li>
			<div class="menu-content">
				<a href="pages-invoice.html" class="btn btn-primary btn-block btn-outline dark">Create Invoice</a>
			</div>
		</div> <!-- / #main-menu-inner -->
	</div> <!-- / #main-menu -->
<!-- /4. $MAIN_MENU -->
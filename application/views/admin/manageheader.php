<?php include_once('include/header.php');?>

<?php include_once('include/side_bar.php');?>



<?php
if(!isset($_SESSION['admin_login']) && !isset($_SESSION['admin_id']))
	{
		redirect('admin/adminlogin');
	}
 ?>



<!-- 11. $JQUERY_DATA_TABLES ===========================================================================

				jQuery Data Tables
-->
				

				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title"></span>
					</div>
					<div class="panel-body">
						<div class="table-primary">
							 <?php if($this->session->flashdata('add')){ ?>
        <div class="alert alert-success">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Success!</strong> <?php echo $this->session->flashdata('add'); ?>
        </div>

    <?php } else if($this->session->flashdata('exist')){  ?>

        <div class="alert alert-danger">
            <a href="#" class="close" data-dismiss="alert">&times;</a>
            <strong>Error!</strong> <?php echo $this->session->flashdata('exist'); ?>
        </div>

    <?php }   ?>
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="jq-datatables-example">
								<thead>
									<tr>
										<th>Sr.</th>
										
										<th> Heading</th>

										<th> Paragraph</th>
										<th> pic</th>
										
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php 
									$i=1;
                                      foreach($header as $headerdata){
									 ?>
									 <tr>
									 		<td><?php echo $i++;?></td>
									 		
									 	<td><?php echo $headerdata->heading;?></td>
									 	<td><?php echo $headerdata->paragraph;?></td>
									 	<td><img src="<?php echo base_url('menus/').$headerdata->header_img; ?>" height="40px" width="40px"></td>
                                       <td><a href="<?php echo site_url('admin/menu/deleteheader/'.$headerdata->id); ?>" class="btn btn-danger btn-xs btn-block" style="background-color: green;">
                                        <i class="fa fa-edit"></i> Delete</a>
                                        <a href="<?php echo site_url('admin/menu/updateheader/'.$headerdata->id); ?>" class="btn btn-success btn-xs btn-block" style="background-color: green;">
                                        <i class="fa fa-edit"></i> Edit</a>

                                    </td>
									 </tr>
									<?php }  ?>
								</tbody>
								</table>							
						</div>
					</div>
				</div>
<!-- /11. $JQUERY_DATA_TABLES -->

			</div>
		</div>

	</div> <!-- / #content-wrapper -->
	<div id="main-menu-bg"></div>
</div> <!-- / #main-wrapper -->

<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
	<script type="text/javascript"> window.jQuery || document.write('<script src="<?php echo base_url();?>assets/javascripts/jquery.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
	<script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">'+"<"+"/script>"); </script>
<![endif]-->


<!-- LanderApp's javascripts -->
<script src="<?php echo base_url();?>assets/javascripts/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/javascripts/landerapp.min.js"></script>

<script type="text/javascript">
	init.push(function () {
		// Javascript code here
	})
	window.LanderApp.start(init);
</script>


				<!-- / Javascript -->
</body>
</html>
<?php include_once('include/header.php');?>

<?php include_once('include/side_bar.php');?>
<style type="text/css">
	.multiselect {
  width: 800px;
}

.selectBox {
  position: relative;
}

.selectBox select {
  width: 100%;
  font-weight: bold;
}

.overSelect {
  position: absolute;
  left: 0;
  right: 0;
  top: 0;
  bottom: 0;
}

#checkboxes {
  display: none;
  border: 1px #dadada solid;
}

#checkboxes label {
  display: block;
}

#checkboxes label:hover {
  
}
</style>



	<div id="content-wrapper">

		<div class="page-header">
			<h1><span class="text-light-gray">EXPERT / </span>Emails</h1>
		</div> <!-- / .page-header -->

		<div class="row">
			<div class="col-sm-12">

<!-- 5. $JQUERY_VALIDATION =========================================================================

				jQuery Validation
-->
				<!-- Javascript -->
				<script>
					init.push(function () {
						$("#jq-validation-phone").mask("(999) 999-9999");
						$('#jq-validation-select2').select2({ allowClear: true, placeholder: 'Select a country...' }).change(function(){
							$(this).valid();
						});
						$('#expertemail').select2({ placeholder: 'Select expert email' }).change(function(){
							$(this).valid();
						});

						// Add phone validator
						$.validator.addMethod(
							"phone_format",
							function(value, element) {
								var check = false;
								return this.optional(element) || /^\(\d{3}\)[ ]\d{3}\-\d{4}$/.test(value);
							},
							"Invalid phone number."
						);

						// Setup validation
						$("#jq-validation-form").validate({
							ignore: '.ignore, .select2-input',
							focusInvalid: false,
							rules: {
								'jq-validation-email': {
								  required: true,
								  email: true
								},
								'jq-validation-password': {
									required: true,
									minlength: 6,
									maxlength: 20
								},
								'jq-validation-password-confirmation': {
									required: true,
									minlength: 6,
									equalTo: "#jq-validation-password"
								},
								'jq-validation-required': {
									required: true
								},
								'jq-validation-url': {
									required: true,
									url: true
								},
								'jq-validation-phone': {
									required: true,
									phone_format: true
								},
								'jq-validation-select': {
									required: true
								},
								'ExpertEmails': {
									required: true,
									minlength: 2
								},
								'jq-validation-select2': {
									required: true
								},
								'expertemail': {
									required: true,
									minlength: 2
								},
								'jq-validation-text': {
									required: true
								},
								'jq-validation-simple-error': {
									required: true
								},
								'jq-validation-dark-error': {
									required: true
								},
								'jq-validation-radios': {
									required: true
								},
								'jq-validation-checkbox1': {
									require_from_group: [1, 'input[name="jq-validation-checkbox1"], input[name="jq-validation-checkbox2"]']
								},
								'jq-validation-checkbox2': {
									require_from_group: [1, 'input[name="jq-validation-checkbox1"], input[name="jq-validation-checkbox2"]']
								},
								'jq-validation-policy': {
									required: true
								}
							},
							messages: {
								'jq-validation-policy': 'You must check it!'
							}
						});
					});
				</script>
				<!-- / Javascript -->


				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title">Experts</span>
					</div>
					<div class="panel-body">
						
						
						<form method="post" action="<?php echo base_url('admin/sendemail/expertEmail') ?>" class="form-horizontal" id="jq-validation-form">
						
							<!-- <div class="form-group">
								<label for="expertemail" class="col-sm-3 control-label">Select2 Multiple</label>
								<div class="col-sm-9">
									<select class="form-control" name="expertemail[]" id="expertemail" multiple="multiple">
										<optgroup label="Expert emails">
											<?php //foreach($emails as $data){ ?>
											<option value="<?php //echo $data->user_email; ?>"><?php //echo $data->user_email; ?></option>
											<?php// }?>
										</optgroup>
										
									</select>
									
								</div>
							</div> -->
								<!-- <div class="form-group">
								<label for="ExpertEmails" class="col-sm-3 control-label">Select Expert Email</label>
								<div class="col-sm-9">
									<select class="form-control"  name="ExpertEmails[]" id="ExpertEmails" multiple="multiple">
										<optgroup label="Expert emails">
											<?php // foreach($emails as $data){ ?>
											<option value="<?php // echo $data->user_email; ?>"><?php // echo $data->user_email; ?></option>
											<?php //}?>
										</optgroup>
										
										
									</select>
								</div>
							</div>
						 -->


							

							<div class="form-group">
								
								

							  <div class="col-sm-9 multiselect" >
							 <div class="selectBox" onclick="showCheckboxes()">
						      <select class="form-control">
						        <option> Select an option</option>
						      </select>
						      <div class="overSelect"></div>
						    </div>
						    <div id="checkboxes">
						    	 <label for="one">
						    	<input type="checkbox" id="select_all">  select all
							<?php if(!empty($emails))
										{ foreach ($emails as $row) {
								?>
							<input type="checkbox" class="checkbox"  name="expert[]" value="<?php echo $row->user_email ;?>"> <?php echo $row->user_email ; ?>
						
                           <?php } 
                       }else{}?>

							      </div>
							    </div>
																
								
							</div>
							<div class="form-group">
								<div class=" col-sm-9 " >
									 <input type="text" name="subject" class="form-control" placeholder="enter subject">
								</div>
							</div>
							
                            
                             <div class="form-group">
								<div class=" col-sm-9 " >
									<h3>Description</h3>
									    <textarea name="textarea" id="textarea"> </textarea>
								</div>
							</div>
                         
							

							<div class="form-group">
								<div class="col-sm-offset-3 col-sm-9">
									<button type="submit" class="btn btn-success">Send Email</button>
								</div>
							</div>
						</form>
					</div>
				</div>
<!-- /5. $JQUERY_VALIDATION -->


<!-- /6. $WIZARD_FORMS_VALIDATION -->

			</div>
		</div>
	</div> <!-- / #content-wrapper -->
	<div id="main-menu-bg"></div>
</div> <!-- / #main-wrapper -->

<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
	<script type="text/javascript"> window.jQuery || document.write('<script src="<?php echo base_url();?>assets/javascripts/jquery.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
	<script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">'+"<"+"/script>"); </script>
<![endif]-->


<!-- LanderApp's javascripts -->
<script src="<?php echo base_url();?>assets/javascripts/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/javascripts/landerapp.min.js"></script>

<script type="text/javascript">
	init.push(function () {
		// Javascript code here
	})
	window.LanderApp.start(init);
</script>
<script type="text/javascript">
	
	$(document).ready(function(){

$('#ExpertEmails').click(function() {
    $('#ExpertEmails option').prop('selected', true);
});

	});
</script>
<script type="text/javascript">
	CKEDITOR.replace('textarea');
</script>
<script type="text/javascript">
	$(document).ready(function(){
    

//select all checkboxes
$("#select_all").change(function(){  //"select all" change 
    var status = this.checked; // "select all" checked status
    $('.checkbox').each(function(){ //iterate all listed checkbox items
        this.checked = status; //change ".checkbox" checked status
    });
});

$('.checkbox').change(function(){ //".checkbox" change 
    //uncheck "select all", if one of the listed checkbox item is unchecked
    if(this.checked == false){ //if this item is unchecked
        $("#select_all")[0].checked = false; //change "select all" checked status to false
    }
    
    //check "select all" if all checkbox items are checked
    if ($('.checkbox:checked').length == $('.checkbox').length ){ 
        $("#select_all")[0].checked = true; //change "select all" checked status to true
    }
});

	});
</script>
<script type="text/javascript">
	var expanded = false;

function showCheckboxes() {
  var checkboxes = document.getElementById("checkboxes");
  if (!expanded) {
    checkboxes.style.display = "block";
    expanded = true;
  } else {
    checkboxes.style.display = "none";
    expanded = false;
  }
}
</script>
</body>

</html>
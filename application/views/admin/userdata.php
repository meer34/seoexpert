<?php include_once('include/header.php');?>

<?php include_once('include/side_bar.php');?>



<?php
if(!isset($_SESSION['admin_login']) && !isset($_SESSION['admin_id']))
	{
		redirect('admin/adminlogin');
	}
 ?>



<!-- 11. $JQUERY_DATA_TABLES ===========================================================================

				jQuery Data Tables
-->
				

				<div class="panel">
					<div class="panel-heading">
						<span class="panel-title"></span>
					</div>
					<div class="panel-body">
						<div class="table-primary">
							<table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="jq-datatables-example">
								<thead>
									<tr>
										<th>Sr.</th>
										<th>Pic</th>
										<th>First Name</th>
										<th>Last Name</th>
										<th>Username</th>
										<th>Email Address</th>
										<th>DOB</th>
										<th>Registration Date</th>
										<th>Gender</th>
										<th>Actions</th>
									</tr>
								</thead>
								<tbody>
									<?php
									//$arr = (array)$allusers;
										if(!empty($allusers))
										{ 
											$i = 1;
											foreach ($allusers as $users) 
											{											
												echo '<tr align="center" id = "'.$users->user_id.'" >';
													echo '<td align="center">'.$i.'</td>';
													echo '<td><img src="'.base_url().$users->profile_pic.'" alt="profile" width="80px" heioght="80px" style="border-radius: 100%;"></td>';
													echo '<td>'.$users->fname.'</td>';
													echo '<td>'.$users->lname.'</td>';
													echo '<td>'.$users->user_nick.'</td>';
													echo '<td>'.$users->user_email.'</td>';
													echo '<td>'.$users->user_bd.'</td>';
													echo '<td>'.$users->registration_date.'</td>';
													echo '<td>'.$users->user_gender.'</td>';?>                          
													<td>

													<a href="<?php echo site_url('admin/manageUsers/deleteUser/'.$users->user_id); ?>" class="btn btn-danger btn-xs btn-block" style="background-color: green;">
														<i class="fa fa-edit"></i> Delete</a>
													<?php
													if( $users->isTerminated == 0 )
														echo '<button class="btn btn-danger btn-xs btn-block terminate" id="terminate"><i class="fa fa-times"></i>Terminate</button>';
													else
														echo '<button class="btn btn-success btn-xs btn-block enable" id="enable"><i class="fa fa-check"></i>Enable</button>';?>
													</td>
												<?php												
												echo '</tr>';
												$i++;
											}
										}
										else
										{ 
											
										}
									?>
									</table>
								</tbody>							
						</div>
					</div>
				</div>
<!-- /11. $JQUERY_DATA_TABLES -->

			</div>
		</div>

	</div> <!-- / #content-wrapper -->
	<div id="main-menu-bg"></div>
</div> <!-- / #main-wrapper -->

<!-- Get jQuery from Google CDN -->
<!--[if !IE]> -->
	<script type="text/javascript"> window.jQuery || document.write('<script src="<?php echo base_url();?>assets/javascripts/jquery.min.js">'+"<"+"/script>"); </script>
<!-- <![endif]-->
<!--[if lte IE 9]>
	<script type="text/javascript"> window.jQuery || document.write('<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js">'+"<"+"/script>"); </script>
<![endif]-->


<!-- LanderApp's javascripts -->
<script src="<?php echo base_url();?>assets/javascripts/bootstrap.min.js"></script>
<script src="<?php echo base_url();?>assets/javascripts/landerapp.min.js"></script>

<script type="text/javascript">
	init.push(function () {
		// Javascript code here
	})
	window.LanderApp.start(init);
</script>

<script>
$( document ).ready(function() 
{	
	function autoRefresh() {
        window.location.reload();
    }
	$(".terminate").on('click', function(e){
		var id = $(this).closest('tr').attr('id');
		if (confirm('Are you sure you want to Terminate this user?')) 
		{
			$.ajax({
				url: "ajax/terminateUser/"+id,			
				method: "POST",
				data: {id: id},
				success: function(data){
					autoRefresh();
				}
			})
		}
	});
	$(".enable").on('click', function(e){
		var id = $(this).closest('tr').attr('id');
		if (confirm('Are you sure you want to Enable this user?')) 
		{
			$.ajax({
				url: "ajax/enableUser/"+id,
				method: "POST",
				data: {id: id},
				success: function(data){
					autoRefresh();
				}
			});
		}
	});
});
</script>
<!-- Javascript -->
<script>
					init.push(function () {
						$('#jq-datatables-example').dataTable();
						$('#jq-datatables-example_wrapper .table-caption').text('ALL Users');
						$('#jq-datatables-example_wrapper .dataTables_filter input').attr('placeholder', 'Search...');
					});
				</script>
				<!-- / Javascript -->
</body>
</html>
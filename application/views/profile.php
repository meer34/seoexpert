
<?php
	if(!isset($_SESSION['login']) && !isset($_SESSION['id']))
	{
		redirect('welcome');
	}
	if($this->session->userdata('isVerified') == "0")	
	{
		echo 'An email has been sent to you with verification code, Please Verifie your Email';
		echo form_open('userprofile/emailVerification');
			echo '<div class="form-group label-floating">';
				echo '<label class="control-label">Verification Code: </label>';
				echo form_input(['class'=>'form-control','type'=>'hidden', 'name'=>'userEmail', 'value' => $this->session->userdata('email')]);
				echo form_input(['class'=>'form-control','type'=>'number', 'name'=>'verificationCode', 'placeholde' => 'code']);
				echo form_submit(['name'=>'register','value'=>'Registration','class'=>'btn btn-primary btn-lg full-width']);
			echo '</div>';
		echo '</form>';
		die;
	}
	include 'static/head.php';
	include 'static/fixedSidebarLeft.php';
	include 'static/navHeader.php';
	include 'static/fixedSidebarRight.php';
?>

<div style="margin-top:20px;"></div>

<!-- Top Header-Profile -->

<div class="container">
	<div class="row">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="top-header">
					<div class="top-header-thumb">
						<?php echo form_open('profile/expert_edit','class = "container" enctype="multipart/form-data"'); ?>
						<img src="<?php echo base_url().$ans->cover_pic;?>	" alt="nature" class="cover-pic" width="100%" height="640px">
					</div>
					<input type="hidden" name="pic_url_3" value="<?php echo $ans->cover_pic;?>">
					<?= form_upload(['class'=>'', 'name'=>'cover_pic', 'id'=>'upload1','value'=>base_url().$ans->cover_pic]);  ?>
					<div class="profile-section">
						<div class="row">
							<div class="col-lg-5 col-md-5 ">
							</div>
						</div>
					</div>
					<div class="top-header-author">

						<a href="02-ProfilePage.html" class="author-thumb">							
							<img src="<?php echo base_url().$ans->profile_pic;?>" alt="nature" class="img-fluid" width="100%" height="100%">
						</a>
							<input type="hidden" name="pic_url_2" value="<?php echo $ans->profile_pic;?>">
							<?= form_upload(['class'=>'', 'name'=>'profile_pic', 'id'=>'upload','value'=>base_url().$ans->profile_pic]);  ?>							  
						<div class="author-content">
							<?php echo '<a href="#" class="h4 author-name">'.$_SESSION['name'].'</a>' ?>							
							<div class="country">San Francisco, CA</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">        
		<div class="col-xl-12 order-xl-2 col-lg-12 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title" >Personal Information</h6>
				</div>
				<div class="ui-block-content">					
					<!-- Personal Information Form  -->					
						<div class="row">					
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-group label-floating">
									<label class="control-label">First Name</label>									
									<?= form_input(['class'=>'form-control','type'=>'hidden','name'=>'isExpert','value'=>'1']); ?>
									<?= form_input(['class'=>'form-control', 'name'=>'fname', 'value' => $ans->fname
									  ]); ?>
									<?= form_error('fname'); ?>
								</div>
					 			<div class="form-group label-floating">
									<label class="control-label">Username</label>
									
										<?= form_input(['class'=>'form-control','name'=>'username','value'=>$ans->user_nick]); ?>
										<?= form_error('username'); ?>
								</div>
								<div class="form-group label-floating">
									<label class="control-label">Your Password</label>
									<?= form_password(['class'=>'form-control','name'=>'uPassword','value'=>set_value('uPassword')]); ?>
									<?= form_error('uPassword'); ?>
								</div>
								<div class="form-group date-time-picker label-floating">
									<label class="control-label">Your Birthday</label>
									<?= form_input(['class'=>'form-control', 'name'=>'datetimepicker', 'value'=>$ans->user_bd]); ?>
									<?= form_error('datetimepicker'); ?>
									<span class="input-group-addon">
										<svg class="olymp-month-calendar-icon icon"><use xlink:href="<?= base_url();?>assets/svg-icons/sprites/icons.svg#olymp-month-calendar-icon"></use></svg>
									</span>
								</div>
								<div class="form-group label-floating">
									<label class="control-label">Enter your phone number</label>
									<?= form_input(['class'=>'form-control','id'=>'phone','value'=>$ans->user_phone, 'type'=>'tel', 'name'=>'phone']); ?>
									<?= form_error('phone'); ?>
								</div>
							</div>
					
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-group label-floating">
									<label class="control-label">Last Name</label>
									<?=  form_input(['class'=>'form-control', 'name'=>'lname','value'=>$ans->lname]); ?>
									<?= form_error('lname'); ?>
								</div>
								<div class="form-group label-floating">
									<label class="control-label">Your Email</label>
									<?= form_input(['class'=>'form-control', 'name'=>'email','type'=>'email','value'=>$ans->user_email]); ?>
									<?= form_error('email'); ?>
								</div>
								
								<div class="form-group label-floating">
									<label class="control-label">Password Confirm</label>
									<?= form_password(['class'=>'form-control','name'=>'uCpassword','value'=>set_value('uCpassword')]); ?>
									<?= form_error('uCpassword'); ?>
								</div>
                                <div class="form-group label-floating is-select">
									<label class="control-label">Your Gender</label>
									  <select name = "gender" class = "" >
                                        <option <?php echo ($ans->user_gender == 'M') ? "selected = 'selected'" : ""; ?> value = "Male">Male</option>
                                        <option <?php echo ($ans->user_gender == 'F') ? "selected = 'selected'" : ""; ?>  value = "Female">Female</option>
                                    </select>
									<?php
										
										echo form_error('gender');
									?>
								</div>
							</div>
					
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-group label-floating">
									<label class="control-label">Write a little description about you (100 max)</label>
									<?= form_textarea(['class'=>'form-control','name'=>'smallDisc','rows'=>'5','maxlength'=>'100',
									'value'=>$ans->short_desc]); ?>
									<?= form_error('smallDics'); ?>
								</div>
                            </div>
                        
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<div class="form-group label-floating">
									<label class="control-label">Write a detailed description about you</label>
									<?= form_textarea(['class'=>'form-control','name'=>'longDisc','rows'=>'5','maxlength'=>'500','value'=>$ans->bio]); ?>
									<?= form_error('longDisc'); ?>
								</div>
							</div>
							
						</div>
					
					<!-- ... end Personal Information Form  -->
				</div>
			</div>
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Address</h6>
				</div>
				<div class="ui-block-content">

					
					<!-- Personal Information Form  -->
						<div class="row">
					
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="form-group label-floating">
								<label class="control-label">Select your country</label>

								
									
									<?= form_input(['class'=>'form-control', 'name'=>'country','type'=>'text','value'=>$ans->country,'list'=>'list']); ?>
									<?= form_error('country'); ?>
									<datalist id="list">
										
										<option>pakistan</option>
										<option>pakistan</option>
									</datalist>
									
							
								</div>
					
								<div class="form-group label-floating">
									<label class="control-label">Your City</label>
									
									<?= form_input(['class'=>'form-control', 'name'=>'city','type'=>'text','value'=>$ans->city,'list'=>'citylist']); ?>
									<?= form_error('city'); ?>
									<datalist id="citylist">
										
										<option>lhr</option>
										<option>grw</option>
									</datalist>
								</div>
							</div>
					
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					
							<div class="form-group label-floating">
								<label class="control-label">Your State / Province</label>
								<?= form_input(['class'=>'form-control', 'name'=>'state','type'=>'text','value'=>$ans->state_province,'list'=>'statelist']); ?>
									<?= form_error('state'); ?>
									<datalist id="statelist">
										
										<option>punjab</option>
										<option>kpk</option>
									</datalist>
							</div>

							<div class="form-group label-floating">
								<label class="control-label">Street</label>
								<?= form_input(['class'=>'form-control','name'=>'street','value'=>$ans->street]) ?>
								<?= form_error('street'); ?>
							</div>
					
							</div>
						</div>
					
					<!-- ... end Personal Information Form  -->
				</div>
			</div>


            <div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title" >Expert information</h6>
				</div>
				<div class="ui-block-content">

					<p>The following payment method are available to you on based on your countery and currency setting.</p>
					<!-- Personal Information Form  -->
						<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
					
							<div class="checkbox">
								<label>

									<?= form_checkbox(['name'=>'optionCheckboxes[]']); ?>
									<strong>Select All</strong>
									<?= form_error('optionCheckboxes[]'); ?>
								</label>
							</div>
						
							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="row">
							
								<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<div class="checkbox">
								<label>
									
                               <?php  ?>
									<?php 

									//$aColors = array("Love and Relationship", "Tarot and Cards", "Dream Interpretation");
//foreach(explode(",", $ans->expert_info) as $expert){
										$expert = explode(",", $ans->expert_info);
										// print_r($expert);
										// foreach($aColors as $value){
									?>
										
							<?php 

							// foreach(explode(',', $ans->expert_info) as $value){
       //                           echo $value;
							// // 	// // if(in_array('Love and Relationship', $value)){
							//  	echo "checked";
							// 	// 	//}
								// }
							//	echo $expert_info;
//print_r($expert_info);


								//if(in_array('Love and Relationship', $value)){
							
										//echo "checked";
							 if(in_array("Love and Relationship",$expert)) {

							?>
								<input type="checkbox"  name="optionsCheckboxes[]"  value="Love and Relationship" checked size="17"  >	Love and Relationship
									
										<?php }	else{//}?>
										<input type="checkbox"  name="optionsCheckboxes[]"  value="Love and Relationship"  size="17"  >	Love and Relationship
										<?php }?>
									
								</label>
							</div>
									<div class="checkbox">
								<label>
									 
 	

									
									
									<?php if(in_array("Tarot and Cards",$expert)) {

							?>
								<input type="checkbox"  name="optionsCheckboxes[]"  value="Tarot and Cards" checked size="17"  >	Tarot and Cards
									
										<?php }	else{//}?>
										<input type="checkbox"  name="optionsCheckboxes[]"  value="Tarot and Cards"  size="17"  >	Tarot and Cards
										<?php }?>
								</label>
							</div>
									<div class="checkbox">
								<label>
									
									<?php if(in_array("Tarot and Cards",$expert)) {

							?>
								<input type="checkbox"  name="optionsCheckboxes[]"  value="Dream Interpretation" checked size="17"  >	Dream Interpretation
									
										<?php }	else{//}?>
										<input type="checkbox"  name="optionsCheckboxes[]"  value="Dream Interpretation"  size="17"  >	Dream Interpretation
										<?php }?>
								</label>
							</div>
							<?php// }?>
								</div>
								<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
								
							<div class="checkbox">
								<label>
									
									<?php if(in_array("Astorolgy",$expert)) {

							?>
								<input type="checkbox"  name="optionsCheckboxes[]"  value="Astorolgy" checked size="17"  >	Astorolgy
									
										<?php }	else{//}?>
										<input type="checkbox"  name="optionsCheckboxes[]"  value="Astorolgy"  size="17"  >	Astorolgy
										<?php }?>
								</label>
									</div>
									<div class="checkbox">
								<label>
									
									<?php if(in_array("Palm Reading",$expert)) {

							?>
								<input type="checkbox"  name="optionsCheckboxes[]"  value="Palm Reading" checked size="17"  >	Palm Reading
									
										<?php }	else{//}?>
										<input type="checkbox"  name="optionsCheckboxes[]"  value="Palm Reading"  size="17"  >	Palm Reading
										<?php }?>
								</label>
							</div>
									<div class="checkbox">
								<label>
									
									<?php if(in_array("Spiritual Guides",$expert)) {

							?>
								<input type="checkbox"  name="optionsCheckboxes[]"  value="Spiritual Guides" checked size="17"  >	Spiritual Guides
									
										<?php }	else{//}?>
										<input type="checkbox"  name="optionsCheckboxes[]"  value="Spiritual Guides"  size="17"  >	Spiritual Guides
										<?php }?>
								</label>
							</div>
								</div>
								 <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<div class="checkbox">
								<label>
									
										<?php if(in_array("Rituals and Energies",$expert)) {

							?>
								<input type="checkbox"  name="optionsCheckboxes[]"  value="Rituals and Energies" checked size="17"  >	Rituals and Energies
									
										<?php }	else{//}?>
										<input type="checkbox"  name="optionsCheckboxes[]"  value="Rituals and Energies"  size="17"  >	Rituals and Energies
										<?php }?>
								</label>
							</div>
									<div class="checkbox">
								<label> 
									
									<?php if(in_array("Home and family",$expert)) {

							?>
								<input type="checkbox"  name="optionsCheckboxes[]"  value="Home and family" checked size="17"  >	Home and family
									
										<?php }	else{//}?>
										<input type="checkbox"  name="optionsCheckboxes[]"  value="Home and family"  size="17"  >	Home and family
										<?php }?>
								</label>
							</div>
									<div class="checkbox">
								<label>
									
									<?php if(in_array("Numerology",$expert)) {

							?>
								<input type="checkbox"  name="optionsCheckboxes[]"  value="Numerology" checked size="17"  >	Numerology
									
										<?php }	else{//}?>
										<input type="checkbox"  name="optionsCheckboxes[]"  value="Numerology"  size="17"  >	Numerology
										<?php }?>
									<?php// } ?> 
								</label>
							</div>
								</div>
						</div>

						</div>					
					 
					<!-- ... end Personal Information Form  -->
				</div>
			</div>
            <!--<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Payment Method</h6>
				</div>
				<div class="ui-block-content">
                    <p>The following payment method are available to you on based on your countery and currency setting.</p>-->

					
					<!-- Personal Information Form  -->
					
					
                       <!-- 
                        <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
							<h6>type of payment</h6>
                            <div class="row">
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
							<div class="radio">
								<label>
									<?php // form_radio(['name'=>'optionsRadios','id'=>'bank']) ?>
								     Bank 
								</label>
							</div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
							<div class="radio">
								<label>
									<?php //form_radio(['name'=>'optionsRadios','id'=>'w_union']) ?>
								     Westron Union 
								</label>
							</div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
							<div class="radio">
								<label>
									<?php //form_radio(['name'=>'optionsRadios','id'=>'paypal']) ?>
								
									paypal
								</label>
							</div>
                                </div>
                                <div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
							<div class="radio">
								<label>
									<?php //form_radio(['name'=>'optionsRadios','id'=>'payoneer']) ?>
									
									payoneer
								</label>
							</div>
                                </div>
                            </div>
						</div>
                    <div class="bank">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
									<label class="control-label">Bank Name</label>
										<?php //form_input(['class'=>'form-control','name'=>'bank','type'=>'email','id'=>'bank','value'=>set_value('bank')]) ?>
										<?php //echo form_error('bank'); ?>
									
								</div>
						
                                
					
                        </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
									<label class="control-label">IBAN(International Bank Account Number)*</label>
									<?php //form_input(['class'=>'form-control','name'=>'account_number','id'=>'account_number','value'=>set_value('account_number')]) ?>
										<?php// echo form_error('account_number'); ?>
									<input class="form-control" placeholder="" type="email" value="151585624-25">
								</div>
						
					
                        </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
									<label class="control-label">Swift Code*</label>
									<input class="form-control" placeholder="" type="email" value="6240025">
								</div>
						
					
                        </div>
                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                            <div class="remember">
										<div class="checkbox">
											<label>
												<input name="optionsCheckboxes" type="checkbox">
												I accept the <a href="#">Terms and Conditions</a> of the website
											</label>
										</div>
									</div>
                            </div>-->
					<!--country code with flag-->
							
							
							
							<!--<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<button class="btn btn-secondary btn-lg full-width">Restore all Attributes</button>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<button class="btn btn-primary btn-lg full-width">Save all Changes</button>
							</div>
                                </div>
</div>
                    
                    
                    <div class="w_union">
                        <div class="row">
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
									<label class="control-label">Enter Your Name*</label>
									<input class="form-control" placeholder="" type="email" value="abcde...">
								</div>
						
                                
					
                        </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
									<label class="control-label">Phone Number*</label>
									<input class="form-control" placeholder="" type="email" value="+923049239011">
								</div>
						
					
                        </div>
                            <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
                                <div class="form-group label-floating">
									<label class="control-label">ID Card Number*</label>
									<input class="form-control" placeholder="" type="email" value="8562412415841-25">
								</div>
						
					
                        </div>
                             <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

                            <div class="remember">
										<div class="checkbox">
											<label>
												<input name="optionsCheckboxes" type="checkbox">
												I accept the <a href="#">Terms and Conditions</a> of the website
											</label>
										</div>
									</div>
                            </div>-->
					<!--country code with flag-->
							
							
							<!--
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<button class="btn btn-secondary btn-lg full-width">Restore all Attributes</button>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<button class="btn btn-primary btn-lg full-width">Save all Changes</button>
							</div>
                                </div>
</div>
                    
                    
                    
                    
                    <div class="paypal">
                            
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 offset-md-2">
                                <div class="form-group label-floating">
									<label class="control-label">paypal ID*</label>
									<input class="form-control" placeholder="" type="email" value="mail@paypal.com">
								</div>
						
					
                        </div>
                             <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 offset-md-2">

                            <div class="remember">
										<div class="checkbox">
											<label>
												<input name="optionsCheckboxes" type="checkbox">
												I accept the <a href="#">Terms and Conditions</a> of the website
											</label>
										</div>
									</div>
                            </div>-->
					<!--country code with flag-->
							
							<!--<div class="row">
							
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<button class="btn btn-secondary btn-lg full-width">Restore all Attributes</button>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<button class="btn btn-primary btn-lg full-width">Save all Changes</button>
							</div>
                                </div>
</div>
                    
                    
                    
                    
                    <div class="payoneer">
                            
                            <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 offset-md-2">
                                <div class="form-group label-floating">
									<label class="control-label">payoneer ID*</label>
									<input class="form-control" placeholder="" type="email" value="mail@payoneer.com">
								</div>
						
					
                        </div>
                             <div class="col-lg-8 col-md-8 col-sm-12 col-xs-12 offset-md-2">

                            <div class="remember">
										<div class="checkbox">
											<label>
												<input name="optionsCheckboxes" type="checkbox">
												I accept the <a href="#">Terms and Conditions</a> of the website
											</label>
										</div>
									</div>
                            </div>-->
					<!--country code with flag-->
							<!--
							<div class="row">
							
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<button class="btn btn-secondary btn-lg full-width">Restore all Attributes</button>
							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
								<button class="btn btn-primary btn-lg full-width">Save all Changes</button>
							</div>
                                </div>
                               
</div>-->

				<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
					<?= form_submit(['name'=>'register','value'=>'Registration','class'=>'btn btn-primary btn-lg full-width'])?>
				</div>

							
						</form>



					
					<!-- ... end Personal Information Form  -->
				</div>
			</div>
		</div>
	</div>
</div>

<!-- ... end Your Account Personal Information -->

<a class="back-to-top" href="#">
	<img src="<?= base_url(); ?>assets/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>

<?php include 'static/footer.php'; ?>

<!-- JS Scripts -->
    <script>
$(document).ready(function(){
    $("#flip").click(function(){
        $("#panel").slideDown("slow");
    });
		$(".bank").hide();
        $(".w_union").hide();
        $(".paypal").hide();
        $(".payoneer").hide(); 
    $("#bank").click(function(){
        $(".bank").show();
        $(".w_union").hide();
        $(".paypal").hide();
        $(".payoneer").hide();
    });
    $("#w_union").click(function(){
        $(".bank").hide();
        $(".w_union").show();
        $(".paypal").hide();
        $(".payoneer").hide();
    });
    $("#paypal").click(function(){
        $(".bank").hide();
        $(".w_union").hide();
        $(".paypal").show();
        $(".payoneer").hide();
    });
    $("#payoneer").click(function(){
        $(".bank").hide();
        $(".w_union").hide();
        $(".paypal").hide();
        $(".payoneer").show();
    });
});
</script>
<script src="<?php echo base_url(); ?>assets/js/intlTelInput.js"></script>
  <script>
    $("#phone").intlTelInput({
      allowDropdown: false,
      autoHideDialCode: false,
      autoPlaceholder: "off",
      dropdownContainer: "body",
      excludeCountries: ["us"],
      formatOnDisplay: false,
      geoIpLookup: function(callback) {
        $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
          var countryCode = (resp && resp.country) ? resp.country : "";
          callback(countryCode);
        });
      },
      hiddenInput: "full_number",
      initialCountry: "auto",
      nationalMode: false,
      onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      placeholderNumberType: "MOBILE",
      preferredCountries: ['cn', 'jp'],
      separateDialCode: true,
      utilsScript: "<?php echo xbase_url(); ?>assets/js/utils.js"
    });
  </script>
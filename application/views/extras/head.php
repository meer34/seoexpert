<!DOCTYPE html>
<html lang="en">
<head>

	<title>Friend Groups</title>

	<!-- Required meta tags always come first -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/Bootstrap/dist/css/bootstrap-reboot.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/Bootstrap/dist/css/bootstrap.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/Bootstrap/dist/css/bootstrap-grid.css') ?>">

	<!-- Main Styles CSS -->
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/main.min.css') ?>">
	<link rel="stylesheet" type="text/css" href="<?= base_url('assets/css/fonts.min.css') ?>">

	<!-- Main Font -->
	<script src="<?= base_url('assets/fonts/fontawesome.min.js') ?>"></script>
	<script src="<?= base_url('assets/fonts/fontawesome-all.min.js') ?>"></script>
	<script src="<?= base_url() ?>assets/js/webfontloader.min.js"></script>
	<script>
		WebFont.load({
			google: {
				families: ['Roboto:300,400,500,700:latin']
			}
		});
	</script>

	<style>
		.icon{
			font-size: 20px;
		}
		.icon:hover{
			color: #ff5e3a;
			font-size: 20px;
		}
	</style>

</head>
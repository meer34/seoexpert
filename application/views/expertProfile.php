<?php
	include 'static/head.php';
	include 'static/fixedSidebarLeft.php'; 
	include 'static/navHeader.php';
	include 'static/fixedSidebarRight.php';
?>

<div class="header-spacer"></div>


<div class="container">
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12 p_m_work">
    <div class="row">
        <div class="col-xl-9 col-lg-9 col-md-12 col-sm-12 col-xs-12 p_m_work">
            
			<div class="post-thumb p_m_work">
				<div class="blog-post-v3 featured-post-item img_blog">
				<picture>
				<img src="<?= base_url() ?>assets/img/hijab-3064633_1920.jpg" class="img-fluid img-thumbnail img-responsive" alt="img">
				</picture>
				<a href="#" class="post-category bg-blue">Blance:00</a>
				</div>
			</div>
            
            <div class="row p_m_work">
            <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 p_m_work btn-block-mine">
                <a href="#" class="btn btn-purple btn-sm btn-block">Small Button</a>
            </div>
                <div class="col-lg-6 col-md-6 col-sm-3 col-xs-3 p_m_work btn-block-mine">
                <a href="#" class="btn btn-yellow btn-sm btn-block">Small Button</a>
            </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 p_m_work btn-block-mine">
                <a href="#" class="btn btn-grey btn-sm btn-block">Small Button</a>
            </div>
                <div class="col-lg-2 col-md-2 col-sm-3 col-xs-3 p_m_work btn-block-mine">
                <a href="#" class="btn btn-purple btn-sm btn-block">Small Button</a>
            </div>
            </div>
        
        </div>
        <div class="col-xl-3 col-lg-3 col-md-12 col-sm-12 col-xs-12 p_m_work">
        <div class="blog-post-v3 featured-post-item p_m_work  d-xs-none d-sm-none d-md-none d-lg-block">
                    <div class="post-thumb blog_cam_chat p_m_work picture_blog">
							<picture>
                                <img src="<?= base_url(); ?>assets/img/hijab-3064633_1920.jpg" class="img-fluid img-thumbnail d-md-none d-lg-block" alt="img">
                            </picture>
							<a href="#" class="post-category bg-yellow">close</a>
					</div>
                    <div class="post-thumb blog_cam_chat p_m_work picture_blog">
							<picture>
                                <img src="<?= base_url(); ?>assets/img/hijab-3064633_1920.jpg" class="img-fluid img-thumbnail" alt="img">
                            </picture>
							<a href="#" class="post-category bg-yellow">close</a>
					</div>
            </div>
            <div class="blog-post-v3 featured-post-item">

			
			<!-- Popup Chat -->
			<div class="ui-block popup-chat l popup-chat-mine">
				<div class="ui-block-title">
					<span class="icon-status online"></span>
					<h6 class="title">Mathilda Brinker</h6>
					<div class="more">
						<svg class="olymp-three-dots-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
						<svg class="olymp-little-delete"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-little-delete"></use></svg>
					</div>
				</div>
				<div class="mCustomScrollbar" data-mcs-theme="dark">
					<ul class="notification-list chat-message chat-message-field">
						<li>
							<div class="author-thumb">
								<img src="<?= base_url(); ?>assets/img/avatar14-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<span class="chat-message-item">Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks</span>
								<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:10pm</time></span>
							</div>
						</li>
			
						<li>
							<div class="author-thumb">
								<img src="<?= base_url(); ?>assets/img/author-page.jpg" alt="author">
							</div>
							<div class="notification-event">
								<span class="chat-message-item">Don’t worry Mathilda!</span>
								<span class="chat-message-item">I already bought everything</span>
								<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:29pm</time></span>
							</div>
						</li>
			
						<li>
							<div class="author-thumb">
								<img src="<?= base_url(); ?>assets/img/avatar14-sm.jpg" alt="author">
							</div>
							<div class="notification-event">
								<span class="chat-message-item">Hi James! Please remember to buy the food for tomorrow! I’m gonna be handling the gifts and Jake’s gonna get the drinks</span>
								<span class="notification-date"><time class="entry-date updated" datetime="2004-07-24T18:18">Yesterday at 8:10pm</time></span>
							</div>
						</li>
					</ul>
				</div>
			
				<form>
			
					<div class="form-group label-floating is-empty">
						<label class="label_controal">Press enter to post...</label>
						<input type="text" class="form-control form_height" placeholder=""  >
						<div class="add-options-message">
							<a href="#" class="options-message">
								<svg class="olymp-computer-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-computer-icon"></use></svg>
							</a>
							<div class="options-message smile-block">
			
								<svg class="olymp-happy-sticker-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-happy-sticker-icon"></use></svg>
			
								<ul class="more-dropdown more-with-triangle triangle-bottom-right">
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat1.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat2.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat3.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat4.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat5.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat6.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat7.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat8.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat9.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat10.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat11.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat12.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat13.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat14.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat15.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat16.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat17.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat18.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat19.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat20.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat21.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat22.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat23.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat24.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat25.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat26.png" alt="icon">
										</a>
									</li>
									<li>
										<a href="#">
											<img src="<?= base_url(); ?>assets/img/icon-chat27.png" alt="icon">
										</a>
									</li>
								</ul>
							</div>
						</div>
					</div>
			
				</form>
			
			
			</div>
			
			<!-- ... end Popup Chat -->
			

		</div>
        </div>
    </div>
    </div>
    
    
    
    
	<div class="row">

		<!-- Main Content -->

		<main class="col-xl-6 order-xl-2 col-lg-12 order-lg-1 col-md-12 col-sm-12 col-xs-12">

			<div class="ui-block">
				
				<!-- News Feed Form  -->
				
				<div class="news-feed-form">
					<!-- Nav tabs -->
					<ul class="nav nav-tabs" role="tablist">
						<li class="nav-item">
							<a class="nav-link active inline-items" data-toggle="tab" href="#home-1" role="tab" aria-expanded="true">
				
								<svg class="olymp-status-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-status-icon"></use></svg>
				
								<span>Status</span>
							</a>
						</li>
						<li class="nav-item">
							<a class="nav-link inline-items" data-toggle="tab" href="#profile-1" role="tab" aria-expanded="false">
				
								<svg class="olymp-multimedia-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-multimedia-icon"></use></svg>
				
								<span>Multimedia</span>
							</a>
						</li>
				
						<li class="nav-item">
							<a class="nav-link inline-items" data-toggle="tab" href="#blog" role="tab" aria-expanded="false">
								<svg class="olymp-blog-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-blog-icon"></use></svg>
				
								<span>Blog Post</span>
							</a>
						</li>
					</ul>
				
					<!-- Tab panes -->
					<div class="tab-content">
						<div class="tab-pane active" id="home-1" role="tabpanel" aria-expanded="true">
							<form>
								<div class="author-thumb">
									<img src="<?= base_url(); ?>assets/img/author-page.jpg" alt="author">
								</div>
								<div class="form-group with-icon label-floating is-empty">
									<label class="control-label">Share what you are thinking here...</label>
									<textarea class="form-control" placeholder=""></textarea>
								</div>
								<div class="add-options-message">
									<a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD PHOTOS">
										<svg class="olymp-camera-icon" data-toggle="modal" data-target="#update-header-photo"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-camera-icon"></use></svg>
									</a>
									<a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="TAG YOUR FRIENDS">
										<svg class="olymp-computer-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-computer-icon"></use></svg>
									</a>
				
									<a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD LOCATION">
										<svg class="olymp-small-pin-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-small-pin-icon"></use></svg>
									</a>
				
								</div>
				
							</form>
						</div>
				
						<div class="tab-pane" id="profile-1" role="tabpanel" aria-expanded="true">
							<form>
								<div class="author-thumb">
									<img src="<?= base_url(); ?>assets/img/author-page.jpg" alt="author">
								</div>
								<div class="form-group with-icon label-floating is-empty">
									<label class="control-label">Share what you are thinking here...</label>
									<textarea class="form-control" placeholder=""  ></textarea>
								</div>
								<div class="add-options-message">
									<a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD PHOTOS">
										<svg class="olymp-camera-icon" data-toggle="modal" data-target="#update-header-photo"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-camera-icon"></use></svg>
									</a>
									<a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="TAG YOUR FRIENDS">
										<svg class="olymp-computer-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-computer-icon"></use></svg>
									</a>
				
									<a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD LOCATION">
										<svg class="olymp-small-pin-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-small-pin-icon"></use></svg>
                                    </a>
				
								</div>
				
							</form>
						</div>
				
						<div class="tab-pane" id="blog" role="tabpanel" aria-expanded="true">
							<form>
								<div class="author-thumb">
									<img src="<?= base_url(); ?>assets/img/author-page.jpg" alt="author">
								</div>
								<div class="form-group with-icon label-floating is-empty">
									<label class="control-label">Share what you are thinking here...</label>
									<textarea class="form-control" placeholder=""  ></textarea>
								</div>
								<div class="add-options-message">
									<a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD PHOTOS">
										<svg class="olymp-camera-icon" data-toggle="modal" data-target="#update-header-photo"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-camera-icon"></use></svg>
									</a>
									<a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="TAG YOUR FRIENDS">
										<svg class="olymp-computer-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-computer-icon"></use></svg>
									</a>
				
									<a href="#" class="options-message" data-toggle="tooltip" data-placement="top"   data-original-title="ADD LOCATION">
										<svg class="olymp-small-pin-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-small-pin-icon"></use></svg>
									</a>
				
								</div>
				
							</form>
						</div>
					</div>
				</div>
				
				<!-- ... end News Feed Form  -->			</div>

			<div id="newsfeed-items-grid">

				<div class="ui-block">
					
					

				
                </div>



			</div>

			

		</main>

		<!-- ... end Main Content -->


		<!-- Left Sidebar -->

		<aside class="col-xl-3 order-xl-1 col-lg-6 order-lg-2 col-md-6 col-sm-12 col-xs-12">
			<div class="ui-block">
				
				<!-- W-Weather -->
				
				<div class="friend-item fav-page widget w-wethear">
					
				
					<div class="friend-item-content">
				
						<div class="more">
							<svg class="olymp-three-dots-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
							<ul class="more-dropdown">
								<li>
									<a href="#">Report Profile</a>
								</li>
								<li>
									<a href="#">Block Profile</a>
								</li>
								<li>
									<a href="#">Turn Off Notifications</a>
								</li>
							</ul>
						</div>
						<div class="friend-avatar">
							<div class="author-thumb">
								<img src="<?= base_url(); ?>assets/img/avatar9.jpg" alt="author">
							</div>
							<div class="author-content">
								<a href="#" class="h5 author-name">Green Goo Rock</a>
								<div class="country">Rock Band</div>
							</div>
						</div>
				
						<div class="swiper-container">
							
								<div class="swiper-slide">
									<p class="friend-about" data-swiper-parallax="-500">
										We are Rock Band from Los Angeles, now based in San Francisco, come and listen to us play!
									</p>
				
								</div>
				
								
							
				
							<!-- If we need pagination -->
						</div>
					</div>
				</div>
				
				<!-- W-Weather -->			</div>

			

			
		</aside>

		<!-- ... end Left Sidebar -->


		<!-- Right Sidebar -->

		<aside class="col-xl-3 order-xl-3 col-lg-6 order-lg-3 col-md-6 col-sm-12 col-xs-12">

			<div class="ui-block">
				
				<!-- W-Birthsday-Alert -->
				
				<div class="widget w-birthday-alert">
					
				
					  <div class="video-thumb f-none">
							<img src="<?= base_url(); ?>assets/img/hijab-3064633_1920.jpg" alt="photo">
							<a href="img/Lag%20Jaa%20Gale%20-%20Sadhana,%20Lata%20Mangeshkar,%20Woh%20Kaun%20Thi%20Romantic%20Song.mp4" class="play-video">
								<svg class="olymp-play-icon"><use xlink:href="#olymp-play-icon"></use></svg>
							</a>
						</div>
                    <div class="widget w-socials socail_h">
						<h6 class="title">Other Social Networks:</h6>
						<a href="#" class="social-item bg-facebook">
							<i class="fab fa-facebook" aria-hidden="true"></i>
							Facebook
						</a>
						<a href="#" class="social-item bg-twitter">
							<i class="fab fa-twitter" aria-hidden="true"></i>
							Twitter
						</a>
						<a href="#" class="social-item bg-dribbble">
							<i class="fab fa-dribbble" aria-hidden="true"></i>
							Dribbble
						</a>
					</div>
					
				</div>
				
				<!-- ... end W-Birthsday-Alert -->			</div>


		</aside>

		<!-- ... end Right Sidebar -->

	</div>
</div>


<!-- Window-popup Update Header Photo -->

<div class="modal fade" id="update-header-photo">
	<div class="modal-dialog ui-block window-popup update-header-photo">
		<a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
			<svg class="olymp-close-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
		</a>

		<div class="ui-block-title">
			<h6 class="title">Update Header Photo</h6>
		</div>

		<a href="#" class="upload-photo-item">
			<svg class="olymp-computer-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-computer-icon"></use></svg>

			<h6>Upload Photo</h6>
			<span>Browse your computer.</span>
		</a>

		<a href="#" class="upload-photo-item" data-toggle="modal" data-target="#choose-from-my-photo">

			<svg class="olymp-photos-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-photos-icon"></use></svg>

			<h6>Choose from My Photos</h6>
			<span>Choose from your uploaded photos</span>
		</a>
	</div>
</div>


<!-- ... end Window-popup Update Header Photo -->

<!-- Window-popup Choose from my Photo -->

<div class="modal fade" id="choose-from-my-photo">
	<div class="modal-dialog ui-block window-popup choose-from-my-photo">
		<a href="#" class="close icon-close" data-dismiss="modal" aria-label="Close">
			<svg class="olymp-close-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
		</a>

		<div class="ui-block-title">
			<h6 class="title">Choose from My Photos</h6>

			<!-- Nav tabs -->
			<ul class="nav nav-tabs" role="tablist">
				<li class="nav-item">
					<a class="nav-link active" data-toggle="tab" href="#home" role="tab" aria-expanded="true">
						<svg class="olymp-photos-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-photos-icon"></use></svg>
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" data-toggle="tab" href="#profile" role="tab" aria-expanded="false">
						<svg class="olymp-albums-icon"><use xlink:href="<?= base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-albums-icon"></use></svg>
					</a>
				</li>
			</ul>
		</div>


		<div class="ui-block-content">
			<!-- Tab panes -->
			<div class="tab-content">
				<div class="tab-pane active" id="home" role="tabpanel" aria-expanded="true">

					<div class="choose-photo-item" data-mh="choose-item">
						<div class="radio">
							<label class="custom-radio">
								<img src="<?= base_url(); ?>assets/img/choose-photo1.jpg" alt="photo">
								<input type="radio" name="optionsRadios">
							</label>
						</div>
					</div>
					<div class="choose-photo-item" data-mh="choose-item">
						<div class="radio">
							<label class="custom-radio">
								<img src="<?= base_url(); ?>assets/img/choose-photo2.jpg" alt="photo">
								<input type="radio" name="optionsRadios">
							</label>
						</div>
					</div>
					<div class="choose-photo-item" data-mh="choose-item">
						<div class="radio">
							<label class="custom-radio">
								<img src="<?= base_url(); ?>assets/img/choose-photo3.jpg" alt="photo">
								<input type="radio" name="optionsRadios">
							</label>
						</div>
					</div>

					<div class="choose-photo-item" data-mh="choose-item">
						<div class="radio">
							<label class="custom-radio">
								<img src="<?= base_url(); ?>assets/img/choose-photo4.jpg" alt="photo">
								<input type="radio" name="optionsRadios">
							</label>
						</div>
					</div>
					<div class="choose-photo-item" data-mh="choose-item">
						<div class="radio">
							<label class="custom-radio">
								<img src="<?= base_url(); ?>assets/img/choose-photo5.jpg" alt="photo">
								<input type="radio" name="optionsRadios">
							</label>
						</div>
					</div>
					<div class="choose-photo-item" data-mh="choose-item">
						<div class="radio">
							<label class="custom-radio">
								<img src="<?= base_url(); ?>assets/img/choose-photo6.jpg" alt="photo">
								<input type="radio" name="optionsRadios">
							</label>
						</div>
					</div>

					<div class="choose-photo-item" data-mh="choose-item">
						<div class="radio">
							<label class="custom-radio">
								<img src="<?= base_url(); ?>assets/img/choose-photo7.jpg" alt="photo">
								<input type="radio" name="optionsRadios">
							</label>
						</div>
					</div>
					<div class="choose-photo-item" data-mh="choose-item">
						<div class="radio">
							<label class="custom-radio">
								<img src="<?= base_url(); ?>assets/img/choose-photo8.jpg" alt="photo">
								<input type="radio" name="optionsRadios">
							</label>
						</div>
					</div>
					<div class="choose-photo-item" data-mh="choose-item">
						<div class="radio">
							<label class="custom-radio">
								<img src="<?= base_url(); ?>assets/img/choose-photo9.jpg" alt="photo">
								<input type="radio" name="optionsRadios">
							</label>
						</div>
					</div>


					<a href="#" class="btn btn-secondary btn-lg btn--half-width">Cancel</a>
					<a href="#" class="btn btn-primary btn-lg btn--half-width">Confirm Photo</a>

				</div>
				<div class="tab-pane" id="profile" role="tabpanel" aria-expanded="false">

					<div class="choose-photo-item" data-mh="choose-item">
						<figure>
							<img src="<?= base_url(); ?>assets/img/choose-photo10.jpg" alt="photo">
							<figcaption>
								<a href="#">South America Vacations</a>
								<span>Last Added: 2 hours ago</span>
							</figcaption>
						</figure>
					</div>
					<div class="choose-photo-item" data-mh="choose-item">
						<figure>
							<img src="<?= base_url(); ?>assets/img/choose-photo11.jpg" alt="photo">
							<figcaption>
								<a href="#">Photoshoot Summer 2016</a>
								<span>Last Added: 5 weeks ago</span>
							</figcaption>
						</figure>
					</div>
					<div class="choose-photo-item" data-mh="choose-item">
						<figure>
							<img src="<?= base_url(); ?>assets/img/choose-photo12.jpg" alt="photo">
							<figcaption>
								<a href="#">Amazing Street Food</a>
								<span>Last Added: 6 mins ago</span>
							</figcaption>
						</figure>
					</div>

					<div class="choose-photo-item" data-mh="choose-item">
						<figure>
							<img src="<?= base_url(); ?>assets/img/choose-photo13.jpg" alt="photo">
							<figcaption>
								<a href="#">Graffity & Street Art</a>
								<span>Last Added: 16 hours ago</span>
							</figcaption>
						</figure>
					</div>
					<div class="choose-photo-item" data-mh="choose-item">
						<figure>
							<img src="<?= base_url(); ?>assets/img/choose-photo14.jpg" alt="photo">
							<figcaption>
								<a href="#">Amazing Landscapes</a>
								<span>Last Added: 13 mins ago</span>
							</figcaption>
						</figure>
					</div>
					<div class="choose-photo-item" data-mh="choose-item">
						<figure>
							<img src="<?= base_url(); ?>assets/img/choose-photo15.jpg" alt="photo">
							<figcaption>
								<a href="#">The Majestic Canyon</a>
								<span>Last Added: 57 mins ago</span>
							</figcaption>
						</figure>
					</div>


					<a href="#" class="btn btn-secondary btn-lg btn--half-width">Cancel</a>
					<a href="#" class="btn btn-primary btn-lg disabled btn--half-width">Confirm Photo</a>
				</div>
			</div>
		</div>

	</div>
</div>

<!-- ... end Window-popup Choose from my Photo -->

<!-- Main Content Badges -->

<div class="container">
    <div class="heding_reviews">
     
        <p>Reviews</p>
    
    </div>
	<div class="row">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">

			<div class="ui-block">

				
				<div class="birthday-item inline-items badges">
					<div class="author-thumb">
						<img src="<?= base_url(); ?>assets/img/badge1.png" alt="author">
						<div class="label-avatar bg-primary">2</div>
					</div>
					<div class="birthday-author-name">
						<a href="#" class="h6 author-name">Olympian User</a>
						<div class="birthday-date">Congratulations! You have been in the Olympus community for 2 years.</div>
					</div>
				
					<div class="skills-item">
						<div class="skills-item-meter">
							<span class="skills-item-meter-active" style="width: 76%"></span>
						</div>
					</div>
				
				</div>

			</div>

			<div class="ui-block">

				
				<div class="birthday-item inline-items badges">
					<div class="author-thumb">
						<img src="<?= base_url(); ?>assets/img/badge2.png" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="#" class="h6 author-name">Favourite Creator</a>
						<div class="birthday-date">You created a favourite page.</div>
					</div>
				
					<div class="skills-item">
						<div class="skills-item-meter">
							<span class="skills-item-meter-active" style="width: 100%"></span>
						</div>
					</div>
				
				</div>

			</div>

			<div class="ui-block">

				
				<div class="birthday-item inline-items badges">
					<div class="author-thumb">
						<img src="<?= base_url(); ?>assets/img/badge3.png" alt="author">
						<div class="label-avatar bg-blue">4</div>
					</div>
					<div class="birthday-author-name">
						<a href="#" class="h6 author-name">Friendly User</a>
						<div class="birthday-date">You have more than 80 friends. Next Tier: 150 Friends. </div>
					</div>
				
					<div class="skills-item">
						<div class="skills-item-meter">
							<span class="skills-item-meter-active" style="width: 52%"></span>
						</div>
					</div>
				
				</div>

			</div>

			<div class="ui-block">

				
				<div class="birthday-item inline-items badges">
					<div class="author-thumb">
						<img src="<?= base_url(); ?>assets/img/badge4.png" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="#" class="h6 author-name">Professional Photographer</a>
						<div class="birthday-date">You have uploaded more than 500 images to your profile.</div>
					</div>
				
					<div class="skills-item">
						<div class="skills-item-meter">
							<span class="skills-item-meter-active" style="width: 100%"></span>
						</div>
					</div>
				
				</div>

			</div>

			<div class="ui-block">

				
				<div class="birthday-item inline-items badges">
					<div class="author-thumb">
						<img src="<?= base_url(); ?>assets/img/badge5.png" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="#" class="h6 author-name">Expert Filmaker</a>
						<div class="birthday-date">You have uploaded more than 80 videos to your profile.</div>
					</div>
				
					<div class="skills-item">
						<div class="skills-item-meter">
							<span class="skills-item-meter-active" style="width: 70%"></span>
						</div>
					</div>
				
				</div>

			</div>

			<div class="ui-block">

				
				<div class="birthday-item inline-items badges">
					<div class="author-thumb">
						<img src="<?= base_url(); ?>assets/img/badge6.png" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="#" class="h6 author-name">Mightier Than The Sword</a>
						<div class="birthday-date">You have written more than 50 blog post entries.</div>
					</div>
				
					<div class="skills-item">
						<div class="skills-item-meter">
							<span class="skills-item-meter-active" style="width: 23%"></span>
						</div>
					</div>
				
				</div>

			</div>

			<div class="ui-block">

				
				<div class="birthday-item inline-items badges">
					<div class="author-thumb">
						<img src="<?= base_url(); ?>assets/img/badge7.png" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="#" class="h6 author-name">Universe Explorer</a>
						<div class="birthday-date">You have visited more than 1000 different user profiles.</div>
					</div>
				
					<div class="skills-item">
						<div class="skills-item-meter">
							<span class="skills-item-meter-active" style="width: 100%"></span>
						</div>
					</div>
				
				</div>

			</div>

			<div class="ui-block">

				
				<div class="birthday-item inline-items badges">
					<div class="author-thumb">
						<img src="<?= base_url(); ?>assets/img/badge8.png" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="#" class="h6 author-name">Nothing to Hide</a>
						<div class="birthday-date">You have completed all your profile fields.</div>
					</div>
				
					<div class="skills-item">
						<div class="skills-item-meter">
							<span class="skills-item-meter-active" style="width: 100%"></span>
						</div>
					</div>
				
				</div>

			</div>

			<div class="ui-block">

				
				<div class="birthday-item inline-items badges">
					<div class="author-thumb">
						<img src="<?= base_url(); ?>assets/img/badge9.png" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="#" class="h6 author-name">Messaging Master</a>
						<div class="birthday-date">You have messaged with at least 20 different people.</div>
					</div>
				
					<div class="skills-item">
						<div class="skills-item-meter">
							<span class="skills-item-meter-active" style="width: 69%"></span>
						</div>
					</div>
				
				</div>

			</div>

			<div class="ui-block">

				
				<div class="birthday-item inline-items badges">
					<div class="author-thumb">
						<img src="<?= base_url(); ?>assets/img/badge10.png" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="#" class="h6 author-name">Musical Sharer</a>
						<div class="birthday-date">You have linked your Spotify account to share your playlist.</div>
					</div>
				
					<div class="skills-item">
						<div class="skills-item-meter">
							<span class="skills-item-meter-active" style="width: 33%"></span>
						</div>
					</div>
				
				</div>

			</div>

			<div class="ui-block">

				
				<div class="birthday-item inline-items badges">
					<div class="author-thumb">
						<img src="<?= base_url(); ?>assets/img/badge11.png" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="#" class="h6 author-name">Superliked Hero</a>
						<div class="birthday-date">You have collected more than 100 likes in one post.</div>
					</div>
				
					<div class="skills-item">
						<div class="skills-item-meter">
							<span class="skills-item-meter-active" style="width: 100%"></span>
						</div>
					</div>
				
				</div>

			</div>

			<div class="ui-block">

				
				<div class="birthday-item inline-items badges">
					<div class="author-thumb">
						<img src="<?= base_url(); ?>assets/img/badge12.png" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="#" class="h6 author-name">Strongly Caffeinated </a>
						<div class="birthday-date">You have written more than 1000 posts.</div>
					</div>
				
					<div class="skills-item">
						<div class="skills-item-meter">
							<span class="skills-item-meter-active" style="width: 65%"></span>
						</div>
					</div>
				
				</div>

			</div>

			<div class="ui-block">

				
				<div class="birthday-item inline-items badges">
					<div class="author-thumb">
						<img src="<?= base_url(); ?>assets/img/badge13.png" alt="author">
						<div class="label-avatar bg-breez">2</div>
					</div>
					<div class="birthday-author-name">
						<a href="#" class="h6 author-name">Events Promoter</a>
						<div class="birthday-date">You have created more than 25 public or private events. Next Tier: 50.</div>
					</div>
				
					<div class="skills-item">
						<div class="skills-item-meter">
							<span class="skills-item-meter-active" style="width: 100%"></span>
						</div>
					</div>
				
				</div>

			</div>

			<div class="ui-block">

				
				<div class="birthday-item inline-items badges">
					<div class="author-thumb">
						<img src="<?= base_url(); ?>assets/img/badge14.png" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="#" class="h6 author-name">Friendship Cultivator</a>
						<div class="birthday-date">You have tagged friends on 200 different posts.</div>
					</div>
				
					<div class="skills-item">
						<div class="skills-item-meter">
							<span class="skills-item-meter-active" style="width: 80%"></span>
						</div>
					</div>
				
				</div>

			</div>

			<div class="ui-block">

				
				<div class="birthday-item inline-items badges">
					<div class="author-thumb">
						<img src="<?= base_url(); ?>assets/img/badge15.png" alt="author">
					</div>
					<div class="birthday-author-name">
						<a href="#" class="h6 author-name">Phantom Profile</a>
						<div class="birthday-date">You haven’t posted anything on your profile for more than 1 month.</div>
					</div>
				
					<div class="skills-item">
						<div class="skills-item-meter">
							<span class="skills-item-meter-active" style="width: 100%"></span>
						</div>
					</div>
				
				</div>

			</div>

		</div>
	</div>
</div>

<a class="back-to-top" href="#">
	<img src="<?= base_url(); ?>assets/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>

<?php include 'static/footer.php'; ?>
<?php include 'static/head.php'; ?>

<body class="landing-page">

<div class="content-bg-wrap">
	<div class="content-bg"></div>
</div>

<!-- Header Standard Landing  -->

<div class="header--standard header--standard-landing" id="header--standard">
	<div class="container">
		<div class="header--standard-wrap">

			<a href="#" class="logo">
				<div class="img-wrap">
					<img src="<?= base_url('assets/img/logo.png'); ?>" alt="Olympus">
				</div>
				<div class="title-block">
					<h6 class="logo-title">olympus</h6>
					<div class="sub-title">SOCIAL NETWORK</div>
				</div>
			</a>

		</div>
	</div>
</div>

<!-- ... end Header Standard Landing  -->
<div class="header-spacer--standard"></div>

<div class="container">
	<div class="row display-flex">
		<div class="col-xl-3 col-lg-4"></div>
		<div class="col-xl-6 col-lg-6 col-md-12 col-sm-12 col-xs-12">
			
			<!-- Login-Registration Form  -->
			
			<div class="registration-login-form">			
				<!-- Tab panes -->
					<div class="tab-pane" id="home" role="tabpanel" data-mh="log-tab">
						<div class="title h6">Register</div>
						<?php $attributes = array ('class'=>'container', 'enctype'=>'multipart/form-data', 'method'=>'POST');
        						echo form_open('signup/user_signup', $attributes); ?>
							<div class="row mt-4">
								<div class="col-lg-6 col-md-6">									
									<div class="form-group label-floating is-empty">
										<label from="uFname" class="control-label">First Name</label>
										<?= form_input(['type'=>'hidden', 'value' => '0', 'name'=>'isExpert']) ?>
										<?= form_input(['class'=>'form-control','name'=>'uFname','value'=>set_value('uFname')]); ?>
										<?= form_error('uFname'); ?>
									</div>
								</div>
								<div class="col-lg-6 col-md-6">									
									<div class="form-group label-floating is-empty">
										<label form="uLname" class="control-label">Last Name</label>
										<?php echo form_input(['class'=>'form-control','name'=>'uLname','value'=>set_value('uLname')]); ?>
										<?php echo form_error('uLname'); ?>
									</div>
								</div>
								<div class="col-lg-12 col-md-6">
									<div class="form-group label-floating is-empty">
										<label class="control-label">Username</label>
										<?php echo form_input(['class'=>'form-control','name'=>'uNname','id'=>'username','value'=>set_value('uNname')]); ?>
										<?php echo form_error('uNname'); ?><div id="userName"></div>
									</div>
								</div>
								<div class="col-xl-12 col-lg-12 col-md-12">
									<div class="form-group label-floating is-empty">
										<label class="control-label">Your Email</label>
										<?php echo form_input(['type'=>'email','class'=>'form-control','name'=>'uEmail', 'id'=>'email', 'value'=>set_value('uEmail')]); ?>
										<?php echo form_error('uEmail'); ?><div id="resultEmail"></div>
									</div>
									<div class="form-group label-floating is-empty">
										<label class="control-label">Phone</label>
										<?php echo form_input(['class'=>'form-control','name'=>'uPhone','value'=>set_value('uPhone')]); ?>
										<?php echo form_error('uPhone'); ?>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group label-floating is-empty">
										<label class="control-label">Password</label>
										<?php echo form_password(['class'=>'form-control','name'=>'uPassword','value'=>set_value('uPassword')]); ?>
										<?php echo form_error('uPassword'); ?>
									</div>
								</div>
								<div class="col-lg-6">	
									<div class="form-group label-floating is-empty">
										<label class="control-label">Confirm Password</label>
										<?php echo form_password(['class'=>'form-control','name'=>'uCpassword','value'=>set_value('uCpassword')]); ?>
										<?php echo form_error('uCpassword'); ?>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group label-floating is-empty">
										<label for="country" class="control-label">Select Country</label>
										<?php
											$countryOptions = array(
												''=>'',
											'PK' => 'Paksitan',
											'CH' => 'China'
										);
										echo form_dropdown('country', $countryOptions);
										echo form_error('country');
										?>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group label-floating is-empty">
										<label for="state" class="control-label">State / Province</label>
										<?php
											echo form_input(['name'=>'state','class'=>'form-control', 'value'=>set_value('province')]);
											echo form_error('state');
										?>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group label-floating is-empty">
										<label for="city" class="control-label">City</label>
										<?php
											echo form_input(['name'=>'city','class'=>'form-control', 'value'=>set_value('city')]);
											echo form_error('city');
										?>
									</div>
								</div>
								<div class="col-lg-6">
									<div class="form-group label-floating is-empty">
										<label for="street" class="control-label">Street</label>
										<?php
											echo form_input(['name'=>'street','class'=>'form-control', 'value'=>set_value('street')]);
											echo form_error('street');
										?>
									</div>	
								</div>		
								<div class="col-lg-12">
									<div class="form-group date-time-picker label-floating">
										<label class="control-label">Date of Birthday</label>
										<?php echo form_input(['class'=>'form-control','name'=>'datetimepicker','value'=>set_value('datetimepicker')]); ?>
										<?php echo form_error('datetimepicker'); ?>
										<span class="input-group-addon">
											<svg class="olymp-calendar-icon">
												<use xlink:href="<?php echo base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-calendar-icon"></use>
											</svg>
										</span>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="form-group label-floating is-empty">
										<label class="control-label">Your Gender</label>
										<?php 
											$genderOptions = [''=>'','M'=>'Male','F'=>'Female'];
											echo form_dropdown('uGender',$genderOptions);
											echo form_error('uGender');
										?>
									</div>
								</div>
								<div class="col-lg-12">
									<div class="remember">
										<div class="checkbox">
											<label>
												<?php echo form_input(['name'=>'optionsCheckboxes','type'=>'checkbox']); ?>
												I accept the <a href="#">Terms and Conditions</a> of the website
											</label>
										</div>
									</div>
									</div>
									<div class="col-lg-12">
										<input type="submit" value="Complete Registration" class="btn btn-success" name="register">
									</div>
								</div>
							</div>
						</form>
						
					</div>
			</div>			
			<!-- ... end Login-Registration Form  -->	
		</div>
	</div>
</div>

<?php include 'static/js.php' ?>

<script>

$( document ).ready(function() 
{
	$( '#username' ).on('change',function()
	{
		var username = $( '#username' ).val();
		if(username != '')
		{					
			$.ajax({				
				url: '<?= base_url("signup/chk_form_data"); ?>',
				method:'POST',
				data:{username:username},
				success:function(data){
					$( '#userName' ).html(data);
					console.log(data);
				}
			});
		}
	});
	$( '#email' ).change(function()
	{
		var email = $( '#email').val();
		if(email != '')
		{			
			$.ajax({
				url: '<?= base_url(); ?>signup/chk_form_data',
				method:'POST',
				data:{email:email},
				success:function(data){
					$( '#resultEmail' ).html(data);
				}
			});
		}
	});
});
</script>
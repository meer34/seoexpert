<?php include 'static/head.php';?>
<body>


<div class="container">

	<div class="row">


			<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<h2 class="presentation-margin wizard-container pmargin text-center">Expert Registration Form</h2>
			<div class="ui-block card  wizard-card">
			<?=form_open_multipart('expertsignup/expert_signup')?>
				<div class="ui-block-title">
					<h6 class="title">Personal Information</h6>
				</div>
				<div class="ui-block-content">

						<div class="row">


							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<div class="form-group label-floating is-empty">
										<label class="control-label">First Name</label>
										<?= form_input(['value'=>'1', 'type'=>'hidden', 'name'=>'isExpert']); ?>
										<?= form_input(['class' => 'form-control', 'name' => 'fname', 'value' => set_value('fname')]);?>
										<?= form_error('fname');?>
									</div>

									<div class="form-group label-floating is-empty">
										<label class="control-label">Your Email</label>
										<?= form_input(['class' => 'form-control', 'name' => 'email','id'=>'email', 'type' => 'email', 'value' => set_value('email')]);?>
										<?= form_error('email');?><div id="resultEmail"></div>
									</div>
									<div class="form-group label-floating is-empty">
										<label class="control-label">Rate Per Minute</label>
										<?= form_input(['class' => 'form-control', 'name' => 'rpm', 'value' => set_value('rpm')]);?>
										<?= form_error('rpm');?>
									</div>

									<div class="form-group label-floating is-empty">
										<label class="control-label">Password</label>
										<?=form_password(['class' => 'form-control', 'name' => 'uPassword', 'value' => set_value('uPassword')]);?>
										<?=form_error('uPassword');?>
									</div>

									<div class="form-group label-floating is-select">
										<label class="control-label">Your Gender</label>
										<?php
										$gender = ['MA' => 'Male', 'FA' => 'Female'];
										echo form_dropdown(['name' => 'gender'], $gender);
										echo form_error('gender');
										?>
									</div>


									<div class="form-group date-time-picker label-floating">
										<label class="control-label">Your Birthday</label>
										<?=form_input(['class' => 'form-control', 'name' => 'datetimepicker','value'=>set_value('datetimepicker')]);?>
										<?=form_error('datetimepicker');?>
										<span class="input-group-addon">
											<svg class="olymp-month-calendar-icon icon"><use xlink:href="<?=base_url();?>assets/svg-icons/sprites/icons.svg#olymp-month-calendar-icon"></use></svg>
										</span>
									</div>

<!--
									<div class="form-group label-floating is-select">
										<label class="control-label">Your State / Province</label>
										<select class="selectpicker form-control">
											<option value="CA">California</option>
											<option value="TE">Texas</option>
										</select>
									</div>
-->

							<!--
								<fieldset class="form-group label-floating is-select">
									<label class="control-label">Country and Timezone</label>
									<select class="selectpicker form-control">
										<option value="AL">United States (UTC-8)</option>
										<option value="2">Ontario (UTC−6)</option>
										<option value="WY">Alberta (UTC−6)</option>
									</select>
								</fieldset>

								<fieldset class="form-group label-floating is-select">
									<label class="control-label">Extended Forecast Days</label>
									<select class="selectpicker form-control">
										<option value="AL">Show Next 7 days</option>
										<option value="2">Show Next 10 days</option>
										<option value="WY">Show Next 14 days</option>
									</select>
								</fieldset>

							-->
							</div>

							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">



							<!--

								<fieldset class="form-group label-floating is-select">
									<label class="control-label">Temperature Unit</label>
									<select class="selectpicker form-control">
										<option value="AL">F° (Farenheit)</option>
										<option value="2">C° (Celsius)</option>
									</select>
								</fieldset>

								<div class="switcher-block">
									<div class="h6 title">Show Extended Forecast on Widget</div>
									<div class="togglebutton blue">
										<label>
											<input type="checkbox" checked="">
										</label>
									</div>
								</div>

							-->







									<div class="form-group label-floating is-empty">
										<label class="control-label">Last Name</label>
										<?=form_input(['class' => 'form-control', 'name' => 'lname', 'value' => set_value('lname')]);?>
										<?=form_error('lname');?>
									</div>
									<div class="form-group label-floating">
										<label class="control-label">Alternative Email</label>
										<?=form_input(['class' => 'form-control', 'name' => 'alt-email', 'type' => 'email', 'value' => set_value('alt-email')]);?>
										<?=form_error('alt-email');?>
									</div>
									<div class="form-group label-floating is-empty">
										<label class="control-label">Phone Number</label>
										<?=form_input(['class' => 'form-control', 'id' => 'phone', 'value' => set_value('phone'), 'type' => 'tel', 'name' => 'phone', 'data-inputmask' => '&quot;mask&quot;: &quot;9999-9999999&quot;']);?>
										<?=form_error('phone');?>
									</div>

									<div class="form-group label-floating is-empty">
										<label class="control-label">Confirm Password</label>
										<?=form_password(['class' => 'form-control', 'name' => 'uCpassword', 'value' => set_value('uCpassword')]);?>
										<?=form_error('uCpassword');?>
									</div>
									<div class="form-group label-floating is-empty">
										<label class="control-label">Username</label>
										<?=form_input(['class' => 'form-control','id'=>'username', 'name' => 'username', 'value' => set_value('username')]);?>
										<?=form_error('username');?><div id="userName"></div>
									</div>






<!--
									<div class="form-group label-floating is-select">
										<label class="control-label">Your City</label>
										<select class="selectpicker form-control">
											<option value="SF">San Francisco</option>
											<option value="NY">New York</option>
										</select>
									</div>
-->
							</div>

							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">

								<div class="file-upload">
									<label for="upload" class="file-upload__label bg-blue">Profile picture</label>
									<input  id="upload" class="file-upload__input" type="file" name="profile" value='<?=set_value('profile');?>'>
									<?php if ( isset($profile_error)) echo $profile_error;?>
								</div>
							<!--
							<img id="blah" src="#" alt="your image" />
							<div class="form-group">
								<label class="control-label">A file upload button without icon</label>
								<input type="file" class="filestyle" data-icon="false">
							</div>
								-->
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">

							<!--
							<div class="form-group">
								<label class="control-label">A file upload button without icon</label>
								<input type="file" class="filestyle" data-icon="false">
							</div>
								-->

								<div class="file-upload">
									<label for="upload1" class="file-upload__label bg-breez">cover picture</label>
									<input id="upload1" class="file-upload__input" type="file" name="cover" value='<?=set_value('cover');?>'>
									<?php if ( isset($cover_error)) echo $cover_error;?>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<!--
							<div class="form-group">
								<label class="control-label">A file upload button without icon</label>
								<input type="file" class="filestyle" data-icon="false">
							</div>
								-->

							<div class="file-upload">
								<?=form_error('front');?>
								<label for="upload2" class="file-upload__label">ID front Picture</label>
								<input id="upload2" class="file-upload__input" type="file" name="front">
								<?php if ( isset($front_error)) echo $front_error;?>
								</div>
							</div>
							<div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
							<!--
							<div class="form-group">
								<label class="control-label">A file upload button without icon</label>
								<input type="file" class="filestyle" data-icon="false">
							</div>

								-->
							<div class="file-upload">
								<?=form_error('back');?>
								<label for="upload3" class="file-upload__label bg-green">ID Back picture</label>
								<input id="upload3" class="file-upload__input" type="file" name="back">
								<?php if ( isset($back_error)) echo $back_error;?>
								</div>
							</div>


								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<div class="form-group label-floating">
										<label class="control-label">Write a Short description about you</label>
										<?=form_textarea(['class' => 'form-control', 'name' => 'smallDisc', 'rows' => '5', 'maxlength' => '100','value'=>set_value('smallDisc')]);?>
										<?=form_error('smallDisc');?>
									</div>
								</div>

								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
									<div class="form-group label-floating">
										<label class="control-label">Write a detailed description about you</label>
										<?=form_textarea(['class' => 'form-control', 'name' => 'longDisc', 'rows' => '5', 'maxlength' => '500','value'=>set_value('smallDisc')]);?>
										<?=form_error('longDisc');?>
									</div>
								</div>

						</div>
				</div>
				<div class="ui-block-title">
					<h6 class="title">Address</h6>
				</div>
				<div class="ui-block-content">

						<div class="row">
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

								<div class="form-group label-floating">
									<label class="control-label">Country</label>
									<?=form_input(['name' => 'country', 'class' => 'form-control','value'=>set_value('country')]);?>
									<?=form_error('country');?>
								</div>

								<div class="form-group label-floating">
									<label class="control-label">City*</label>
									<?=form_input(['name' => 'city', 'class' => 'form-control','value'=>set_value('city')]);?>
									<?=form_error('city');?>
								</div>

								<div class="form-group label-floating">
									<label class="control-label">Zip Code*</label>
									<?=form_input(['name' => 'zip', 'class' => 'form-control','value'=>set_value('zip')]);?>
									<?=form_error('zip');?>
								</div>






							</div>
							<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

								<div class="form-group label-floating">
									<label class="control-label">State/Province</label>
									<?=form_input(['name' => 'state', 'class' => 'form-control','value'=>set_value('state')]);?>
									<?=form_error('state');?>
								</div>

								<div class="form-group label-floating">
									<label class="control-label">Street*</label>
									<?=form_input(['name' => 'street', 'class' => 'form-control','value'=>set_value('street')]);?>
									<?=form_error('street');?>
								</div>


							</div>
						</div>
				</div>
				<div class="ui-block-title">
					<h6 class="title">Expert Information</h6>
				</div>
				<div class="ui-block-content">


							<p>Select some categories<?=form_error('optionsCheckboxes');?></p>


							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

								<div class="checkbox">
									<label>
										<input type="checkbox" name="optionsCheckboxes[]"  value="<?= set_value('optionsCheckboxes[]')?>" id="cbgroup1_master" onchange="togglecheckboxes(this,'optionsCheckboxes')">
										<strong>Select All</strong>
									</label>
								</div>
							</div>

							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<div class="row">

									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
										<div class="checkbox">
											<label>
												<input type="checkbox" name="optionsCheckboxes" value="<?= set_value('optionsCheckboxes[]')?>"> Love and Relationship
											</label>
										</div>
										<div class="checkbox">
											<label>
												<input type="checkbox" name="optionsCheckboxes" value="<?= set_value('optionsCheckboxes[]')?>"> Tarot and Cards
											</label>
										</div>
										<div class="checkbox">
											<label>
												<input type="checkbox" name="optionsCheckboxes" value="<?= set_value('optionsCheckboxes[]')?>"> Dream Interpretation
											</label>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">

										<div class="checkbox">
											<label>
												<input type="checkbox" name="optionsCheckboxes" value="<?= set_value('optionsCheckboxes[]')?>"> Astorolgy
											</label>
										</div>
										<div class="checkbox">
											<label>
												<input type="checkbox" name="optionsCheckboxes" value="<?= set_value('optionsCheckboxes[]')?>"> Palm Reading
											</label>
										</div>
										<div class="checkbox">
											<label>
												<input type="checkbox" name="optionsCheckboxes" value="<?= set_value('optionsCheckboxes')?>"> Spiritual Guides
											</label>
										</div>
									</div>
									<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
										<div class="checkbox">
											<label>
												<input type="checkbox" name="optionsCheckboxes" value="<?= set_value('optionsCheckboxes[]')?>"> Rituals and Energies
											</label>
										</div>
										<div class="checkbox">
											<label>
												<input type="checkbox" name="optionsCheckboxes" value="<?= set_value('optionsCheckboxes[]')?>"> Home and family
											</label>
										</div>
										<div class="checkbox">
											<label>
												<input type="checkbox" name="optionsCheckboxes" value="<?= set_value('optionsCheckboxes[]')?>"> Numerology
											</label>
										</div>
									</div>
								</div>

							</div>


				</div>
				<!-- <div class="ui-block-title" id="payment" role="tabpanel">
					<h6 class="title">Payment Method</h6>
				</div>
				<div class="ui-block-content">
							<p>The following payment method are available to you on based on your countery and currency setting.</p>


							Personal Information Form 



							<div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
								<h6>Type of payment</h6>
								<div class="row">
									<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
										<div class="radio">
											<label>
												<?=form_radio(['type' => 'radio', 'name' => 'optionsRadios', 'id' => 'bank', 'checked' => 'checked']);?> Bank
											</label>
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
										<div class="radio">
											<label>
												<?=form_radio(['type' => 'radio', 'name' => 'optionsRadios', 'id' => 'w_union']);?> Westron Union
											</label>
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
										<div class="radio">
											<label>
											<?=form_radio(['type' => 'radio', 'name' => 'optionsRadios', 'id' => 'paypal']);?> paypal
											</label>
										</div>
									</div>
									<div class="col-lg-3 col-md-3 col-sm-12 col-xs-12">
										<div class="radio">
											<label>
											<?=form_radio(['name' => 'optionsRadios', 'id' => 'payoneer']);?> payoneer
											</label>
										</div>
									</div>
								</div>
							</div>
							<div class="bank">
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<div class="form-group label-floating">
											<label class="control-label">Bank Name</label>
											<?=form_input(['class' => 'form-control', 'name' => 'bName'])?>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<div class="form-group label-floating">
											<label class="control-label">IBAN(International Bank Account Number)*</label>
											<?=form_input(['class' => 'form-control', 'name' => 'iban'])?>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<div class="form-group label-floating">
											<label class="control-label">Swift Code*</label>
											<?=form_input(['class' => 'form-control', 'name' => 'sCode'])?>
										</div>
									</div>
								</div>
							</div>
							<div class="w_union">
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<div class="form-group label-floating">
											<label class="control-label">Enter Your Name*</label>
											<?=form_input(['class' => 'form-control', 'name' => 'cName'])?>
										</div>
									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<div class="form-group label-floating">
											<label class="control-label">Phone Number*</label>
											<?=form_input(['class' => 'form-control', 'name' => 'cPhone'])?>
										</div>


									</div>
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<div class="form-group label-floating">
											<label class="control-label">ID Card Number*</label>
											<?=form_input(['class' => 'form-control', 'name' => 'idcn'])?>
										</div>


									</div>
								</div>
							</div>
							<div class="paypal">
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
										<div class="form-group label-floating">
											<label class="control-label">paypal ID*</label>
											<?=form_input(['class' => 'form-control', 'name' => 'pID'])?>
										</div>
									</div>
								</div>
							</div>
							<div class="payoneer">
								<div class="row">
									<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">

										<div class="form-group label-floating">
											<label class="control-label">payoneer ID*</label>
											<?=form_input(['class' => 'form-control', 'name' => 'pID1'])?>
										</div>
									</div>
								</div>
							</div> -->

							<div class="row">
								<div class="col-lg-6 offset-4 col-md-6 col-sm-12 col-xs-12">
									<div class="remember">
										<div class="checkbox">
											<label>
												<input name="accept" type="checkbox">
												I accept the <a href="#">Terms and Conditions</a> of the website<?=form_error('accept');?>
											</label>
										</div>
									</div>
								</div>
								<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12 offset-3">
									<input type="submit" value="Complete Registration!" class="btn btn-purple btn-lg full-width" />
								</div>
							</div>

				</div>



				</div>

			</form>
			</div>
		</div>


</div>




<a class="back-to-top" href="#">
	<img src="<?=base_url();?>assets/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>

<?php include 'static/js.php';?>
	<script>
		$(document).ready(function () {

			$(".nav a").click(function () {
				$(".nav li a").removeClass("active");
				$(".nav a").removeClass("active");
				$(this).tab('show');
			});
		});
	</script>
	<script>
		$(document).ready(function () {
			$("#flip").click(function () {
				$("#panel").slideDown("slow");
			});
			$(".bank").show();
			$(".w_union").hide();
			$(".paypal").hide();
			$(".payoneer").hide();
			$("#bank").click(function () {
				$(".bank").show();
				$(".w_union").hide();
				$(".paypal").hide();
				$(".payoneer").hide();
			});
			$("#w_union").click(function () {
				$(".bank").hide();
				$(".w_union").show();
				$(".paypal").hide();
				$(".payoneer").hide();
			});
			$("#paypal").click(function () {
				$(".bank").hide();
				$(".w_union").hide();
				$(".paypal").show();
				$(".payoneer").hide();
			});
			$("#payoneer").click(function () {
				$(".bank").hide();
				$(".w_union").hide();
				$(".paypal").hide();
				$(".payoneer").show();
			});
		});
	</script>

	<script>
		function togglecheckboxes(master, group) {
			var cbarray = document.getElementsByName(group);
			for (var i = 0; i < cbarray.length; i++) {
				cbarray[i].checked = master.checked;
			}
		}
	</script>

   <script type="text/javascript">
		ar reader = new FileReader();
		reader.onload = function (e) {
			$('#blah').attr('src', e.target.result);
		}

		function readURL(input) {
			if (input.files && input.files[0]) {
				reader.readAsDataURL(input.files[0]);
			}
		}
    </script>
	<script>

$( document ).ready(function() 
{
	$( '#username' ).on('change',function()
	{
		var username = $( '#username' ).val();
		if(username != '')
		{					
			$.ajax({				
				url: '<?= base_url("signup/chk_form_data"); ?>',
				method:'POST',
				data:{username:username},
				success:function(data){
					$( '#userName' ).html(data);
				}
			});
		}
	});
	$( '#email' ).on('change', function()
	{
		var email = $( '#email').val();
		if(email != '')
		{			
			$.ajax({
				url: '<?= base_url(); ?>signup/chk_form_data',
				method:'POST',
				data:{email:email},
				success:function(data){
					$( '#resultEmail' ).html(data);
				}
			});
		}
	});
});
</script>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml"><head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>Fusion - Metro Email Newsletter Template</title>
<meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0;">
<link rel="stylesheet" type="text/css" href="<?php echo base_url(); ?>assets/emailtemplate/css/style.css">
<style type="text/css">
body {
	background-color: #ffffff;
	margin: 0px;
	padding: 0px;
	text-align: center;
	width: 100%;
}
html { width: 100%; }
.contentbg
{
	 background-color: #ffffff;
}
img {
	border:0px;
	outline:none;
	text-decoration:none;
	display:block;
}

@media only screen and (max-width:640px)

{
	body{width:auto!important;}
	table[class=main]{width:446px !important;}
	table[class=logo]{width:100% !important;}
	table[class=righttop-details]{width:100% !important;}
	table[class=social-top]{width:100% !important;}
	td[class=viewonline-field]{padding: 10px 0px 0px 0px !important;}
	td[id=viewonline]{text-align:center !important;}
	table[class=subimagebox]{width:100% !important;}
	td[class=subcontent-hidebox]{height:20px !important; display:block !important;}
	td[class=subcontent-hideboxtwo]{height:30px !important; display:block !important;}
	td[class=main-contentbox]{width:285px !important; float:left !important; padding-left:75px !important;}
	td[class=maincontent-hidebox]{height:30px !important; display:block !important;}
	td[class=contentbox]{width:386px !important; display:block; padding:0px 25px !important;}
	table[class=subcontentbox]{width:386px !important;}
	td[class=downloadhidebox]{display:none !important;}
	table[class=icon-bottom]{width:100% !important;}
	td[class=hidebox]{height:15px !important; display:block !important;}
	td[class=hidebox-footer]{height:10px !important; display:block !important;}
	td[class=footerbox]{width:100% !important; display:block !important;}
}

@media only screen and (max-width:450px)
{
	body{width:auto!important;}
	table[class=main]{width:320px !important;}
	table[class=logo]{width:100% !important;}
	table[class=righttop-details]{width:100% !important;}
	table[class=social-top]{width:100% !important;}
	td[class=viewonline-field]{padding: 10px 0px 0px 0px !important;}
	td[id=viewonline]{text-align:center !important;}
	table[class=subimagebox]{width:100% !important;}
	td[class=subcontent-hidebox]{height:20px !important; display:block !important;}
	td[class=subcontent-hideboxtwo]{height:30px !important; display:block !important;}
	td[class=main-contentbox]{width:285px !important; float:left !important; padding-left:12px !important;}
	td[class=maincontent-hidebox]{height:30px !important; display:block !important;}
	td[class=contentbox]{width:100% !important; display:block; padding:0px !important;}
	table[class=subcontentbox]{width:100% !important;}
	td[class=downloadhidebox]{display:none !important;}
	table[class=icon-bottom]{width:100% !important;}
	td[class=hidebox]{height:15px !important; display:block !important;}
	td[class=hidebox-footer]{height:10px !important; display:block !important;}
	td[class=footerbox]{width:100% !important; display:block !important;}
}
</style>

<!-- Internet Explorer fix -->
<!--[if IE]>
<style type="text/css">

@media only screen and (max-width:640px)
{
    table[class=subimagebox]{width:100% !important; float:left;}
    td[class=main-contentbox]{width:285px !important; float:left; padding-left:75px !important;}
    td[class=contentbox]{width:386px !important; float:left; display:block; padding:0px 25px !important;}
    table[class=subcontentbox]{width:386px !important; float:left;}
    td[class=footerbox]{width:100% !important; float:left; display:block;}
}

@media only screen and (max-width:450px)
{
    table[class=subimagebox]{width:100% !important; float:left;}
    td[class=main-contentbox]{width:285px !important; float:left; padding-left:12px !important;}
    td[class=contentbox]{width:100% !important; float:left; display:block; padding:0px !important;}
    table[class=subcontentbox]{width:100% !important; float:left;}
    td[class=footerbox]{width:100% !important; float:left; display:block;}
}

</style>
<![endif]-->
<!-- / Internet Explorer fix -->

<!-- Outlook -->
<!--[if gte mso 9]>
<style type="text/css">
.righttop-details{width:250px !important;}
.icon-bottom{width:286px !important;}
.subimagebox{width:240px !important;}
</style>
<![endif]-->

<!--[if gte mso 10]>
<style type="text/css">
.righttop-details{width:250px !important;}
.icon-bottom{width:286px !important;}
.subimagebox{width:240px !important;}
</style>
<![endif]-->

<!--[if gte mso 15]>
<style type="text/css">
.righttop-details{width:250px !important;}
.icon-bottom{width:286px !important;}
.subimagebox{width:240px !important;}
</style>
<![endif]-->
<!-- / Outlook -->

</head>

<body>

<!--Table Start-->
<table class="contentbg" width="100%" cellspacing="0" cellpadding="0" border="0">
  <tbody><tr>
    <td><table width="100%" cellspacing="0" cellpadding="0" border="0">
      <tbody><tr>
        <td valign="top"><table width="100%" cellspacing="0" cellpadding="0" border="0">
          <tbody><tr>
            <td valign="top" align="center">
            
            <!--Header Start-->
            <table class="main" width="610" cellspacing="0" cellpadding="0" border="0">
      <tbody><tr>
        <td width="5">&nbsp;</td>
        <td style="padding-bottom:5px;" valign="top"><table width="100%" cellspacing="0" cellpadding="0" border="0">
          <tbody><tr>
            <td valign="top">
            
            <!--Logo Start-->
            <table class="logo" width="60px" cellspacing="0" cellpadding="0" border="0" align="left">
              <tbody><tr>
                <td valign="top" align="center"><table width="186" cellspacing="0" cellpadding="0" border="0">
                  <tbody><tr>
                    <td  valign="top" bgcolor="#fff" align="center"><a href="#" target="_blank"><img editable="true" mc:edit="logo" src="https://image.ibb.co/dQF98J/logo.jpg" alt="logo" style="display:block; border: none;height: 120px;"></a></td>
                  </tr>
                </tbody></table></td>
              </tr>
            </tbody></table>
            <!--Logo End-->
            
            <table class="righttop-details" width="390" cellspacing="0" cellpadding="0" border="0" align="right">
              <tbody><tr>
                <td style="padding-top:15px;" align="right">
                
                <!--Social Top Start-->
                <table class="social-top" width="245" cellspacing="0" cellpadding="0" border="0">
                  <tbody><tr>
                    <td align="center"><table width="245" cellspacing="0" cellpadding="0" border="0">
                      <tbody><tr>
                        <td><a href="https://www.facebook.com/WeCounselors" target="_blank"><img editable="true" mc:edit="social-1" src="https://image.ibb.co/i16hTJ/facebook.png" alt="social icon" style="display:block; border: none;" width="45" height="45"></a></td>
                        <td width="5">&nbsp;</td>
                        <td><a href="https://twitter.com/CounselorsWe" target="_blank"><img editable="true" mc:edit="social-2" src="https://image.ibb.co/bSgMNd/twitter.png" alt="social icon" style="display:block; border: none;" width="45" height="45"></a></td>
                        <td width="5">&nbsp;</td>
                        <td><a href="https://www.instagram.com/wecounselors/" target="_blank"><img editable="true" mc:edit="social-3" src="https://image.ibb.co/irUK8J/instagram.png" alt="social icon" style="display:block; border: none;" width="45" height="45"></a></td>
                        <td width="5">&nbsp;</td>
                        <td><a href="https://www.youtube.com/channel/UCQ90JZL4_8BzplAgeyihY4Q" target="_blank"><img editable="true" mc:edit="social-4" src="https://image.ibb.co/hM6kFy/you_tube.png" alt="social icon" style="display:block; border: none;" width="45" height="45"></a></td>
                        <td width="5">&nbsp;</td>
                        <td><a href="https://plus.google.com/109964264678439138018" target="_blank"><img editable="true" mc:edit="social-5" src="https://image.ibb.co/ikLvFy/googleplus.png" alt="social icon" style="display:block; border: none;" width="45" height="45"></a></td>
                      </tr>
                    </tbody></table></td>
                  </tr>
                </tbody></table>
                <!--Social Top End-->
                
                </td>
              </tr>
              <tr>
                <td style="padding-top:8px; padding-left:20px;" class="viewonline-field" valign="top">
                
                <!--View Online Start-->
                <table width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody><tr>
                    <td mc:edit="view-online" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: normal; color: #7e7e7e; line-height: 18px;" id="viewonline" valign="top" align="right"><singleline></singleline>
                      <singleline>If you can’t see this email? <a style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: normal; color: #7e7e7e; text-decoration: none;" href="#" target="_blank">View it online</a></singleline></td>
                  </tr>
                </tbody></table>
                <!--View Online End-->
                
                </td>
              </tr>
            </tbody></table>
            </td>
          </tr>
        </tbody></table></td>
        <td width="5">&nbsp;</td>
      </tr>
    </tbody></table>
            <!--Header End-->
            
            </td>
          </tr>
          <tr>
            <td valign="top" bgcolor="#006ac1">
            
            <!--Heading Text Start-->
        <table class="main" width="610" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody><tr>
            <td width="5">&nbsp;</td>
            <td mc:edit="heading-text" style="font-family: 'Open Sans', sans-serif; font-size: 30px; font-weight: normal; color: #ffffff; line-height: 36px; text-align:center; padding:18px 25px 25px 25px;" valign="top"><singleline>Welcome to We Counselers </singleline></td>
            <td width="5">&nbsp;</td>
          </tr>
        </tbody></table>
        <!--Heading Text End-->
            
            </td>
          </tr>
        </tbody></table></td>
      </tr>
<!--Content Start y1-->
      <tr style="height: 150px;">

        <td valign="top" bgcolor="#fff">
            
          <!--Heading Text Start-->
      <table class="main" width="610" cellspacing="0" cellpadding="0" border="0" align="center">
        <tbody><tr>
          <td width="5">&nbsp;</td>
          <td mc:edit="heading-text" style="font-family: 'Open Sans', sans-serif; font-size: 24px; font-weight: normal; color: #000000; line-height: 36px; text-align:left; padding:18px 25px 25px 25px;" valign="top"><singleline><?php echo $heading; ?></singleline>
          <p style="font-family: 'Open Sans', sans-serif;font-size: 15px;font-weight: normal;color: #000000;line-height: 20px;text-align: justify;"><?php echo $expertmessage; ?> </p>
          </td>
          <td width="5">&nbsp;</td>
        </tr>
      </tbody></table>
      <!--Heading Text End-->
          
          </td>


      </tr>
      <tr>
        <td valign="top">
        <table width="100%" cellspacing="0" cellpadding="0" border="0">
          <tbody><tr>
            <td valign="top" bgcolor="#006ac1">
            
            <!--Icon Bottom Start-->
        <table class="main" width="610" cellspacing="0" cellpadding="0" border="0" align="center">
          <tbody><tr>
            <td width="5">&nbsp;</td>
            <td style="padding:20px 0px 20px 0px;" valign="top"><table class="icon-bottom" width="300" cellspacing="0" cellpadding="0" border="0" align="left">
              <tbody><tr>
                <td valign="top"><table width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody><tr>
                    <td width="28" valign="top" align="right"><img editable="true" mc:edit="bottom-icon-1" src="https://image.ibb.co/bsEpay/footer_icon_1.png" alt="icon" style="display:block; border: none;" width="28" height="31"></td>
                    <td mc:edit="see-all" style="padding:0px 15px;" valign="top" align="left"><singleline><a style="font-family: 'Open Sans', sans-serif; font-size: 24px; font-weight: normal; color: #ffffff; text-decoration: none;" href="#" target="_blank">Get Cradit</a></singleline></td>
                  </tr>
                </tbody></table></td>
              </tr>
            </tbody></table>
              <table class="icon-bottom" width="300" cellspacing="0" cellpadding="0" border="0" align="right">
                <tbody><tr>
                  <td class="hidebox" style="display:none;"></td>
                </tr>
                <tr>
                  <td valign="top"><table width="100%" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                      <td style="padding-top:6px;" width="28" valign="top" align="right"><img editable="true" mc:edit="bottom-icon-2" src="https://image.ibb.co/cTg7TJ/footer_icon_2.png" alt="icon" style="display:block; border: none;" width="33" height="22"></td>
                      <td mc:edit="subscribe" style="padding-left:15px;" valign="top" align="left"><singleline><a style="font-family: 'Open Sans', sans-serif; font-size: 24px; font-weight: normal; color: #ffffff; text-decoration: none;" href="#" target="_blank">Sign in!</a></singleline></td>
                    </tr>
                  </tbody></table></td>
                </tr>
              </tbody></table></td>
            <td width="5">&nbsp;</td>
          </tr>
        </tbody></table>
        <!--Icon Bottom End-->
        
        </td>
          </tr>
          <tr>
            <td valign="top" bgcolor="#569ce3">
            
            <!--Footer Start-->
    <table class="main" width="610" cellspacing="0" cellpadding="0" border="0" align="center">
      <tbody><tr>
        <td width="5">&nbsp;</td>
        <td style="padding:21px 0px 33px 0px;" valign="top"><table width="100%" cellspacing="0" cellpadding="0" border="0">
          <tbody><tr>
            <td class="footerbox" width="50%" valign="top">
            
            
            <!--Address Contact Start-->
        <table width="100%" cellspacing="0" cellpadding="0" border="0" align="left">
          <tbody><tr>
            <td valign="top"><table width="100%" cellspacing="0" cellpadding="0" border="0">
              <tbody><tr>
                <td valign="top">


                <!--Social Top Start-->
                <table class="social-top" width="245" cellspacing="0" cellpadding="0" border="0">
                    <tbody><tr>
                      <td align="center"><table width="245" cellspacing="0" cellpadding="0" border="0">
                        <tbody><tr>
                          <td><a href="https://www.facebook.com/WeCounselors" target="_blank"><img editable="true" mc:edit="social-1" src="https://image.ibb.co/i16hTJ/facebook.png" alt="social icon" style="display:block; border: none;" width="45" height="45"></a></td>
                          <td width="5">&nbsp;</td>
                          <td><a href="https://twitter.com/CounselorsWe" target="_blank"><img editable="true" mc:edit="social-2" src="https://image.ibb.co/bSgMNd/twitter.png" alt="social icon" style="display:block; border: none;" width="45" height="45"></a></td>
                          <td width="5">&nbsp;</td>
                          <td><a href="https://www.instagram.com/wecounselors/" target="_blank"><img editable="true" mc:edit="social-3" src="https://image.ibb.co/irUK8J/instagram.png" alt="social icon" style="display:block; border: none;" width="45" height="45"></a></td>
                          <td width="5">&nbsp;</td>
                          <td><a href="https://www.youtube.com/channel/UCQ90JZL4_8BzplAgeyihY4Q" target="_blank"><img editable="true" mc:edit="social-4" src="https://image.ibb.co/hM6kFy/you_tube.png" alt="social icon" style="display:block; border: none;" width="45" height="45"></a></td>
                          <td width="5">&nbsp;</td>
                          <td><a href="https://plus.google.com/109964264678439138018" target="_blank"><img editable="true" mc:edit="social-5" src="https://image.ibb.co/ikLvFy/googleplus.png" alt="social icon" style="display:block; border: none;" width="45" height="45"></a></td>
                        </tr>
                      </tbody></table></td>
                    </tr>
                  </tbody></table>
                  <!--Social Top End-->
                  <!-- <table width="245" cellspacing="0" cellpadding="0" border="0" align="left">
                    <tbody><tr>
                      <td valign="top">
                      <table width="100%" cellspacing="0" cellpadding="0" border="0">
                          <tbody><tr>
                            <td style="padding-right:5px;"><a href="#"><img mc:edit="footer-social-01" src="img/social-icon-footer-1.png" alt="social media" style="display:block; border: none;" width="45" height="45"></a></td>
                            <td style="padding-right:5px;"><a href="#"><img mc:edit="footer-social-02" src="img/social-icon-footer-2.png" alt="social media" style="display:block; border: none;" width="45" height="45"></a></td>
                            <td style="padding-right:5px;"><a href="#"><img mc:edit="footer-social-03" src="img/social-icon-footer-3.png" alt="social media" style="display:block; border: none;" width="45" height="45"></a></td>
                            <td style="padding-right:5px;"><a href="#"><img mc:edit="footer-social-04" src="img/social-icon-footer-4.png" alt="social media" style="display:block; border: none;" width="45" height="45"></a></td>
                            <td><a href="#"><img mc:edit="footer-social-05" src="img/social-icon-footer-5.png" alt="social media" style="display:block; border: none;" width="45" height="45"></a></td>
                            <td width="55">&nbsp;</td>
                          </tr>
                        </tbody></table>
                      </td>
                    </tr>
                  </tbody></table> -->
                </td>
              </tr>
              <tr>
                <td mc:edit="address-contact" style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: normal; color: #ffffff; line-height: 18px; text-align:left; padding-top:17px;" valign="top"><singleline>© Copyright 2018 - All Rights Reserved<br>
                  We Counselers 4884 Lilac Lane<br>
                  Statesboro, GA 30458<br>
                  <a style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: normal; color: #ffffff; text-decoration: none;" href="#">info@mail.com</a></singleline></td>
              </tr>
              </tbody></table></td>
          </tr>
        </tbody></table>
        <!--Address Contact End-->
            
            
            </td>
            <td class="footerbox" width="50%" valign="top">
            
            
            <!--Footer Link Start-->
        <table width="100%" cellspacing="0" cellpadding="0" border="0" align="right">
          <tbody><tr>
            <td class="hidebox-footer" style="display:none;"></td>
          </tr>
          <tr>
            <td valign="top"><table width="100%" cellspacing="0" cellpadding="0" border="0">
              <tbody><tr>
                <td width="50%" valign="top"><table width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody><tr>
                    <td mc:edit="recommend" style="font-family: 'Open Sans', sans-serif; font-size: 20px; font-weight: 400; color: #ffffff; line-height: 24px; text-align:left; padding-top:12px;" valign="top"><singleline>Recommend</singleline></td>
                  </tr>
                  <tr>
                    <td class="textfooter" style="padding-top:8px;" mc:edit="recommend-link" valign="top" align="left">
                      <singleline><a style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: normal; color: #ffffff; text-decoration: none;" href="#" target="_blank">Terms &amp; Conditions </a>
                      </singleline><br>
                      <singleline><a style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: normal; color: #ffffff; text-decoration: none;" href="#" target="_blank">Ownership Statement </a></singleline><br>
                      <singleline><a style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: normal; color: #ffffff; text-decoration: none;" href="#" target="_blank">Refund Policy </a></singleline><br>
                      <singleline><a style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: normal; color: #ffffff; text-decoration: none;" href="#" target="_blank">Privacy Policy</a></singleline><br>
                      </td>
                  </tr>
                </tbody></table></td>
                <td style="padding-left:10px;" width="50" valign="top"><table width="100%" cellspacing="0" cellpadding="0" border="0">
                  <tbody><tr>
                    <td mc:edit="newsletter" style="font-family: 'Open Sans', sans-serif; font-size: 20px; font-weight: 400; color: #ffffff; line-height: 24px; text-align:left; padding-top:12px;" valign="top"><singleline>Newsletter</singleline></td>
                  </tr>
                  <tr>
                    <td class="textfooter" style="padding-top:8px;" mc:edit="newsletter-link" valign="top" align="left"><singleline><a style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: normal; color: #ffffff; text-decoration: none;" href="#" target="_blank">Forward</a></singleline><br>
                      <singleline><a style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: normal; color: #ffffff; text-decoration: none;" href="#" target="_blank"><unsubscribe>Unsubscribe</unsubscribe></a></singleline><br>
                      <singleline><a style="font-family: 'Open Sans', sans-serif; font-size: 13px; font-weight: normal; color: #ffffff; text-decoration: none;" href="#" target="_blank">Home</a></singleline></td>
                  </tr>
                </tbody></table></td>
              </tr>
            </tbody></table></td>
          </tr>
        </tbody></table>
        <!--Footer Link End-->
            
            
            </td>
          </tr>
        </tbody></table></td>
        <td width="5">&nbsp;</td>
      </tr>
    </tbody></table>
    <!--Footer End-->
            
            </td>
          </tr>
        </tbody></table>
        </td>
      </tr>
    </tbody></table></td>
  </tr>
</tbody></table>
<!--Table End-->



</body></html>
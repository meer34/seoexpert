<?php
	include 'static/head.php';
	include 'static/fixedSidebarLeft.php';	
	include 'static/fixedSidebarRight.php';
	include 'static/navHeader.php';
?>
<!-- Main Header Groups -->

<div class="main-header">

<div id="jssor_1" style="position:relative;margin:0 auto;top:0px;left:0px;width:1200px;height:400px;overflow:hidden;visibility:hidden;">
	<!-- Loading Screen -->
	<div data-u="loading" style="position:absolute;top:0px;left:0px;background:url('img/loading.gif') no-repeat 50% 50%;background-color:rgba(0, 0, 0, 0.7);"></div>
	<div data-u="slides" style="cursor:default;position:relative;top:0px;left:0px;width:1300px;height:500px;overflow:hidden;">
		<div>
			<img data-u="image" src="<?= base_url(); ?>assets/img/slide1.png" />
		</div>
		<div>
			<img data-u="image" src="<?= base_url(); ?>assets/img/slide2.jpg" />
		</div>
		<div>
			<img data-u="image" src="<?= base_url(); ?>assets/img/slide3.jpg" />
		</div>
	</div>
	<!-- Bullet Navigator -->
	<div data-u="navigator" class="jssorb05" style="bottom:16px;right:16px;" data-autocenter="1">
		<!-- bullet navigator item prototype -->
		<div data-u="prototype" style="width:16px;height:16px;"></div>
	</div>
	<!-- Arrow Navigator -->
	<div data-u="arrowleft" class="jssora031" style="width:50px;height:50px;top:0px;left:20px;" data-autocenter="2">
		<svg viewbox="-12986 -2977 16000 16000" style="width:100%;height:100%;">
			<polygon class="a" points="-1182.1,12825.5 -792,12435.4 -8204.5,5023 -792,-2389.4 -1182.1,-2779.5 -8984.8,5023"></polygon>
		</svg>
	</div>
	<div data-u="arrowright" class="jssora031" style="width:50px;height:50px;top:0px;right:20px;" data-autocenter="2">
		<svg viewbox="-12986 -2977 16000 16000" style="width:100%;height:100%;">
			<polygon class="a" points="-8594.7,12825.5 -8984.8,12435.4 -1572.3,5023 -8984.8,-2389.4 -8594.7,-2779.5 -792,5023"></polygon>
		</svg>
	</div>
</div>


</div>
<!-- ... end Main Header Groups -->

<div class="container">
	<div class="row">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block responsive-flex1200">
				<div class="ui-block-title">
					<ul class="filter-icons  fa-icon">
						<li>
							<a href="#">
								<i class="fa fa-facebook"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-twitter"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-instagram"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-youtube"></i>
							</a>
						</li>
						<li>
							<a href="#">
								<i class="fa fa-google-plus"></i>
							</a>
						</li>

					</ul>
					<div class="w-select">
						<div class="title">Filter By:</div>
						<fieldset class="form-group">
							<select class=" form-control">
								<option value="NU">All Categories</option>
								<option value="NU">Favourite</option>
								<option value="NU">Likes</option>
							</select>
						</fieldset>
					</div>

					<div class="w-select">
						<fieldset class="form-group label-floating is-select">
							<label class="control-label"></label>
							<select class=" form-control">
								<option value="NU">Rating (Top)</option>
								<option value="NU">Rating (Lower)</option>
							</select>
						</fieldset>
					</div>

					<div class="w-select">
						<fieldset class="form-group">
							<select class=" form-control">
								<option value="NU">Price (Low)</option>
								<option value="NU">Price (High)</option>
							</select>
						</fieldset>
					</div>

					<a href="#" data-toggle="modal" data-target="#create-photo-album" class="btn btn-primary btn-md-2">Filter</a>
<!--
					<form class="w-search">
						<div class="form-group with-button">
							<input class="form-control" type="text" placeholder="Search Blog Posts......">
							<button>
								<svg class="olymp-magnifying-glass-icon"><use xlink:href="svg-icons/sprites/icons.svg#olymp-magnifying-glass-icon"></use></svg>
							</button>
						</div>
					</form>
					-->
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="col-xl-12">
<!-- Main Content Groups -->
<?php
$query = $this->db->query('SELECT `user_id`, `fname`, `lname`, `profile_pic`, `short_desc`, `availability` FROM users WHERE isExpert = 1 ORDER BY `availability` ASC');
$profiles = $query->result();
foreach($profiles as $profile):
	echo '<div class="photo-album-item-wrap col-4-width" >
			<div class="photo-album-item" data-mh="album-item">
				<div class="crumina-module crumina-teammembers-item teammembers-thumb" style="margin-bottom: 10px;">
					<img class="main" src="'.base_url($profile->profile_pic) .'" alt="team member" height="200px">
					<img class="hover" src="'. base_url($profile->profile_pic) .'" alt="team member">
				</div>';
				if ($profile->availability == 3){
				echo '<a class="btn btn-danger btn-sm" style="padding: 5px; color: white;">Offline</a>';
				} else if ($profile->availability == 1) {
					echo '<a class="btn btn-success btn-sm" style="padding: 5px; color: white;">Online</a>';
				} else if ($profile->availability == 2) {echo '<a class="btn btn-yellow btn-sm" style="padding: 5px; color: white;">Busy</a>';}
				echo '<div class="content">
					<a href="'. base_url('live/expert/'.$profile->user_id) .'" class="title h5">'. $profile->fname.' '.$profile->lname .'</a>';

					$rating_query = $this->db->query('SELECT `id`, `clientId`, `expertId`, AVG(`rating`) AS rating, `review`, `time` FROM rating WHERE expertId = '.$profile->user_id);
					$rating_result = $rating_query->row();

						echo '<div class="mstar">';

						
							if( (round($rating_result->rating)) == 0){
								echo '<i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
							}
							if( (round($rating_result->rating)) == 1){
								echo '<i class="fa fa-star checked"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
							}
							if( (round($rating_result->rating)) == 2){
								echo '<i class="fa fa-star checked"></i><i class="fa fa-star checked"></i><i class="fa fa-star"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
							}
							if( (round($rating_result->rating)) == 3){
								echo '<i class="fa fa-star checked"></i><i class="fa fa-star checked"></i><i class="fa fa-star checked"></i><i class="fa fa-star"></i><i class="fa fa-star"></i>';
							}
							if( (round($rating_result->rating)) == 4){
								echo '<i class="fa fa-star checked"></i><i class="fa fa-star checked"></i><i class="fa fa-star checked"></i><i class="fa fa-star checked"></i><i class="fa fa-star"></i>';
							}
							if( (round($rating_result->rating)) == 5){
								echo '<i class="fa fa-star checked"></i><i class="fa fa-star checked"></i><i class="fa fa-star checked"></i><i class="fa fa-star checked"></i><i class="fa fa-star checked"></i>';
							}

							/*<i class="star star-under fa fa-star">
								<i class="star star-over fa fa-star"></i>
							</i>
							<i class="star star-under fa fa-star">
								<i class="star star-over fa fa-star"></i>
							</i>
							<i class="star star-under fa fa-star">
								<i class="star star-over fa fa-star"></i>
							</i>
							<i class="star star-under fa fa-star">
								<i class="star star-over fa fa-star"></i>
							</i>
							<i class="star star-under fa fa-star">
								<i class="star star-over fa fa-star"></i>
							</i>*/

						echo '</div>

					<div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<div class="control-block-button" data-swiper-parallax="-100">
									<div>
										<ul class="socials socials--round">
											<li>
												<a href="" class="social-item olympus">
													<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
													<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" xml:space="preserve">
													<metadata> Svg Vector Icons : http://www.onlinewebfonts.com/icon </metadata>
													<g><path d="M10.5,302.8c0,0-14.8-241.4,286.8-241.4s461.8,0,461.8,0S990,79.3,990,302.8c0,223.5,0,177.1,0,177.1s0,220.4-233,220.4c-178.2,0-53.8,0-53.8,0v238.3L423.8,701.4H231.9c0,0-220.6-19-221.5-264.6C9.5,191.1,10.5,302.8,10.5,302.8L10.5,302.8z M758.3,288.1c-46,0-83.3,37.3-83.3,83.3c0,46,37.3,83.3,83.3,83.3s83.3-37.3,83.3-83.3C841.5,325.4,804.3,288.1,758.3,288.1z M512.6,288.1c-46,0-83.3,37.3-83.3,83.3c0,46,37.3,83.3,83.3,83.3c46,0,83.3-37.3,83.3-83.3C595.9,325.4,558.6,288.1,512.6,288.1z M266.9,288.1c-46,0-83.3,37.3-83.3,83.3c0,46,37.3,83.3,83.3,83.3c46,0,83.3-37.3,83.3-83.3C350.2,325.4,312.9,288.1,266.9,288.1z"/></g>
													</svg>
												</a>
											</li>

											<li>
												<a href="" class="social-item facebook">
													<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
													<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" xml:space="preserve">
													<metadata> Svg Vector Icons : http://www.onlinewebfonts.com/icon </metadata>
													<g><path d="M990,745c0,21.5-6,41.4-15.7,58.9L664.8,457.7l306.1-267.8C982.8,208.8,990,231,990,255V745z M500,520.6l425.9-372.6c-17.5-9.6-37.1-15.4-58.4-15.4h-735c-21.3,0-40.9,5.9-58.3,15.4L500,520.6z M618.7,498l-98.6,86.3c-5.8,5-12.9,7.5-20.2,7.5s-14.4-2.5-20.2-7.5L381.2,498L67.9,848.7c18.8,11.8,40.8,18.8,64.6,18.8h735c23.8,0,45.8-7.1,64.6-18.8L618.7,498z M29.1,189.9C17.2,208.8,10,231,10,255v490c0,21.5,6,41.4,15.7,58.9l309.4-346.3L29.1,189.9z"/></g>
													</svg>
												</a>
											</li>

											<li>
												<a href="" class="social-item twitter">
													<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
													<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" xml:space="preserve">
													<metadata> Svg Vector Icons : http://www.onlinewebfonts.com/icon </metadata>
													<g><g><path d="M635.2,273.1L848.6,59.7l-25.2-25c-32.5-32.5-89.4-32.5-122,0L610.3,126C594,142.3,585,164,585,187c0,23,9,44.7,25.2,60.9L635.2,273.1L635.2,273.1z"/><path d="M248.4,609.8c-32.5-32.5-89.4-32.5-122,0L35.2,701C19,717.3,10,739,10,762s9,44.7,25.4,61.1l25.1,24.8L273.4,635L248.4,609.8L248.4,609.8z"/><path d="M926.4,137.7L906,117.4L692.8,330.6l37.4,37.3c5.5,5.5,8.4,12.7,8.4,20.4c0,7.7-3,14.9-8.4,20.4L409,729.8c-10.9,10.8-29.8,10.9-40.7,0L331,692.4L118.1,905.7l20.4,20.4c14.9,14.9,71.6,63.5,175.7,63.5c92.9,0,244.7-40.9,439.9-236C1143.5,364,935.6,147,926.4,137.7L926.4,137.7z"/></g></g>
													</svg>
												</a>
											</li>
										</ul>
									</div>


								</div>
							</div>

							<div class="swiper-slide">

							<div class="teammembers-item-prof">'. $profile->short_desc .'</div>
							<!-- If we need pagination
							<div class="specialization">
							<p>Specialized in love and relationship carrers, finance carrers, finance carrers, finance</p>
							</div>
							-->
							</div>
						</div>
						<!-- If we need pagination -->
						<div class="swiper-pagination"></div>
					</div>
				</div>
			</div>
		</div>';
	endforeach; ?>
	<a id="load-more-button" href="#" class="btn btn-control btn-more" data-load-link="products-load-moreV2.html" data-container="products-grid-1">
			<svg class="olymp-three-dots-icon"><use xlink:href="<?= base_url();?>assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
			<div class="ripple-container"></div>
		</a>
</div>
</div>

<!-- <div class="container">
	<div class="col-xl-12">
		<?php foreach($experts as $expert) : ?>
		<div class="photo-album-item-wrap col-4-width" >
			<div class="photo-album-item" data-mh="album-item">
				<div class="crumina-module crumina-teammembers-item teammembers-thumb" style="margin-bottom: 10px;">
					<img class="main" src="<?=base_url($expert['profile_pic']);?>" alt="team member" height="200px">
					<img class="hover" src="<?=base_url($expert['profile_pic']);?>" alt="team member">
				</div>
				<?php if ($expert['availability'] == 3){
				echo '<a class="btn btn-danger btn-sm" style="padding: 5px; color: white;">Offline</a>';
				} else if ($expert['availability'] == 1) {
					echo '<a class="btn btn-success btn-sm" style="padding: 5px; color: white;">Online</a>';
				} else if ($expert['availability'] == 2) {echo '<a class="btn btn-yellow btn-sm" style="padding: 5px; color: white;">Busy</a>';}?>
				<div class="content">
					<a href="<?= base_url('live/expert/'.$expert['user_id']); ?>" class="title h5"><?= $expert['fname'].' '.$expert['lname'].' '.round($rating) ?></a>

						

						<div class="mstar">
							<i class="star star-under fa fa-star">
								<i class="star star-over fa fa-star"></i>
							</i>
							<i class="star star-under fa fa-star">
								<i class="star star-over fa fa-star"></i>
							</i>
							<i class="star star-under fa fa-star">
								<i class="star star-over fa fa-star"></i>
							</i>
							<i class="star star-under fa fa-star">
								<i class="star star-over fa fa-star"></i>
							</i>
							<i class="star star-under fa fa-star">
								<i class="star star-over fa fa-star"></i>
							</i>
						</div>

					<div class="swiper-container">
						<div class="swiper-wrapper">
							<div class="swiper-slide">
								<div class="control-block-button" data-swiper-parallax="-100">
									<div>
										<ul class="socials socials--round">
											<li>
												<a href="" class="social-item olympus">
													<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
													<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" xml:space="preserve">
													<metadata> Svg Vector Icons : http://www.onlinewebfonts.com/icon </metadata>
													<g><path d="M10.5,302.8c0,0-14.8-241.4,286.8-241.4s461.8,0,461.8,0S990,79.3,990,302.8c0,223.5,0,177.1,0,177.1s0,220.4-233,220.4c-178.2,0-53.8,0-53.8,0v238.3L423.8,701.4H231.9c0,0-220.6-19-221.5-264.6C9.5,191.1,10.5,302.8,10.5,302.8L10.5,302.8z M758.3,288.1c-46,0-83.3,37.3-83.3,83.3c0,46,37.3,83.3,83.3,83.3s83.3-37.3,83.3-83.3C841.5,325.4,804.3,288.1,758.3,288.1z M512.6,288.1c-46,0-83.3,37.3-83.3,83.3c0,46,37.3,83.3,83.3,83.3c46,0,83.3-37.3,83.3-83.3C595.9,325.4,558.6,288.1,512.6,288.1z M266.9,288.1c-46,0-83.3,37.3-83.3,83.3c0,46,37.3,83.3,83.3,83.3c46,0,83.3-37.3,83.3-83.3C350.2,325.4,312.9,288.1,266.9,288.1z"/></g>
													</svg>
												</a>
											</li>

											<li>
												<a href="" class="social-item facebook">
													<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
													<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" xml:space="preserve">
													<metadata> Svg Vector Icons : http://www.onlinewebfonts.com/icon </metadata>
													<g><path d="M990,745c0,21.5-6,41.4-15.7,58.9L664.8,457.7l306.1-267.8C982.8,208.8,990,231,990,255V745z M500,520.6l425.9-372.6c-17.5-9.6-37.1-15.4-58.4-15.4h-735c-21.3,0-40.9,5.9-58.3,15.4L500,520.6z M618.7,498l-98.6,86.3c-5.8,5-12.9,7.5-20.2,7.5s-14.4-2.5-20.2-7.5L381.2,498L67.9,848.7c18.8,11.8,40.8,18.8,64.6,18.8h735c23.8,0,45.8-7.1,64.6-18.8L618.7,498z M29.1,189.9C17.2,208.8,10,231,10,255v490c0,21.5,6,41.4,15.7,58.9l309.4-346.3L29.1,189.9z"/></g>
													</svg>
												</a>
											</li>

											<li>
												<a href="" class="social-item twitter">
													<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">
													<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1000 1000" enable-background="new 0 0 1000 1000" xml:space="preserve">
													<metadata> Svg Vector Icons : http://www.onlinewebfonts.com/icon </metadata>
													<g><g><path d="M635.2,273.1L848.6,59.7l-25.2-25c-32.5-32.5-89.4-32.5-122,0L610.3,126C594,142.3,585,164,585,187c0,23,9,44.7,25.2,60.9L635.2,273.1L635.2,273.1z"/><path d="M248.4,609.8c-32.5-32.5-89.4-32.5-122,0L35.2,701C19,717.3,10,739,10,762s9,44.7,25.4,61.1l25.1,24.8L273.4,635L248.4,609.8L248.4,609.8z"/><path d="M926.4,137.7L906,117.4L692.8,330.6l37.4,37.3c5.5,5.5,8.4,12.7,8.4,20.4c0,7.7-3,14.9-8.4,20.4L409,729.8c-10.9,10.8-29.8,10.9-40.7,0L331,692.4L118.1,905.7l20.4,20.4c14.9,14.9,71.6,63.5,175.7,63.5c92.9,0,244.7-40.9,439.9-236C1143.5,364,935.6,147,926.4,137.7L926.4,137.7z"/></g></g>
													</svg>
												</a>
											</li>
										</ul>
									</div>


								</div>
							</div>

							<div class="swiper-slide">

							<div class"teammembers-item-prof"><?= $expert['short_desc'] ?></div> If we need pagination
								<div class="specialization">
									<p>Specialized in love and relationship carrers, finance carrers, finance carrers, finance</p>
								</div>
							</div>
						</div>
						 If we need pagination 
						<div class="swiper-pagination"></div>
					</div>
				</div>
			</div>
		</div>
			<?php endforeach; ?>
		<a id="load-more-button" href="#" class="btn btn-control btn-more" data-load-link="products-load-moreV2.html" data-container="products-grid-1">
			<svg class="olymp-three-dots-icon"><use xlink:href="<?= base_url();?>assets/svg-icons/sprites/icons.svg#olymp-three-dots-icon"></use></svg>
			<div class="ripple-container"></div>
		</a>
	</div>
</div> -->

<!-- ... end Main Content Groups -->

				

	 <script>
		var rating = 4.633493;
		rateStyleJQ(rating, 'mstar');
		// Star Rating
		function rateStyleJQ(num, divID) {
			var ratingRounded = Math.floor(num);
			$("."+divID+" .star-over").slice(0, ratingRounded).addClass('star-visible');
		  	var partialShade = Math.round((num-ratingRounded)*100);
		  	if (partialShade!= 0) {
		  	$($("."+divID+" .star-over").get(ratingRounded)).addClass('star-visible').css("width", partialShade+"%");
		}
		}
	</script>
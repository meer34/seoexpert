
<?php
	if(!isset($_SESSION['login']) && !isset($_SESSION['id']))
	{
		redirect('welcome');
	}
	if($this->session->userdata('isVerified') == "0")	
	{
		echo 'An email has been sent to you with verification code, Please Verifie your Email';
		echo form_open('userprofile/emailVerification');
			echo '<div class="form-group label-floating">';
				echo '<label class="control-label">Verification Code: </label>';
				echo form_input(['class'=>'form-control','type'=>'hidden', 'name'=>'userEmail', 'value' => $this->session->userdata('email')]);
				echo form_input(['class'=>'form-control','type'=>'number', 'name'=>'verificationCode', 'placeholde' => 'code']);
				echo form_submit(['name'=>'register','value'=>'Registration','class'=>'btn btn-primary btn-lg full-width']);
			echo '</div>';
		echo '</form>';
		die;
	}
	include 'static/head.php';
	include 'static/fixedSidebarLeft.php';
	include 'static/navHeader.php';
	include 'static/fixedSidebarRight.php';
?>

<div style="margin-top:20px;"></div>

<!-- Top Header-Profile -->

<div class="container">
	<div class="row">
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="top-header">
					<div class="top-header-thumb">						
						<img src="<?php echo base_url().$cover_pic;?>" alt="nature" class="cover-pic" width="100%" height="640px">
					</div>					
					<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#cover_pic_model">Upload Cover</button>
					<div class="profile-section">
						<div class="row">
							<div class="col-lg-5 col-md-5 ">
							</div>
						</div>
					</div>
					<div class="top-header-author">

						<a href="02-ProfilePage.html" class="author-thumb">							
							<img src="<?php echo base_url().$profile_pic;?>" alt="nature" class="img-fluid" width="100%" height="100%">
						</a>
						<button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#profile_pic_model">Upload Profile</button>						  
						<div class="author-content">
							<?php echo '<a href="#" class="h4 author-name">'.$_SESSION['name'].'</a>' ?>							
							<div class="country"><?php echo $city.', '.$country ?></div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<div class="container">
	<div class="row">        
		<div class="col-xl-12 order-xl-2 col-lg-12 order-lg-2 col-md-12 order-md-1 col-sm-12 col-xs-12">
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title" >Personal Information</h6>
				</div>
				<div class="ui-block-content">					
					<!-- Personal Information Form  -->		
					<?php echo form_open('userprofile/user_edit','class = "container" enctype="multipart/form-data"'); ?>			
					<div class="row">					
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="form-group label-floating">
								<label class="control-label">First Name</label>									
								<?= form_input(['class'=>'form-control', 'name'=>'fname', 'value' => $fname]); ?>
								<?= form_error('fname'); ?>
							</div>
							<div class="form-group label-floating">
								<label class="control-label">Username</label>									
									<?= form_input(['class'=>'form-control','name'=>'username','disabled'=>'disabled','value'=>$user_nick]); ?>
									<?= form_error('username'); ?>
							</div>
							<div class="form-group label-floating">
								<label class="control-label">Your Password</label>
								<?= form_password(['class'=>'form-control','name'=>'uPassword','value'=>set_value('uPassword')]); ?>
								<?= form_error('uPassword'); ?>
							</div>
							<div class="form-group date-time-picker label-floating">
								<label class="control-label">Your Birthday</label>
								<?= form_input(['class'=>'form-control', 'name'=>'datetimepicker', 'value'=>$user_bd]); ?>
								<?= form_error('datetimepicker'); ?>
								<span class="input-group-addon">
									<svg class="olymp-month-calendar-icon icon"><use xlink:href="<?= base_url();?>assets/svg-icons/sprites/icons.svg#olymp-month-calendar-icon"></use></svg>
								</span>
							</div>
							<div class="form-group label-floating">
								<label class="control-label">Enter your phone number</label>
								<?= form_input(['class'=>'form-control','id'=>'phone','value'=>$user_phone, 'type'=>'tel', 'name'=>'phone']); ?>
								<?= form_error('phone'); ?>
							</div>
						</div>
				
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="form-group label-floating">
								<label class="control-label">Last Name</label>
								<?=  form_input(['class'=>'form-control', 'name'=>'lname','value'=>$lname]); ?>
								<?= form_error('lname'); ?>
							</div>
							<div class="form-group label-floating">
								<label class="control-label">Your Email</label>
								<?= form_input(['class'=>'form-control', 'name'=>'email','type'=>'email','disabled'=>'disabled','value'=>$user_email]); ?>
								<?= form_error('email'); ?>
							</div>
							
							<div class="form-group label-floating">
								<label class="control-label">Password Confirm</label>
								<?= form_password(['class'=>'form-control','name'=>'uCpassword','value'=>set_value('uCpassword')]); ?>
								<?= form_error('uCpassword'); ?>
							</div>
							<div class="form-group label-floating is-select">
								<label class="control-label">Your Gender</label>
									<select name = "gender" class = "" >
									<option <?php echo ($user_gender == 'M') ? "selected = 'selected'" : ""; ?> value = "Male">Male</option>
									<option <?php echo ($user_gender == 'F') ? "selected = 'selected'" : ""; ?>  value = "Female">Female</option>
								</select>
								<?php								
									echo form_error('gender');
								?>
							</div>
						</div>
						<?php if($this->session->userdata('isExpert') == 1){ ?>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="form-group label-floating">
								<label class="control-label">Write a little description about you (100 max)</label>
								<?= form_textarea(['class'=>'form-control','name'=>'smallDisc','rows'=>'5','maxlength'=>'100',
								'value'=>$short_desc]); ?>
								<?= form_error('smallDics'); ?>
							</div>
						</div>
					
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="form-group label-floating">
								<label class="control-label">Write a detailed description about you</label>
								<?= form_textarea(['class'=>'form-control','name'=>'longDisc','rows'=>'5','maxlength'=>'500','value'=>$bio]); ?>
								<?= form_error('longDisc'); ?>
							</div>
						</div>
						<?php } ?>
					</div>					
					<!-- ... end Personal Information Form  -->
				</div>
			</div>
			<div class="ui-block">
				<div class="ui-block-title">
					<h6 class="title">Address</h6>
				</div>
				<div class="ui-block-content">					
					<!-- Personal Information Form  -->
					<div class="row">					
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
							<div class="form-group label-floating">
								<label class="control-label">Select your country</label>								
									<?= form_input(['class'=>'form-control', 'name'=>'country','type'=>'text','value'=>$country,'list'=>'list']); ?>
									<?= form_error('country'); ?>
									<datalist id="list">									
										<option>pakistan</option>
										<option>pakistan</option>
									</datalist>					
							</div>					
							<div class="form-group label-floating">
								<label class="control-label">Your City</label>									
								<?= form_input(['class'=>'form-control', 'name'=>'city','type'=>'text','value'=>$city,'list'=>'citylist']); ?>
								<?= form_error('city'); ?>
								<datalist id="citylist">										
									<option>lhr</option>
									<option>grw</option>
								</datalist>
							</div>
						</div>
				
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">					
							<div class="form-group label-floating">
								<label class="control-label">Your State / Province</label>
								<?= form_input(['class'=>'form-control', 'name'=>'state','type'=>'text','value'=>$state_province,'list'=>'statelist']); ?>
								<?= form_error('state'); ?>
								<datalist id="statelist">										
									<option>punjab</option>
									<option>kpk</option>
								</datalist>
							</div>
							<div class="form-group label-floating">
								<label class="control-label">Street</label>
								<?= form_input(['class'=>'form-control','name'=>'street','value'=>$street]); ?>
								<?= form_error('street'); ?>
							</div>					
						</div>
					</div>					
					<!-- ... end Personal Information Form  -->
					</div>
					</div>
					<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<?= form_submit(['name'=>'register','value'=>'Registration','class'=>'btn btn-primary btn-lg full-width']); ?>
					</div>	
					</form>					
					<!-- ... end Personal Information Form  -->
				</div>
			</div>
		</div>
	</div>
</div>

<!-- ... end Your Account Personal Information -->

<a class="back-to-top" href="#">
	<img src="<?= base_url(); ?>assets/svg-icons/back-to-top.svg" alt="arrow" class="back-icon">
</a>

<?php include 'static/footer.php'; ?>
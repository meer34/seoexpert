<!DOCTYPE html>
<html lang="en">
<head>

	<title><?= $this->session->flashdata('title'); ?></title>

	<!-- Required meta tags always come first -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="x-ua-compatible" content="ie=edge">

	<!-- Bootstrap CSS -->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/Bootstrap/dist/css/bootstrap-reboot.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/Bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/Bootstrap/dist/css/bootstrap.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/rateYo/2.3.2/jquery.rateyo.min.css">

	<!-- Main Styles CSS -->
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/main.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/fonts.min.css">
	<link rel="stylesheet" type="text/css" href="<?= base_url(); ?>assets/css/style.css">

	<!-- Main Font -->
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/fontawesome.css" integrity="sha384-GVa9GOgVQgOk+TNYXu7S/InPTfSDTtBalSgkgqQ7sCik56N9ztlkoTr2f/T44oKV" crossorigin="anonymous">
	<!-- <script src="<?= base_url(); ?>assets/fonts/fontawesome.min.js"></script>
	<script src="<?= base_url(); ?>assets/fonts/fontawesome-all.min.js"></script> -->
	<script src="<?= base_url(); ?>assets/js/webfontloader.min.js"></script>
	<script src="<?= base_url(); ?>assets/js/jquery-3.2.1.js"></script>
	<script>
		WebFont.load({
			google: {
				families: ['Roboto:300,400,500,700:latin']
			}
		});
	</script>
	<!-- includes in head -->
	<!-- <script src="<?= base_url(); ?>assets/js/jquery-1.11.3.min.js" type="text/javascript"></script> -->
		<script src="<?= base_url(); ?>assets/js/jssor.slider-24.1.5.min.js" type="text/javascript"></script>
		<script type="text/javascript">
			jQuery(document).ready(function ($) {
	
				var jssor_1_SlideoTransitions = [
				  [{b:900,d:2000,x:-379,e:{x:7}}],
				  [{b:900,d:2000,x:-379,e:{x:7}}],
				  [{b:-1,d:1,o:-1,sX:2,sY:2},{b:0,d:900,x:-171,y:-341,o:1,sX:-2,sY:-2,e:{x:3,y:3,sX:3,sY:3}},{b:900,d:1600,x:-283,o:-1,e:{x:16}}]
				];
	
				var jssor_1_options = {
				  $AutoPlay: 1,
				  $SlideDuration: 800,
				  $SlideEasing: $Jease$.$OutQuint,
				  $CaptionSliderOptions: {
					$Class: $JssorCaptionSlideo$,
					$Transitions: jssor_1_SlideoTransitions
				  },
				  $ArrowNavigatorOptions: {
					$Class: $JssorArrowNavigator$
				  },
				  $BulletNavigatorOptions: {
					$Class: $JssorBulletNavigator$
				  }
				};
	
				var jssor_1_slider = new $JssorSlider$("jssor_1", jssor_1_options);
	
				/*responsive code begin*/
				/*remove responsive code if you don't want the slider scales while window resizing*/
				function ScaleSlider() {
					var refSize = jssor_1_slider.$Elmt.parentNode.clientWidth;
					if (refSize) {
						refSize = Math.min(refSize, 1920);
						jssor_1_slider.$ScaleWidth(refSize);
					}
					else {
						window.setTimeout(ScaleSlider, 30);
					}
				}
				ScaleSlider();
				$(window).bind("load", ScaleSlider);
				$(window).bind("resize", ScaleSlider);
				$(window).bind("orientationchange", ScaleSlider);
				/*responsive code end*/
			});
		</script>

</head>
<body>
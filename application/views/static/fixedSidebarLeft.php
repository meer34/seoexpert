<!-- Fixed Sidebar Left -->

<div class="fixed-sidebar">
	<div class="fixed-sidebar-left sidebar--small" id="sidebar-left">

		<a href="<?php echo base_url(); ?>" class="logo">
			<div class="img-wrap">

				<img src="<?php echo base_url(); ?>assets/img/logo.png" alt="Olympus">

				

			</div>
		</a>

		<div class="mCustomScrollbar" data-mcs-theme="dark">
			<ul class="left-menu">
				<li>
					<a href="#" class="js-sidebar-open">
						<svg class="olymp-menu-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="OPEN MENU"><use xlink:href="<?php echo base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-menu-icon"></use></svg>
					</a>
				</li>
				<?php foreach ($sidebar as  $value) {
					# code...
				?>
				<li>
					<a href="<?php echo base_url() ?>welcome/about">
						<img src="<?php echo base_url().$value->icon;?>" height="40px" width="40px">
					</a>
				</li>
			<?php }	?>
				
			</ul>
		</div>
	</div>

	<div class="fixed-sidebar-left sidebar--large" id="sidebar-left-1">
		<a href="<?php echo base_url(); ?>" class="logo">
			<div class="img-wrap">
				<img src="<?php echo base_url(); ?>assets/img/logo.png" alt="Olympus">
			</div>
			<div class="title-block">
				<h6 class="logo-title">olympus</h6>
			</div>
		</a>

		<div class="mCustomScrollbar" data-mcs-theme="dark">
			<ul class="left-menu">
				<li>
					<a href="#" class="js-sidebar-open">
						<svg class="olymp-close-icon left-menu-icon"><use xlink:href="<?php echo base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
						<span class="left-menu-title">Collapse Menu</span>
					</a>
				</li>
				 <?php //print_r($sidebar);
				 foreach($sidebar as $side) { ?>
				<li>
					<a href="<?php echo base_url() ?>welcome/about">
						<img src="<?php echo base_url().$side->icon;?>" height="40px" width="40px">&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="left-menu-title">  <?php   echo $side->category;?></span>
					</a>
				</li>
				<?php } ?>
				
			</ul>

			<div class="profile-completion">

				<div class="skills-item">
					<div class="skills-item-info">
						<span class="skills-item-title">Profile Completion</span>
						<span class="skills-item-count"><span class="count-animate" data-speed="1000" data-refresh-interval="50" data-to="76" data-from="0"></span><span class="units">76%</span></span>
					</div>
					<div class="skills-item-meter">
						<span class="skills-item-meter-active bg-primary" style="width: 76%"></span>
					</div>
				</div>

				<span>Complete <a href="#">your profile</a> so people can know more about you!</span>

			</div>
		</div>
	</div>
</div>

<!-- ... end Fixed Sidebar Left -->


<!-- Fixed Sidebar Left -->

<div class="fixed-sidebar fixed-sidebar-responsive">

	<div class="fixed-sidebar-left sidebar--small" id="sidebar-left-responsive">
		<a href="<?php echo base_url(); ?>" class="logo js-sidebar-open">
			<img src="<?php echo base_url(); ?>assets/img/logo.png" alt="Olympus">
		</a>

	</div>

	<div class="fixed-sidebar-left sidebar--large" id="sidebar-left-1-responsive">
		<a href="<?php echo base_url(); ?>" class="logo">
			<div class="img-wrap">
				<img src="<?php echo base_url(); ?>assets/img/logo.png" alt="Olympus">
			</div>
			<div class="title-block">
				<h6 class="logo-title">olympus</h6>
			</div>
		</a>

		<div class="mCustomScrollbar" data-mcs-theme="dark">

			<div class="control-block">
				<?php if (isset($_SESSION['login']) && isset($_SESSION['id'])) {?>
				<div class="author-page author vcard inline-items">
					<div class="author-thumb">						
						<img alt="author" src="<?= base_url($this->session->userdata('profile')); ?>" width="36px" height="36px" class="avatar">
						<span class="icon-status online"></span>
					</div>
					<a href="02-ProfilePage.html" class="author-name fn">
						<div class="author-title">
							<?php echo @$_SESSION['name']; ?> <svg class="olymp-dropdown-arrow-icon"><use xlink:href="<?php echo base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-dropdown-arrow-icon"></use></svg>
						</div>
						<span class="author-subtitle">SPACE COWBOY</span>
					</a>
				</div>
				<?php } ?>
			</div>

			<div class="ui-block-title ui-block-title-small">
				<h6 class="title">MAIN SECTIONS</h6>
			</div>

			<ul class="left-menu">
				<li>
					<a href="#" class="js-sidebar-open">
						<svg class="olymp-close-icon left-menu-icon"><use xlink:href="<?php echo base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-close-icon"></use></svg>
						<span class="left-menu-title">Collapse Menu</span>
					</a>
				</li>
				 <?php //print_r($sidebar);
				 foreach($sidebar as $side) { ?>
				<li>
					<a href="<?php echo base_url() ?>welcome/about">
						<img src="<?php echo base_url().$side->icon;?>" height="40px" width="40px">&nbsp;&nbsp;&nbsp;&nbsp;
						<span class="left-menu-title">  <?php   echo $side->category;?></span>
					</a>
				</li>
				<?php } ?>
			</ul>
			<?php if (isset($_SESSION['login']) && isset($_SESSION['id'])) {?>
			<div class="ui-block-title ui-block-title-small">
				<h6 class="title">YOUR ACCOUNT</h6>
			</div>

			<ul class="account-settings">
				<li>
					<a href="<?= site_url('profile/update/' . $this->session->userdata('id')) ?>">

						<svg class="olymp-menu-icon"><use xlink:href="<?php echo base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-menu-icon"></use></svg>

						<span>Profile Settings</span>
					</a>
				</li>
				<li>
					<a href="#">
						<svg class="olymp-star-icon left-menu-icon"  data-toggle="tooltip" data-placement="right"   data-original-title="FAV PAGE"><use xlink:href="<?php echo base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-star-icon"></use></svg>

						<span>Create Fav Page</span>
					</a>
				</li>
				<li>
					<a href="<?php echo base_url() ?>logout">
						<svg class="olymp-logout-icon"><use xlink:href="<?php echo base_url(); ?>assets/svg-icons/sprites/icons.svg#olymp-logout-icon"></use></svg>

						<span>Log Out</span>
					</a>
				</li>
			</ul>
			<?php } ?>
			<div class="ui-block-title ui-block-title-small">
				<h6 class="title">About Olympus</h6>
			</div>

			<ul class="about-olympus">
				<li>
					<a href="#">
						<span>Terms and Conditions</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span>FAQs</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span>Careers</span>
					</a>
				</li>
				<li>
					<a href="#">
						<span>Contact</span>
					</a>
				</li>
			</ul>

		</div>
	</div>
</div>

<!-- ... end Fixed Sidebar Left -->
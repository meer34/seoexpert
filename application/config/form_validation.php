<?php

$config = [
    'signup' => [
                    [
                        'field' => 'uFname',
                        'label' => 'First Name',
                        'rules' => 'required|alpha'
                    ],
                    [
                        'field' => 'uLname',
                        'label' => 'Last Name',
                        'rules' => 'required|alpha'
                    ],
                    [
                        'field' => 'uNname',
                        'label' => 'Username',
                        'rules' => 'required|alpha_numeric|is_unique[users.user_nick]'
                    ],
                    [
                        'field' => 'uEmail',
                        'label' => 'Email',
                        'rules' => 'required|valid_email|is_unique[users.user_email]'
                    ],
                    [
                        'field' => 'uPhone',
                        'label' => 'Phone',
                        'rules' => 'required|numeric'
                    ],
                    [
                        'field' => 'uPassword',
                        'label' => 'Password',
                        'rules' => 'required|alpha_numeric'
                    ],
                    [
                        'field' => 'uCpassword',
                        'label' => 'Confirm Password',
                        'rules' => 'required|matches[uPassword]',
                    ],
                    [
                        'field' => 'country',
                        'label' => 'Country',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'state',
                        'label' => 'State',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'city',
                        'label' => 'City',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'street',
                        'label' => 'Street',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'datetimepicker',
                        'label' => 'Date of Birth',
                        'rules' => 'required',
                    ],
                    [
                        'field' => 'uGender',
                        'label' => 'Gender',
                        'rules' => 'required',                        
                    ]
                ],

    'login' =>  [
                    [
                        'field' => 'email',
                        'label' => 'Email',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'password',
                        'label' => 'Password',
                        'rules' => 'required'
                    ]
                ],
    'eSignup' => [
                    [
                        'field' => 'fname',
                        'label' => 'First Name',
                        'rules' => 'required|alpha'
                    ],
                    [
                        'field' => 'lname',
                        'label' => 'Last Name',
                        'rules' => 'required|alpha'
                    ],
                    [
                        'field' => 'username',
                        'label' => 'Username',
                        'rules' => 'required|alpha_numeric|is_unique[users.user_nick]'
                    ],
                    [
                        'field' => 'email',
                        'label' => 'Email',
                        'rules' => 'required|valid_email|is_unique[users.user_email]'
                    ],
                    [
                        'field' => 'alt-email',
                        'label' => 'Alternative Email',
                        'rules' => 'required|valid_email'
                    ],
                    [
                        'field' => 'rpm',
                        'label' => 'Rate Per Minute',
                        'rules' => 'required|numeric'
                    ],
                    [
                        'field' => 'uPassword',
                        'label' => 'Password',
                        'rules' => 'required|alpha_numeric'
                    ],
                    [
                        'field' => 'uCpassword',
                        'label' => 'Confirm Password',
                        'rules' => 'required|matches[uPassword]'
                    ],
                    [
                        'field' => 'gender',
                        'label' => 'Gender',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'datetimepicker',
                        'label' => 'Date of Birth',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'phone',
                        'label' => 'Phone',
                        'rules' => 'required|numeric'
                    ],
                    [
                        'field' => 'country',
                        'label' => 'Country',
                        'rules' => 'required|alpha'
                    ],
                    [
                        'field' => 'state',
                        'label' => 'State',
                        'rules' => 'required|alpha'
                    ],
                    [
                        'field' => 'city',
                        'label' => 'City',
                        'rules' => 'required|alpha'
                    ],
                    [
                        'field' => 'street',
                        'label' => 'Street',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'zip',
                        'label' => 'Zipcode',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'smallDisc',
                        'label' => 'Short Descpription',
                        'rules' => 'required|max_length[100]|min_length[100]'
                    ],
                    [
                        'field' => 'longDisc',
                        'label' => 'Long Descprition',
                        'rules' => 'required|max_length[300]|min_length[300]'
                    ],
                ],
    'expertedit' => [
                    [
                        'field' => 'fname',
                        'label' => 'First Name',
                        'rules' => 'required|alpha'
                    ],
                    [
                        'field' => 'lname',
                        'label' => 'Last Name',
                        'rules' => 'required|alpha'
                    ],
                    [
                        'field' => 'username',
                        'label' => 'Username',
                        'rules' => 'required|alpha_numeric|is_unique[users.user_nick]'
                    ],
                    [
                        'field' => 'email',
                        'label' => 'Email',
                        'rules' => 'required|valid_email|is_unique[users.user_email]'
                    ],
                    [
                        'field' => 'uPassword',
                        'label' => 'Password',
                        'rules' => 'required|alpha_numeric'
                    ],
                    [
                        'field' => 'uCpassword',
                        'label' => 'Confirm Password',
                        'rules' => 'required|matches[uPassword]'
                    ],
                    [
                        'field' => 'gender',
                        'label' => 'Gender',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'datetimepicker',
                        'label' => 'Date of Birth',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'phone',
                        'label' => 'Phone',
                        'rules' => 'required|numeric'
                    ],
                    [
                        'field' => 'country',
                        'label' => 'Country',
                        'rules' => 'required|alpha'
                    ],
                    [
                        'field' => 'state',
                        'label' => 'State',
                        'rules' => 'required|alpha'
                    ],
                    [
                        'field' => 'city',
                        'label' => 'City',
                        'rules' => 'required|alpha'
                    ],
                    [
                        'field' => 'street',
                        'label' => 'Street',
                        'rules' => 'required'
                    ],                   
                    [
                        'field' => 'smallDisc',
                        'label' => 'Short Descpription',
                        'rules' => 'required|max_length[100]|min_length[100]'
                    ],
                    [
                        'field' => 'longDisc',
                        'label' => 'Long Descprition',
                        'rules' => 'required|max_length[300]|min_length[300]'
                    ]
                ],
    'userEdit' => [
                    [
                        'field' => 'fname',
                        'label' => 'First name',
                        'rules' => 'required|alpha'
                    ],
                    [
                        'field' => 'lname',
                        'label' => 'Last name',
                        'rules' => 'required|alpha'
                    ],
                    [
                        'field' => 'username',
                        'label' => 'Username',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'email',
                        'label' => 'Username',
                        'rules' => 'required|is_unique[users.use_email]'
                    ],
                    [
                        'field' => 'uPassword',
                        'label' => 'Password',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'uCpasswrod',
                        'label' => 'Confirm Password',
                        'rules' => 'required|mathces[uPassword]'
                    ],
                    [
                        'field' => 'datetimepicker',
                        'label' => 'Date of Birth',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'Phone',
                        'label' => 'Phone',
                        'rules' => 'required'
                    ],        
                    [
                        'field' => 'datetimepicker',
                        'label' => 'Date of Birth',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'country',
                        'label' => 'Country',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'city',
                        'label' => 'City',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'state',
                        'label' => 'State',
                        'rules' => 'required'
                    ],
                    [
                        'field' => 'street',
                        'label' => 'Street',
                        'rules' => 'required'
                    ],            
                ]
];
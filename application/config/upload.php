<?php

    $config = [
        'upload_path' => './uploads/',
        'allowed_types' => 'gif|png|jgp|jpeg',
        'max_size' => '1000',
        'width' => '1024',
        'height' => '768'
    ];